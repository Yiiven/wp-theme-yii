=== Highlighting Code Block ===
Contributors: looswebstudio
Donate link: https://wemo.tech/2122/
Tags: SyntaxHighlighter, syntax highlighting, sourcecode, block-editor, classic editor, Guternberg,
Requires at least: 4.6
Tested up to: 5.2.1
Stable tag: 1.0.5
Requires PHP: 5.6
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

「Highlighting Code Block」只需单击按钮即可添加漂亮的语法突出显示代码块。

== Description ==

「Highlighting Code Block」只需单击按钮即可添加漂亮的语法突出显示代码块。

新旧两种编辑器都支持。

有关此插件的详细说明，请单击此处[ 查看 ](https://wemo.tech/2122/)。


= 安装并启用后 =

- 在块编辑器「格式设置」中提供一个名为「Highlighting Code Block」的代码块。
选择「Highlighting Code Block」后，出现带有选择框的代码块。
请选择您喜欢的语言，然后编写代码。
- 在经典编辑器中，工具栏上会出现「代码块」的选取框。
从此处的选择框中选择您喜欢的语言，将插入代码块(pre-Tag)。
也可以选择已输入的文本后再选择代码块。文本将自动被代码块包裹。（如果包含换行符，可能无法完美地进行包裹。）


= 不工作？ =

本插件仅在PHP5.6+版本中运行。
请确认您的PHP版本。



= 各種設定 =

可以从管理后台左侧菜单的「设置」中的「CODE BLOCK」进入设置界面更改配置信息。


== 安装 ==


= 自动安装 =
1. 从WordPress安装插件的搜索字段输入「Highlighting Code Block」搜索。
2. 如果找到了本插件，请点击"现在安装"进行安装，安装后启用即可。

= 手动安装 =
1.将整个「highlighting-code-block」文件夹上传到「/wp-content/plugins/」目录中。
2.从「插件」菜单启用本插件。


== Frequently Asked Questions ==

= 关于可使用的语言 =
默认情况下，可以使用以下语言。
  - HTML
  - CSS
  - SCSS
  - JavaScript
  - TypeScript
  - PHP
  - Ruby
  - Python
  - Swift
  - C
  - C#
  - C++
  - Objective-C
  - SQL
  - JSON
  - Bash
  - Git


== Screenshots ==

1. Code Coloring
2. Select 「Highlighing Code Block
3. Select lang (Guternberg)
4. Writing your code (Guternberg)
5. Added select box (Tinymce)
6. Select lang (Tinymce)
7. Writing your code (Tinymce)
8. ex) Light color
9. ex) Dark color
10. Base Setting
11. Higher Setting


== Changelog ==
= 1.0.5 =
将script的载入移动到wp_footer
更改HCB块标志
代码块的font-family变得能自定义了。

= 1.0.4 =
WordPress5.2.1适配
代码块的font-family变更(因为windows的字体难读)
允许您在代码块中设置文件名

= 1.0.3 =
WordPress5.1.1适配

= 1.0.2 =
Change Readme.txt

= 1.0.1 =
Comment delete.

= 1.0 =
Initial working version.
