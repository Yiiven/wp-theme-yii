document.addEventListener('DOMContentLoaded', function () {
    // Token customize
    var keyToken = document.querySelectorAll('.token.keyword');
    for (var i = 0; i < keyToken.length; i++) {
        var elem = keyToken[i];

        var defArr = ['function', 'def', 'class'];
        if (defArr.indexOf( elem.textContent ) !== -1) {
            elem.classList.add('def');
        } else if (elem.textContent === "this") {
            elem.classList.add('this');
        }
    }

    //Line highlighter position.
    var lineHighlighter = document.querySelectorAll('.line-highlight');
    for (var i = 0; i < lineHighlighter.length; i++) {
        var elem = lineHighlighter[i];
        if ( !elem.parentNode.classList.contains("line-numbers") ) {
            var dataStart = elem.getAttribute('data-start');
            var topPosEm = (dataStart - 1) * 1.6;
            if (dataStart > 5) {
                topPosEm = topPosEm - (dataStart * 0.02);
            }
            if (dataStart > 10) {
                topPosEm = topPosEm - (dataStart * 0.01);
            }
            if (dataStart > 20) {
                topPosEm = topPosEm - (dataStart * 0.001);
            }
            elem.style.top = topPosEm + "em";
        }
    }

    //line_off
});
