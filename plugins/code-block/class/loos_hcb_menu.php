<?php

class LOOS_HCB_Menu {

    /**
     * Set const
     */
    const PAGE_NAME     = 'hcb_settings';
    const SECTION_NAME  = 'hcb_setting_section';
    const SECTION_NAME2 = 'hcb_setting_section2';


    /**
     * Constructor
     */
    public function __construct() {
        add_action( 'admin_menu', array( $this, 'add_hcb_page' ) );
        add_action( 'admin_init', array( $this, 'add_hcb_fields' ) );
    }


    /**
     * Add HCB setting page.
     */
    public function add_hcb_page() {
        add_options_page(
            'CODE BLOCK',  // 页面标题
            'CODE BLOCK',  // 菜单标题
            'manage_options', // 操作此页面的权限
            self::PAGE_NAME,   // 页面名称
            array($this, 'hcb_settings_cb') // 回调函数。输出此函数的执行结果
        );
    }


    /**
     * 设置页面显示函数
     */
    public function hcb_settings_cb() {
        echo '<div class="wrap hcb_setting"><h1>Highlighting Code Block 设置</h1><form action="options.php" method="post">';
            do_settings_sections( self::PAGE_NAME ); // 显示此页面中注册的区域
            settings_fields( self::PAGE_NAME );      // register_setting() 中使用的组名称必须匹配。
            submit_button();
        echo '</form></div>';
    }


    /**
     * 注册设置项目字段
     */
    public function add_hcb_fields() {

        /**
         * 注册数据库中保存的选项名称
         * 因为以相同选项排列保存值，所以register_setting()只有一个 
         *   @param :  组名/数据库选项名称/分类函数 
         */
        register_setting( self::PAGE_NAME, LOOS_HCB::DB_NAME['settings'] );  

        /**
         * 基本设置
         */
        add_settings_section(
            self::SECTION_NAME,  //ID名
            '基本设置',            //区域标题
            '',                  //输出区域的函数名称
            self::PAGE_NAME      //显示区域的设定页面
        );
        // 是否显示语言名称
        add_settings_field(
            'show_lang',                         //ID名
            '显示语言名称',                      //字段标题
            array( $this, 'settings_field_cb'),  //输出字段的函数。作为参数传递一个数组（$args）
            self::PAGE_NAME,                     //显示字段的设定页面
            self::SECTION_NAME,                  //要显示字段的设置页面的区域
            array(
                'id' => 'show_lang',
                'type' => 'checkbox',
                'label' => '在代码块中显示语言名称',
                'desc' => ' 如果选中的话，在站点显示侧的代码中会显示语言类型。'
            )
        );
        // 是否显示行号
        add_settings_field(
            'show_linenum',
            '显示行号',
            array( $this, 'settings_field_cb'),
            self::PAGE_NAME,
            self::SECTION_NAME,
            array(
                'id' => 'show_linenum',
                'type' => 'checkbox',
                'label' => '在代码块中显示行号',
                'desc' => '如果选中的话，将在代码块的左边缘显示行号。'
            )
        );
        // font-smoothing设置
        add_settings_field(
            'font_smoothing',
            '字体平滑',
            array( $this, 'settings_field_cb'),
            self::PAGE_NAME,
            self::SECTION_NAME,
            array(
                'id' => 'font_smoothing',
                'type' => 'checkbox',
                'label' => '启用字体平滑',
                'desc' => '代码块将追加<code>-webkit-font-smoothing: antialiased;</code>和<code>-moz-osx-font-smoothing: grayscale;</code>。'
            )
        );
        //  代码颜色(前台)
        add_settings_field(
            'front_coloring',
            '代码颜色(站点显示)',
            array($this, 'settings_field_cb'),
            self::PAGE_NAME,   
            self::SECTION_NAME,
            array(
                'id' => 'front_coloring',
                'type' => 'radio',
                'choices' => array(
                    'Light' => 'light',
                    'Dark' => 'dark',
                )
            )
        );
        // 代码色彩(编辑器)
        add_settings_field(
            'editor_coloring',
            '代码色彩(编辑器中)',
            array($this, 'settings_field_cb'),
            self::PAGE_NAME,   
            self::SECTION_NAME,
            array(
                'id' => 'editor_coloring',
                'type' => 'radio',
                'choices' => array(
                    'Light' => 'light',
                    'Dark' => 'dark',
                )
            )
        );
        // 字体大小(PC)
        add_settings_field(
            'fontsize_pc',
            '字体大小(PC)',
            array($this, 'settings_field_cb'),
            self::PAGE_NAME,   
            self::SECTION_NAME,
            array(
                'id' => 'fontsize_pc',
                'type' => 'input',
                'input_type' => 'text',
                'before' => 'font-size: ',
                'after' => '',
                'desc' => ''
            )
        );
        // 字体大小(SP)
        add_settings_field(
            'fontsize_sp',
            '字体大小(SP)',
            array($this, 'settings_field_cb'),
            self::PAGE_NAME,   
            self::SECTION_NAME,
            array(
                'id' => 'fontsize_sp',
                'type' => 'input',
                'input_type' => 'text',
                'before' => 'font-size: ',
                'after' => '',
                'desc' => ''
            )
        );
        // font-family
        add_settings_field(
            'fontsize_sp',
            'font-family: <br>代码字体',
            array($this, 'settings_field_cb'),
            self::PAGE_NAME,   
            self::SECTION_NAME,
            array(
                'id' => 'font_family',
                'type' => 'textarea',
                'rows' => 2,
                'before' => '',
                'after' => '',
                'desc' => '默认：<code>Menlo, Consolas, "メイリオ", sans-serif;</code>'
            )
        );
        // 块编辑器的内容宽度
        add_settings_field(
            'block_width',
            '块编辑器的内容宽度',
            array($this, 'settings_field_cb'),
            self::PAGE_NAME,   
            self::SECTION_NAME,
            array(
                'id' => 'block_width',
                'type' => 'input',
                'input_type' => 'number',
                'before' => '',
                'after' => ' px',
                'desc' => '可以设定适用于撰写代码的宽度。( WordPress的默认值是<code>610px</code>)'
            )
        );

        /**
         * 高级设置
         */
        add_settings_section( self::SECTION_NAME2, '高级设置', '', self::PAGE_NAME);
        // 要使用的语言设置
        add_settings_field(
            'support_langs',
            '要使用的语言设置',
            array($this, 'settings_field_cb'),
            self::PAGE_NAME,
            self::SECTION_NAME2,
            array(
                'id' => 'support_langs',
                'type' => 'textarea',
                'rows' => 16,
                'desc' => '「<code>类名:"语言名"</code>」多语言以「<code>,</code>」分隔。是否换行均可。
                        <br>&emsp;&emsp;「类名」是prism.js中使用的类名、相当于「lang-xxx」的「xxx」に該当する部分です
                        <br>&emsp;&emsp;「语言名」显示在编辑器按钮和代码块上
                        <br>※ 使用默认不支持的语言时、请与独立的「prism.js」设定并用。'
            )
        );
        // 独立的色彩文件
        add_settings_field(
            'prism_css_path',
            '独立的色彩文件',
            array($this, 'settings_field_cb'),
            self::PAGE_NAME,
            self::SECTION_NAME2,
            array(
                'id' => 'prism_css_path',
                'type' => 'input',
                'input_type' => 'text',
                'before' => get_stylesheet_directory_uri()."/ ",
                'after' => '',
                'desc' => '配置您准备的代码颜色CSS样式表文件。'
            )
        );
        // 独立的prism
        add_settings_field(
            'prism_js_path',
            '独立的prism.js',
            array($this, 'settings_field_cb'),
            self::PAGE_NAME,
            self::SECTION_NAME2,
            array(
                'id' => 'prism_js_path',
                'type' => 'input',
                'input_type' => 'text',
                'before' => get_stylesheet_directory_uri()."/ ",
                'after' => '',
                'desc' => '您可以使用对应于您自己的语言集合的prism.js文件。'
            )
        );
        // 帮助
        add_settings_field(
            'hcb_help',
            '帮助',
            array($this, 'settings_field_cb'),
            self::PAGE_NAME,
            self::SECTION_NAME2,
            array(
                'type' => '',
                'desc' => '
                文件请上传到主题目录。
                <br>
                如果指定了文件路径，则无法载入现有的彩色文件或prism.js文件。
                <br><br>
                ※ 当前载入的prism.js文件是<a href="https://prismjs.com/download.html#themes=prism&languages=markup+css+clike+javascript+c+csharp+bash+cpp+ruby+markup-templating+git+java+json+objectivec+php+sql+scss+python+typescript+swift&plugins=line-highlight+line-numbers" target="_blank"> 这里 </a>可以下载到的JS文件。
                '
            )
        );
    }


    /**
     * 设置项目字段显示函数
     */
    public function settings_field_cb( $args ) {

        if ( $args['type'] === 'input' ) {
            //<input:"text"|"number"|"email"|...]>
            self::set_field_input( $args['id'], $args['input_type'], $args['before'], $args['after'] );

        } elseif( $args['type'] === 'radio' ) {

            //<input:"radio">
            self::set_field_radio( $args['id'], $args['choices']);

        } elseif( $args['type'] === 'checkbox' ) {
            //<input:"checkbox">
            self::set_field_checkbox( $args['id'], $args['label'] );

        } elseif( $args['type'] === 'textarea' ) {
            //<textarea>
            self::set_field_textarea( $args['id'], $args['rows'] );

        }

        if ( isset( $args['desc'] ) ) {
            // Field description
            echo '<p class="description">', $args['desc'], '</p>';
        }

    }


    /**
     * <input:"text"|"number"|"email"|...]>
     */
    private static function set_field_input( $field_id, $type, $before_text, $after_text ) {

        $setting_name = LOOS_HCB::DB_NAME['settings'];
        //$setting_data = get_option( $setting_name );
        $setting_data = LOOS_HCB::$settings;

        echo $before_text, '<input id="',$field_id,'" name="',$setting_name."[$field_id]" ,'" type="', $type, '" value="', $setting_data[$field_id] ,'" />', $after_text;

    }

    /**
     * <textarea>
     */
    private static function set_field_textarea( $field_id, $rows ) {

        $setting_name = LOOS_HCB::DB_NAME['settings'];
        //$setting_data = get_option( $setting_name );
        $setting_data = LOOS_HCB::$settings;

        echo '<textarea id="',$field_id,'" name="',$setting_name."[$field_id]" ,'" type="text" class="regular-text" rows="', $rows, '" >', $setting_data[$field_id] ,'</textarea>';

    }

    /**
     * <input:"radio">
     */
    private static function set_field_radio( $field_id, $choices ) {

        $setting_name = LOOS_HCB::DB_NAME['settings'];
        //$setting_data = get_option( $setting_name );
        $setting_data = LOOS_HCB::$settings;

        echo '<fieldset>';
        foreach ($choices as $key => $val) {
            $radio_id = $field_id.'_'.$val;
            $checked = checked( $setting_data[$field_id], $val, false );
            $attr = 'id="'.$radio_id.'" name="'.$setting_name."[$field_id]".'" value="'.$val.'" '.$checked.'"';
            echo '<label for="', $radio_id, '">',
                    '<input type="radio" ', $attr, ' >',
                    '<span>', $key, '</span>',
                '</label><br>';
        }
        echo '</fieldset>';

    }

    /**
     * <input:"checkbox">
     */
    private static function set_field_checkbox( $field_id, $label ) {

        $setting_name = LOOS_HCB::DB_NAME['settings'];
        //$setting_data = get_option( $setting_name );
        $setting_data = LOOS_HCB::$settings;

        $checked = checked( $setting_data[$field_id], 'on', false );
        echo '<input type="hidden" name="', $setting_name."[$field_id]", '" value="off">';
        echo '<input type="checkbox" id="', $field_id, '" name="', $setting_name."[$field_id]", '" value="on" ', $checked, ' />';
        echo '<label for="', $field_id, '">', $label, '</label>';

    }

}

