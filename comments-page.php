<?php
/**
 * The template for displaying Comments on pages.
 * @package yii
 */
?>
<!-- You can start editing here. -->
<?php if(post_password_required()){ ?>
    <!-- 如果评论是开启的，但需要密码访客查看。 -->
    <p class="nocomments align-center"><?php _e('本文受密码保护。输入密码方可查看。', 'yii'); ?></p>
<?php }else{ ?>
    <div class="comment-switcher">
        <span class="comment-title"><?php printf(_n('当前留言：%s', '当前留言：%1$s', get_comments_number(), 'yii'), number_format_i18n(get_comments_number()));?></span>
        <span class="comment-scroll"><i class="yi yi-speechbubble-dark"></i><?php _e('前往留言区', 'yii') ?></span>
    </div>
    <?php if(!have_comments()){ ?>
        <!-- 如果评论是开启的，但没有评论。 -->
        <p class="nocomments align-center"><?php _e('快来抢占本文沙发吧！', 'yii'); ?></p>
        <div id="comment-pagination" class="comment-pagination"></div>
    <?php }else{ ?>
        <ol class="comment-list">
            <?php get_template_part("template/comments-list") ?>
        </ol>
        <div id="comment-pagination" class="comment-pagination">
            <?php 
            the_comments_pagination(
                array(
                    'prev_text' => '<span class="prev-comments alignleft">'.__('上一页', 'yii').'</span>',
                    'next_text' => '<span class="next-comments alignright">'.__('下一页', 'yii').'</span>',
                )
            );
            ?>
        </div>
    <?php } ?>

    <?php 
        $req = get_option('require_name_email');
        $aria_req = ($req ? " aria-required='true'" : '');
    ?>
    <div id="respond" class="comment-respond wow fadeInUp" data-wow-delay="0.3s">
        <h3 id="reply-title" class="comment-reply-title"><span><?php _e('发表留言', 'yii'); ?></span><small><?php cancel_comment_reply_link(''.sprintf(__('取消回复', 'yii')).'');?></small></h3>
        <?php if(get_option('comment_registration') && !$user_ID): ?>
            <p class="comment-nologin"><?php print ''.sprintf(__('您必须', 'yii')).''; ?><a href="<?php echo get_option('siteurl'); ?>/wp-login.php?redirect_to=<?php echo urlencode(get_permalink()); ?>"><?php _e('登录', 'yii'); ?></a><?php _e('才能发表留言！', 'yii'); ?></p>
        <?php else : ?>
            <form id="commentform" class="comment-form" method="post" action="<?php echo get_option('siteurl'); ?>/wp-comments-post.php">
                <?php if($user_ID): ?>
                    <div class="user_avatar">
                        <?php 
                            global $current_user;
                            wp_get_current_user();
                            echo yi_get_avatar($user_ID, $current_user->user_email, false, 50);
                            $hidden = 'hidden="hidden"';
                            $commenter = array(
                                "comment_author" => $current_user->display_name,
                                "comment_author_email" => $current_user->user_email,
                                "comment_author_url" => $current_user->user_url,
                                "comment_author_qq" => "",
                                "comment_author_tel" => "",
                            );
                        ?>
                        <?php _e('您已登录为：', 'yii'); ?><a href="<?php echo admin_url(); ?>/profile.php"><?php echo $user_identity; ?></a><br />
                        <a href="<?php echo wp_logout_url(get_permalink()); ?>"><?php _e('登出', 'yii'); ?></a>
                    </div>
                    <?php elseif ('' != $comment_author): ?>
                    <div class="author_avatar">
                        <?php echo yi_get_avatar(0, $comment_author_email, false, $size='50'); ?>
                        <?php printf (''.sprintf(__('欢迎', 'yii')).' <strong>%s</strong>', $comment_author); ?> <?php _e('再次光临！', 'yii'); ?><br />
                        <a href="javascript:toggleCommentAuthorInfo();" id="toggle-comment-author-info"><?php _e('修改信息', 'yii'); ?></a>
                        <script type="text/javascript">
                            //<![CDATA[
                            var changeMsg = "修改信息";
                            var closeMsg = "关闭";
                            function toggleCommentAuthorInfo(){
                                jQuery('#comment-author-info').slideToggle('slow', function(){
                                    if (jQuery('#comment-author-info').css('display') == 'none'){
                                        jQuery('#toggle-comment-author-info').text(changeMsg);
                                    }else{
                                        jQuery('#toggle-comment-author-info').text(closeMsg);
                                    }
                                });
                            }
                            jQuery(document).ready(function(){
                                jQuery('#comment-author-info').hide();
                            });
                            //]]>
                        </script>
                    </div>
                <?php endif; ?>
                <div class="comment-form-comment">
                    <div class="comment-tool">
                        <?php get_template_part('template/smiley-bar'); ?>
                        <div class="comment-tool-float">
                            <?php get_template_part('template/smiley'); ?>
                        </div>
                    </div>
                    <textarea id="comment" name="comment" placeholder="*<?php _e('赠人玫瑰，手留余香...', 'yii'); ?>" onfocus="this.placeholder=''" onblur="this.placeholder='<?php _e('赠人玫瑰，手留余香...','yii'); ?>'" cols="45" rows="8" aria-required="true"></textarea>
                </div>
                <div id="comment-author-info" <?php if($user_ID){echo $hidden;} ?>>
                    <p class="comment-form-author">
                        <label for="author"><span class="required">*</span><?php _e('昵称', 'yii'); ?></label>
                        <input id="author" name="author" type="text" placeholder="*<?php _e('昵称', 'yii'); ?>" value="<?php echo esc_attr($commenter['comment_author']); ?>" size="40"'.$aria_req.'/>
                    </p>
                    <p class="comment-form-email">
                        <label for="email"><span class="required">*</span><?php _e('邮箱', 'yii'); ?></label>
                        <input id="email" name="email" type="text" placeholder="*<?php _e('您的邮件地址不会被公开。', 'yii'); ?>" value="<?php echo esc_attr($commenter['comment_author_email']); ?>" size="40"'.$aria_req.'/>
                    </p>
                    <p class="comment-form-url">
                        <label for="url"><?php _e('网站', 'yii'); ?></label>
                        <input id="url" placeholder="<?php _e('网站', 'yii'); ?>" name="url" type="text" value="<?php echo esc_attr($commenter['comment_author_url']); ?>" size="40"/>
                    </p>
                    <?php if(_yi('comment_meta_info')){ ?>
                        <p class="comment-form-qq">
                            <label for="qq"><?php _e('QQ', 'yii'); ?></label>
                            <input id="qq" placeholder="<?php _e('QQ', 'yii'); ?>" name="qq" type="text" value="<?php echo esc_attr($commenter['comment_author_qq']); ?>" size="11"/>
                        </p>
                        <p class="comment-form-tel">
                            <label for="tel"><?php _e('TEL', 'yii'); ?></label>
                            <input id="tel" placeholder="<?php _e('TEL', 'yii'); ?>" name="tel" type="text" value="<?php echo esc_attr($commenter['comment_author_tel']); ?>" size="11"/>
                        </p>
                    <?php } ?>
                </div>
                <?php if(!is_user_logged_in()){echo yi_protection_math();} ?>
                <p class="form-submit">
                    <input id="submit" name="submit" type="submit" tabindex="5" value="<?php _e('提交留言', 'yii'); ?>"/>
                    <?php comment_id_fields(); do_action('comment_form', $post->ID); ?>
                </p>
                <?php if(_yi('comment_reply_notify')){ ?>
                <p>
                    <input class="reply-notify" type="checkbox" name="comment_reply_notify" id="comment_reply_notify" value="comment_reply_notify" checked="checked"/>
                    <label for="comment_reply_notify" class="checkbox inline hide" style="padding-top:0">有人回复时邮件通知我</label>
                </p>
                <?php } ?>
            </form>
        <?php endif; ?>
    </div>
<?php } ?>