<?php
/**
 * The template for displaying the searchform
 *
 * @package yii
 * 站内搜索
 */
?>
<div class="searchbar">
    <form role="search" method="get" id="baiduform" class="searchform" action="<?php echo esc_url(home_url()); ?>/" target="">
        <span class="search-input">
            <input class="swap_value" name="s" type="text" value="<?php the_search_query(); ?>" placeholder="<?php _e('输入搜索内容', 'yii'); ?>" required />
            <button type="submit" id="searchbaidu"><i class="yi yi-search"></i></button>
        </span>
        <?php if (_yi('search_cat')) { ?>
        <span class="search-cat">
            <?php
            //$args = array(
            //    'show_option_all' => '全部分类',
            //    'hide_empty'      => 0,
            //    'name'            => 'cat',
            //    'show_count'      => 0,
            //    'taxonomy'        => 'category',
            //    'hierarchical'    => 1,
            //    'depth'           => -1,
            //    'exclude'         => _yi('not_search_cat'),
            //); 
            //wp_dropdown_categories($args);
            ?>
        </span>
        <?php } ?>
    </form>
</div>
