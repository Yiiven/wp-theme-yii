<?php
    function yi_get_next_post_link($in_same_term = false, $excluded_terms = '', $taxonomy = 'category' ) {
        return yi_get_adjacent_post_link($in_same_term, $excluded_terms, true, $taxonomy);
    }

    function yi_get_previous_post_link($in_same_term = false, $excluded_terms = '', $taxonomy = 'category' ) {
        return yi_get_adjacent_post_link($in_same_term, $excluded_terms, false, $taxonomy);
    }

    function yi_get_next_post_title($in_same_term = false, $excluded_terms = '', $taxonomy = 'category' ) {
        return yi_get_adjacent_post_title($in_same_term, $excluded_terms, true, $taxonomy);
    }

    function yi_get_previous_post_title($in_same_term = false, $excluded_terms = '', $taxonomy = 'category' ) {
        return yi_get_adjacent_post_title($in_same_term, $excluded_terms, false, $taxonomy);
    }

    /**
     * 获取上一篇，下一篇文章链接地址
     * @param  boolean  $in_same_term   指定文章是否在同一分类目录或标签下
     * @param  string/array  $excluded_terms     要排除的目录ID，多个ID以","分隔，或者传入ID数组
     * @param  boolean $previous        是否指向上一篇，true-上一篇,false-下一篇
     * @param  string  $taxonomy        分类法，当$in_same_term为true可用，可选值tag,categroy，默认值:categroy
     * @return string                   文章的链接地址
     */
    function yi_get_adjacent_post_link($in_same_term, $excluded_terms, $previous=true, $taxonomy){
        $adjacent = $previous ? 'previous' : 'next';
        //$post = add_filter("get_{$adjacent}_post", $in_same_term, $excluded_terms, $taxonomy);
        $function_name = "get_{$adjacent}_post";
        $post = $function_name($in_same_term, $excluded_terms, $taxonomy);
        if($post){
            return get_permalink($post->ID);
        }
        return "";
    }

    /**
     * 获取上一篇，下一篇文章标题
     * @param  boolean  $in_same_term   指定文章是否在同一分类目录或标签下
     * @param  string/array  $excluded_terms    要排除的目录ID，多个ID以","分隔，或者传入ID数组
     * @param  boolean $previous        是否指向上一篇，true-上一篇,false-下一篇
     * @param  string  $taxonomy        分类法，当$in_same_term为true可用，可选值tag,categroy，默认值:categroy
     * @return string                   文章的标题
     */
    function yi_get_adjacent_post_title($in_same_term, $excluded_terms, $previous=true, $taxonomy){
        $adjacent = $previous ? 'previous' : 'next';
        //$post = add_filter("get_{$adjacent}_post", $in_same_term, $excluded_terms, $taxonomy);
        $function_name = "get_{$adjacent}_post";
        $post = $function_name($in_same_term, $excluded_terms, $taxonomy);
        if($post){
            return get_the_title( $post->ID );
        }
        return "";
    }

    /**
     * 在文章内随机挂载广告
     */
    function yi_insert_after_paragraph($insertion, $content){
        $where_display = _yi('ads_single_inside_where');
        $paragraph_id = 0;//控制在哪几个段落中随机，默认为0
        switch($where_display){
            case "p0"://独立于标题之前
                $explode_tag = "<article>";
                break;
            case "p1"://嵌入文章，在摘要之后
                $explode_tag = "<p>";
                break;
            case "pr"://嵌入文章，随机段落
                $explode_tag = "<p>";
                $paragraph_id = rand(1, substr_count($content, $explode_tag));
                break;
            default:
                break;
        }
        $paragraphs = explode($explode_tag, $content);
        foreach ($paragraphs as $index => $paragraph){
            if (trim($paragraph)){
                $paragraphs[$index] = $explode_tag.$paragraphs[$index];
            }
            if ($paragraph_id == $index){
                $paragraphs[$index] = $insertion.$paragraphs[$index];
            }
        }
        return implode('', $paragraphs);
    }
    if(_yi('ads_single_inside')){
        add_filter('the_content', 'yi_insert_post_ads', 999);
    }
    function yi_insert_post_ads($content){
        $ad_content_before = '<div class="ads-single-inside">';
        $ad_content_after = '</div>';
        $ad_content = stripslashes(_yi('ads_single_inside_code'));//<img>必须包含class属性，属性值可以为空
        $ad_code = $ad_content_before.($ad_content).$ad_content_after;
        if (is_single() && !is_admin()){
            // 修改 2 这个段落数
            return yi_insert_after_paragraph($ad_code, $content);
        }
        return $content;
    }