<?php 
/*
 * breadcrumb
 */
function yi_breadcrumb() {
    /* === OPTIONS === */
    $text['home']     = get_bloginfo('name'); // 首页，用站点名称替换"首页"
    $text['category'] = '%s'; // 分类
    $text['search']   = '%s'; // 搜索结果
    $text['tag']      = '%s'; // 标签
    $text['author']   = '%s'; // 作者
    $text['404']      = __('哎呀，迷路了', 'yii').'...'; // 404
    $text['page']     = '第 %s 页'; // 页码
    $text['cpage']    = '第 %s 页'; // 评论页码
    $wrap_before    = ''; // 面包屑开始
    $wrap_after     = ''; // 面包屑结束
    $sep            = '<i class="yi yi-arrowright"></i>'; // 面包屑分隔符，需要配合CSS，更简单的方式是使用文本替代
    $before         = ''; // 面包屑之前的备注，结合"$after"可用于自定义面包屑文档结构
    $after          = ''; // 面包屑之后的备注，结合"$before"可用于自定义面包屑文档结构
    $show_on_home   = _yi('show_crumbs_on_home') ? 1 : 0; // 1 - 在首页显示面包屑, 0 - 不显示在首页
    $show_homelink = _yi('show_crumbs_homelink') ? 1 : 0; // 1 - 显示首页链接, 0 - 不显示
    $show_current   = _yi('show_crumbs_current') ? 1 : 0; // 1 - 显示当前页标题, 0 - 不显示
    $show_last_sep  = _yi('show_crumbs_last_step') ? 1 : 0; // 1 - 显示最后一个分隔符，0-不显示
    /* === END OF OPTIONS === */
    global $post;   // 文章列表
    $home_url       = home_url('/');
    $link           = '<a class="%1$s" href="%2$s" rel="category tag">%3$s</a>';

    $parent_id      = ( $post ) ? $post -> post_parent : '';    // 文章父级ID
    /**
     * sprintf(format,arg1,arg2,arg++)
     * format - 必须，规定字符串以及如何格式化其中的变量。
     * arg1 - 必须，规定插到 format 字符串中第一个 % 符号处的参数。
     * arg2 - 可选，规定插到 format 字符串中第二个 % 符号处的参数。
     * arg++ - 可选，规定插到 format 字符串中第三、四等等 % 符号处的参数。
     */
    $home_link      = sprintf( $link, "crumbs", $home_url, $text['home']);

    $breadcrumb = $wrap_before;
    if ( is_home() || is_front_page() ) {
        //主页||首页
        if($show_on_home){//判断是否在首页显示
            $breadcrumb .= $home_link;
        }
        if ( get_query_var( 'paged' ) ) {
            $breadcrumb .= $sep . $before . sprintf( $text['page'], get_query_var( 'paged' ) ) . $after;//追加页码到面包屑
        }
    } else {
        //显示首页链接
        if ( $show_homelink ) {
            $breadcrumb .= $home_link . $sep;
        }
        //分类页
        if ( is_category() ) {
            $parents = get_ancestors( get_query_var('cat'), 'category' );   // 获取全部父级分类
            $len = count($parents);
            // 列出分类，最后一个之后不追加分隔符
            foreach ( array_reverse( $parents ) as $key => $cat ) {
                $breadcrumb .= sprintf( $link, "", get_category_link( $cat ), get_cat_name( $cat ));
                if( $key + 1 < $len) $breadcrumb .= $sep;
            }
            // 获取分页
            if ( get_query_var( 'paged' ) ) {
                $cat = get_query_var('cat');
                $breadcrumb .= sprintf( $link, "", get_category_link( $cat ), get_cat_name( $cat ));
                $breadcrumb .= $sep . $before . sprintf( $text['page'], get_query_var( 'paged' ) ) . $after;//追加页码到面包屑
            } else {
                //显示当前页标题
                if ( $show_current ) {
                    $breadcrumb .= $before . sprintf( $text['category'], single_cat_title( '', false ) ) . $after;
                }
            }
        }
        //搜索结果页
        if ( is_search() ) {
            if ( get_query_var( 'paged' ) ) {
                $breadcrumb .= sprintf( $link, "", $home_url . '?s=' . get_search_query(), sprintf( $text['search'], get_search_query() ));
                $breadcrumb .= $sep . $before . sprintf( $text['page'], get_query_var( 'paged' ) ) . $after;
            } else {
                if ( $show_current ) {
                    $breadcrumb .= $before . sprintf( $text['search'], get_search_query() ) . $after;
                }
            }
        }

        //按年份
        if ( is_year() ) {
            if ( $show_current ) $breadcrumb .= $before . get_the_time('Y') . $after;
        }
        //按月份
        if ( is_month() ) {
            $breadcrumb .= sprintf( $link, "", get_year_link( get_the_time('Y') ), get_the_time('Y'));
            if ( $show_current ) $breadcrumb .= $sep . $before . get_the_time('F') . $after;
        }
        //按日期
        if ( is_day() ) {
            $breadcrumb .= sprintf( $link, "", get_year_link( get_the_time('Y') ), get_the_time('Y')) . $sep;
            $breadcrumb .= sprintf( $link, "", get_month_link( get_the_time('Y'), get_the_time('m') ), get_the_time('F'));
            if ( $show_current ) $breadcrumb .= $sep . $before . get_the_time('d') . $after;
        }

        //文章页，且非附件归档页
        if ( is_single() && ! is_attachment() ) {
            // 判断文章类型
            if ( get_post_type() != 'post' ) {
                // 
                $post_type = get_post_type_object( get_post_type() );
                $breadcrumb .= sprintf( $link, "", get_post_type_archive_link( $post_type->name ), $post_type->labels->name);
                if ( $show_current ) $breadcrumb .= $sep . $before . get_the_title() . $after;
            } else {
                $cat = get_the_category();
                $catID = $cat[0] -> cat_ID;
                $parents = get_ancestors( $catID, 'category' );
                $parents = array_reverse( $parents );
                $parents[] = $catID;
                $len = count($parents);
                foreach ( $parents as $key => $cat ) {
                    $breadcrumb .= sprintf( $link, "", get_category_link( $cat ), get_cat_name( $cat ));
                    if( $key + 1 < $len) $breadcrumb .= $sep;
                }
                if ( get_query_var( 'cpage' ) ) {
                    $breadcrumb .= $sep . sprintf( $link, "", get_permalink(), get_the_title());
                    $breadcrumb .= $sep . $before . sprintf( $text['cpage'], get_query_var( 'cpage' ) ) . $after;
                } else {
                    if ( $show_current ) $breadcrumb .= $sep . $before . get_the_title() . $after;
                }
            }
        }

        // 文章归档页
        if ( is_post_type_archive() ) {
            $post_type = get_post_type_object( get_post_type() );
            if ( get_query_var( 'paged' ) ) {
                $breadcrumb .= sprintf( $link, "", get_post_type_archive_link( $post_type->name ), $post_type->label);
                $breadcrumb .= $sep . $before . sprintf( $text['page'], get_query_var( 'paged' ) ) . $after;
            } else {
                if ( $show_current ) $breadcrumb .= $before . $post_type->label . $after;
            }
        }

        // 附件归档页
        if ( is_attachment() ) {
            $parent = get_post( $parent_id );
            $cat = get_the_category( $parent->ID );
            $catID = $cat[0] -> cat_ID;
            $parents = get_ancestors( $catID, 'category' );
            $parents = array_reverse( $parents );
            $parents[] = $catID;
            $len = count($parents);
            foreach ( $parents as $key => $cat ) {
                $breadcrumb .= sprintf( $link, "", get_category_link( $cat ), get_cat_name( $cat ));
                if( $key + 1 < $len) $breadcrumb .= $sep;
            }
            $breadcrumb .= sprintf( $link, "", get_permalink( $parent ), $parent->post_title);
            if ( $show_current ) $breadcrumb .= $sep . $before . get_the_title() . $after;
        }

        // 单页面，无父级页面（即顶级页面）
        if ( is_page() && ! $parent_id ) {
            if ( $show_current ) $breadcrumb .= $before . get_the_title() . $after;
        }

        // 单页面，有父级页面（即子页面）
        if ( is_page() && $parent_id ) {
            $parents = get_post_ancestors( get_the_ID() );
            $len = count($parents);
            foreach ( array_reverse( $parents ) as $key => $pageID ) {
                $breadcrumb .= sprintf( $link, "", get_page_link( $pageID ), get_the_title( $pageID ));
                if( $key + 1 < $len) $breadcrumb .= $sep;
            }
            if ( $show_current ) $breadcrumb .= $sep . $before . get_the_title() . $after;
        }

        // 标签归档页
        if ( is_tag() ) {
            if ( get_query_var( 'paged' ) ) {
                $tagID = get_query_var( 'tag_id' );
                $breadcrumb .= sprintf( $link, "", get_tag_link( $tagID ), single_tag_title( '', false ));
                $breadcrumb .= $sep . $before . sprintf( $text['page'], get_query_var( 'paged' ) ) . $after;
            } else {
                if ( $show_current ) $breadcrumb .= $before . sprintf( $text['tag'], single_tag_title( '', false ) ) . $after;
            }
        }

        // 作者归档页
        if ( is_author() ) {
            $author = get_userdata( get_query_var( 'author' ) );
            if ( get_query_var( 'paged' ) ) {
                $breadcrumb .= sprintf( $link, "", get_author_posts_url( $author->ID ), sprintf( $text['author'], $author->display_name ));
                $breadcrumb .= $sep . $before . sprintf( $text['page'], get_query_var( 'paged' ) ) . $after;
            } else {
                if ( $show_current ) $breadcrumb .= $before . sprintf( $text['author'], $author->display_name ) . $after;
            }
        }

        // 是否产生404请求错误
        if ( is_404() ) {
            if ( $show_current ) $breadcrumb .= $before . $text['404'] . $after;
        }

        // 有指定类型但不是单独的日志
        if ( has_post_format() && ! is_singular() ) {
            $breadcrumb .= get_post_format_string( get_post_format() );
        }
    }
    $breadcrumb .= $wrap_after;
    echo $breadcrumb;
} // end of yi_breadcrumb()
