<?php 
// 是否是非常规爬虫
if(is_spider()){
    header("Content-type: text/html; charset=utf-8");
    yi_log("这是一个爬虫");
    err(__("请勿采集本站信息，如需转载本站内容请联系站长！", "yii"));//似乎不起作用，调试中
}

//是否为移动终端
function is_mobile() {
    $is_mobile = false;
    $user_agent = strtolower($_SERVER['HTTP_USER_AGENT']);
    yi_log($user_agent);
    $mobile_browser = Array(
        "mqqbrowser", //手机QQ浏览器
        "opera mobi", //手机opera
        "juc","iuc",//uc浏览器
        "fennec",//Firefox的一个小型版本
        "xiaomi","miuibrowser",// 小米手机浏览器
        "mzbrowser",//魅族手机浏览器
        "samsung","sony",//三星、索尼手机浏览器
        "ios","ipad","iphone","ipaq","ipod",//IOS浏览器
        //"applewebKit/4",
        //"applewebkit/5",
        "iemobile", "windows ce","windows phone",//windows phone
        "Lumia","Nexus",
        "240x320","480x640",
        "asus","audio","blackberry","blazer",
        "coolpad" ,"dopod", "etouch", "hitachi","htc","huawei", "jbrowser", "lenovo",
        "lg","lg-","lge-","lge", "mobi","moto","nokia",
        "phone","symbian","tablet","tianyu",
        "wap","xda","xde","zte",
        "mobile",//移动端浏览器
        "android",//安卓浏览器
    );
    foreach ($mobile_browser as $device) {
        if (stristr($user_agent, $device)) {
            $is_mobile = true;
            break;
        }
    }
    return $is_mobile;
}

/**
 * 查找数组中任意项（字符串），是否在另一字符串中出现过
 */
function dstrpos($useragent='', $arr=[]){
    foreach ($arr as $value) {
        if (stristr($useragent, $value)) {
            return true;
            break;
        }
    }
    return false;
}

/**
 * 检测客户端是否是爬虫蜘蛛，防采集(搜索引擎除外)
 * @param  string $useragent 客户端user-agent
 * @return boolean           如果是蜘蛛返回true，否则返回false
 */
function is_spider($useragent = '') {
    $seo_spider = array(
        'baiduspider',
        'baiduspider-mobile-gate',
        'yodaoBot',
        'sogou web spider',
        'googlebot',
        'yahoo! Slurp',
        'yahoo! Slurp China',
        'yahoo ContentMatch Crawler',
        'sogou-Test-Spider',
        'sogou Orion spider',
        'sogou head spider',
        'sogou in spider',
        'twiceler',
        'sosospider',
        'collapsarWEB qihoobot',
        'naverBot',
        'surveyBot',
        'yanga WorldSearch Bot',
        'discobot',
        'ia_archiver',
        'msnbot',
        'sohu-search'
    );
    $kw_spiders = array(
        'bot', 'crawl', 'spider' ,'slurp', 'sohu-search', 'lycos', 'robozilla'
    );
    $kw_browsers = array('msie', 'netscape', 'opera', 'konqueror', 'mozilla');
    $useragent = strtolower(empty($useragent) ? (isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : "") : $useragent);
    //yi_dump($useragent);
    // 判断是否为空AGENT
    if(!$useragent){
        return true;
    }
    // 识别常见搜索引擎，允许搜索引擎抓取内容
    $clint_ip = get_client_ip();
    $clint_hostname = gethostbyaddr($clint_ip);
    if(dstrpos($clint_hostname, $seo_spider)){
        return false;
    }
    // 识别爬虫
    //strpos(string,find,start)查找一个字符串在另一字符串中首次出现的下标，如未出现返回FALSE
    if(strpos($useragent, 'http://') === false && dstrpos($useragent, $kw_browsers)){
        return false;
    }
    if(dstrpos($useragent, $kw_spiders)){
        return true;
    }
    return false;
}
/**
 * 获取客户端真是IP地址
 */
function get_client_ip(){
    $method = array(
        "cons1" => "HTTP_CLIENT_IP",
        "cons2" => "HTTP_X_FORWARDED_FOR",
        "cons3" => "REMOTE_ADDR",
        "vari" => $_SERVER['REMOTE_ADDR'],
    );
    $ip = "unknown";
    foreach ($method as $key => $value) {
        if($key == "vari"){
            if(isset($value) && $value && strcasecmp($value, "unknown")){
                $ip = $value;
                break;
            }
        }else{
            if(getenv($value) && strcasecmp(getenv($value), "unknown")){
                $ip = getenv($value);
                break;
            }
        }
    }
    return $ip;
}
