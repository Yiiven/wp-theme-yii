<?php 
/* 获取当前页面url
 * 要开启全站HTTPS，请前往wp-config.php添加$_SERVER['HTTPS'] = 'ON'
 * 区分大小写哦
/* ---------------- */
function yi_get_current_page_url(){
    $ssl = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'ON') ? true : false;
    $sp = strtolower($_SERVER['SERVER_PROTOCOL']);
    $protocol = substr($sp, 0, strpos($sp, '/')) . (($ssl) ? 's' : '');
    $port  = $_SERVER['SERVER_PORT'];
    $port = ((!$ssl && $port=='80') || ($ssl && $port=='443')) ? '' : ':'.$port;
    $host = isset($_SERVER['HTTP_X_FORWARDED_HOST']) ? $_SERVER['HTTP_X_FORWARDED_HOST'] : (isset($_SERVER['HTTP_HOST']) ? explode(':', $_SERVER['HTTP_HOST'])[0] : $_SERVER['SERVER_NAME']);
    return $protocol . '://' . $host . $port . $_SERVER['REQUEST_URI'];
}
 
/* AJAX登录变量
/* -------------- */
function yi_get_sign_ajax_object(){
    $object = array();
    $object['redirecturl'] = yi_get_current_page_url();
    $object['ajaxurl'] = admin_url('/admin-ajax.php');
    $object['loadingmessage'] = '正在请求中，请稍等...';
    $object_json = json_encode($object);
    return $object_json;
}
 
/* AJAX登录验证 */
/* ------------- */
function yi_ajax_login(){
    $result = array();
    if(isset($_POST['security']) && wp_verify_nonce($_POST['security'], 'signin')){// 安全验证
        $creds = array();
        $creds['user_login'] = $_POST['username'];//用户名
        $creds['user_password'] = $_POST['password'];//密码
        $creds['remember'] = (isset($_POST['remember'])) ? $_POST['remember'] : false;//记住登录开关状态
        $login = wp_signon($creds, true);// 用户登录
        if (!is_wp_error($login)){
            $result['success']  = 1;
            //$result['loggedin'] = 1;// 登录成功
            $result['message']  = __('登录成功', 'yii');
        }else{
            $result['success']  = 0;
            $result['message']  = ($login->errors) ? strip_tags($login->get_error_message()) : esc_html__('请输入正确用户名和密码以登录', 'yii');
        }
    }else{
        $result['success']  = 0;
        $result['message'] = __('安全认证失败，请重试！', 'yii');
    }
    header('content-type: application/json; charset=utf-8');
    echo json_encode($result);
    exit;
}
add_action('wp_ajax_ajaxlogin', 'yi_ajax_login');
add_action('wp_ajax_nopriv_ajaxlogin', 'yi_ajax_login');

/* AJAX注册验证
/* ------------- */
function yi_ajax_register(){
    $result = array();
    if(isset($_POST['security']) && wp_verify_nonce($_POST['security'], 'signup')){
        $user_login = sanitize_user($_POST['username']);
        $user_pass = $_POST['password'];
        $user_email = apply_filters('user_registration_email', $_POST['email']);
        $errors = new WP_Error();
        if(!validate_username($user_login)){
            $errors->add('invalid_username', __('请输入一个有效用户名！', 'yii'));
        }elseif(username_exists($user_login)){
            $errors->add('username_exists', __('换一个吧，这个名字已经被其他人使用了!', 'yii'));
        }elseif(email_exists($user_email)){
            $errors->add('email_exists', __('此邮箱已经注册过了！', 'yii'));
        }
        
        do_action('register_post', $user_login, $user_email, $errors);
        $errors = apply_filters('registration_errors', $errors, $user_login, $user_email);
        if($errors->get_error_code()){
            $result['success']  = 0;
            $result['message']  = $errors->get_error_message();
 
        }else{
            $user_id = wp_create_user($user_login, $user_pass, $user_email);
            if (!$user_id){
                $errors -> add('registerfail', sprintf(__('无法注册，请联系管理员', 'yii'), get_option('admin_email')));
                $result['success']  = 0;
                $result['message']  = $errors->get_error_message();     
            } else{
                update_user_option($user_id, 'default_password_nag', true, true); //Set up the Password change nag.
                wp_new_user_notification($user_id, $user_pass);   
                $result['success']  = 1;
                $result['message']  = esc_html__('注册成功', 'yii');
                //自动登录
                wp_set_current_user($user_id);
                wp_set_auth_cookie($user_id);
                $result['loggedin'] = 1;
            }
 
        }   
    }else{
        $result['success']  = 0;
        $result['message'] = __('安全认证失败，请重试！', 'yii');
    }
    header('content-type: application/json; charset=utf-8');
    echo json_encode($result);
    exit;   
}
add_action('wp_ajax_ajaxregister', 'yi_ajax_register');
add_action('wp_ajax_nopriv_ajaxregister', 'yi_ajax_register');

// 验证码
function yi_register_captcha_verify($user_login, $user_email, $errors){
	if(!isset($_POST['captcha']) || empty($_POST['captcha'])){
		$errors->add('empty_captcha', __('请输入验证码','yii'));
	}else{
		$captcha = strtolower(trim($_POST['captcha']));
		session_start();
		$session_captcha = strtolower($_SESSION['captcha']);
		if($captcha != $session_captcha){
			$errors->add('wrong_captcha', __('请输入正确的验证码','yii'));
		}
	}
}
add_action('register_post','yi_register_captcha_verify',10,3);