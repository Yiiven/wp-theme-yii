<?php 
if(function_exists('register_sidebar'))
{
    $sidebars = array(
        "sidebar_top" => ["name"=>"公共头部", "description"=>""],
        "sidebar_bottom" => ["name"=>"公共底部", "description"=>"需要启用主题的footer才会展示"],
        "sidebar_left" => ["name"=>"侧边栏-左", "description"=>"需要使用携带左边栏的布局才会展示"],
        "sidebar_right" => ["name"=>"侧边栏-右", "description"=>"需要使用携带右边栏的布局才会展示"],
    );
    foreach ($sidebars as $key => $value) {
        register_sidebar(array(
            'id' => $key,
            'name' => $value["name"],
            'description'   => $value["description"],
            'class'         => $key,
            'before_widget' => '<aside class="widget %2$s">',
            'after_widget' => '</aside>',
            'before_title' => '<h3>',
            'after_title' => '</h3>'
        ));
    }
}
