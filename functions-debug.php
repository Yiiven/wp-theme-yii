<?php 
//a function for debug

function yi_dump(...$params){
    //label     标签名
    //strict    是否严谨
    add_action('shutdown', function ($label=null,$strict=true) use ($params) {
        echo "<div style='position: fixed; z-index: 99999; left: 10%; right: 10%; width: 80%; min-height:80px; max-height: 50%; bottom: 0; background-color: #fff; box-shadow: 0 0 8px #000; border:1px solid #ddd; border-radius: 0.5rem 0.5rem 0 0; overflow-y: scroll;'>";
        echo $output_before = "<ol><li style='padding-left: 2rem; vertical-align: middle;'>";
        if(count($params) != 0){
            foreach ($params as $var) {
                $label = ($label === null) ? '' : rtrim($label).' ';
                if (!$strict) {
                    if (ini_get('html_errors')) {
                        $output = print_r($var, true);
                        $output = '<pre>' . $label . htmlspecialchars($output, ENT_QUOTES) . '</pre>';//编码双引号和单引号
                    } else {
                        $output = $label . print_r($var, true);
                    }
                } else {
                    ob_start();
                    var_dump($var);
                    $output = ob_get_clean();
                    if (!extension_loaded('xdebug')) {
                        $output = preg_replace("/\]\=\>\n(\s+)/m", '] => ', $output);
                        $output = '<pre>' . $label . htmlspecialchars($output, ENT_QUOTES) . '</pre>';
                    }
                }
                echo $output;
            }
        }
        echo $output_after = "</li></ol></div>";
    });
}
//$a = (object)[1,2,3];
//$b = ["a","b","c"];
//yi_dump($a,$b);
//yi_dump($b);

/**
 * 浏览器友好的变量输出
 * @param mixed $var 变量
 * @param boolean $echo 是否输出 默认为True 如果为false 则返回输出字符串
 * @param string $label 标签 默认为空
 * @param boolean $strict 是否严谨 默认为true
 * @return void|string
 */
 function yi_dump_two($var, $echo=true, $label=null, $strict=true) {
     $label = ($label === null) ? '' : rtrim($label) . ' ';
     if (!$strict) {
         if (ini_get('html_errors')) {
             $output = print_r($var, true);
             $output = '<pre>' . $label . htmlspecialchars($output, ENT_QUOTES) . '</pre>';
         } else {
             $output = $label . print_r($var, true);
         }
     } else {
         ob_start();
         var_dump($var);
         $output = ob_get_clean();
         if (!extension_loaded('xdebug')) {
             $output = preg_replace("/\]\=\>\n(\s+)/m", '] => ', $output);
             $output = '<pre>' . $label . htmlspecialchars($output, ENT_QUOTES) . '</pre>';
         }
     }
     if ($echo) {
         echo($output);
         return null;
     }else
         return $output;
 }