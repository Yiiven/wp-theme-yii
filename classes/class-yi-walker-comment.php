<?php
/**
 * Custom comment walker for this theme
 *
 * @package WordPress
 * @subpackage yii
 * @since 1.0.0
 * This class outputs custom comment walker for HTML5 friendly WordPress comment and threaded replies.
 */
class yi_walker_comment extends Walker_Comment{

    /**
     * 将评论按照HTML5格式化输出
     *
     * @see wp_list_comments()
     *
     * @param WP_Comment $comment Comment to display.
     * @param int        $depth   Depth of the current comment.
     * @param array      $args    An array of arguments.
     */
    protected function html5_comment($comment, $depth, $args){
        $tag = ('div' === $args['style']) ? 'div' : 'li';

        $comment_author_url = get_comment_author_url($comment);
        $comment_author     = get_comment_author($comment);
        $avatar             = yi_get_avatar($comment->user_id, $comment->comment_author_email, false, $args['avatar_size']);
        $comment_timestamp  = sprintf(__('%1$s %2$s', 'yii'), get_comment_date('', $comment), get_comment_time());

        $comments_html = '';
        $comments_html .= '<'.$tag.' id="comment-'.get_comment_ID().'" '.comment_class($this->has_children ? 'parent' : '', $comment, null, false).'>';
        $comments_html .= '<article id="div-comment-'.get_comment_ID().'" class="comment-body">';
        
        $comments_html .= '<div class="comment-author vcard">';
        if(0 != $args['avatar_size']){
            $comments_html .= $avatar;// 头像
        }
        if(!empty($comment_author_url)){
            $author_text_html = '<strong class="fn '.(yi_is_comment_by_post_author($comment) ? "author" : "").'">'.sprintf('<a href="%s" rel="external nofollow" class="url">', $comment_author_url).$comment_author.'</a>'.'</strong>';
        }else{
            $author_text_html = '<strong class="fn '.(yi_is_comment_by_post_author($comment) ? "author" : "").'">'.$comment_author.'</strong>';
        }
        $comments_html .= sprintf(
            wp_kses(
                __('%s <span class="screen-reader-text">:</span>', 'yii'),
                array('span' => array('class' => array()))
            ),
            $author_text_html
        );// 评论者名称

        $comments_html .= '<span class="comment-meta comment-metadata">';
        $comments_html .= '<a href="'.esc_url(get_comment_link($comment, $args)).'">';
        $comments_html .= '<time datetime="'.get_comment_time('c').'" title="'.$comment_timestamp.'">';
        $comments_html .= yi_get_time_ago($comment_timestamp);
        $comments_html .= '</time>';
        $comments_html .= '</a>';// comment-metadata
        
        $comments_html .= '<span class="comment-reply-box">';
        $comments_html .= get_comment_reply_link(
            array_merge(
                $args,
                array(
                    //'add_below' => 'div-comment',
                    'reply_text'    => __('回复', 'yii'),// 回复链接的文本
                    'login_text'    => __('登录以回复', 'yii'),// 登录回复链接的文本
                    'depth'     => $depth,
                    'max_depth' => $args['max_depth'],
                    'before'    => '<span class="comment-reply">',
                    'after'     => '</span>',
                )
            )
        );
        $comments_html .= yi_edit_comment_link(__('编辑', 'yii'), '<span class="edit-link-sep"> | </span><span class="edit-link">', '</span>');
        $comments_html .= '</span>';// comment-reply end
        $comments_html .= '</span>';// comment-meta end
        $comments_html .= '</div>';// comment-author end

        
        $comments_html .= '<p class="comment-content">';
        $comments_html .= preg_replace('/(max-)?height\:\s*1(\.0)?r?em\;/', '', convert_smilies(get_comment_text()));//完成表情转换，并修正表情尺寸
        $comments_html .= '</p>';//comment-content end
        if('0' == $comment->comment_approved){
            $comments_html .= '<p class="comment-awaiting-moderation"><i class="yi yi-noticelight"></i>'.__('您的评论正在等待审核...', 'yii').'</p>';
        }
        $comments_html .= '</article>';// comment end
        echo $comments_html;
    }
}

// 样式参照http://zmingcx.com/wordpress-5-2-2.html

