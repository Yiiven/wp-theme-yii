<?php 
/**
 * The main template file.
 * @package yii
 */
?>

<?php get_header(); ?>
    <div id="container" class="container">
        <?php get_template_part("template/slider") ?>
        <?php if(have_posts()) : //检查博客是否有日志 ?>
        <main class="main">
            <div class="block-content">
            <?php while(have_posts()) : the_post(); //执行 the_post() 去调取日志 ?>
                <?php get_template_part("template/article") ?>
            <?php endwhile; ?>
            </div>
        </main>
        <div class="page-navigation">
            <?php yi_paged(); ?>
        </div>
        <?php else : //博客没有日志的时候执行 ?>
        <div class="main">
            <div class="post">
                <h2><?php _e('博主很懒，什么也没留下...'); ?></h2>
            </div>
        </div>
        <?php endif; ?>
    </div>
<?php get_footer(); ?>