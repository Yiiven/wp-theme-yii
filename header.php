<?php
    $origin = 'https://www.yiven.vip/,https://yiven.vip/';
    header('Access-Control-Allow-Origin:' . $origin);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <title>
        <?php echo trim(wp_title(' | ', 0, 'right')); ?>
        <?php bloginfo('name'); ?> <?php the_yi('connector'); ?> <?php bloginfo('description'); ?>
        <?php
            $paged = get_query_var('paged'); 
            if ($paged > 1) {    
                printf(' - 第%s页', $paged);   
            }
        ?>
    </title>
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <?php
    $keywords = _yi("seo_keywords");
    $description = _yi("seo_description");
    if(_yi('seo_single') && (is_single()||is_page())){
        $tags = "";
        $post_tags = get_the_tags();
        $i = 0;
        if(!empty($post_tags)){
            foreach ($post_tags as $tag){
                if($i==0){
                    $tags .= $tag->name;
                }else{
                    $tags .= ",".$tag->name;
                }
                $i++;
            }
        }
        $excerpt = trim(get_the_excerpt());
        if(!_yi('seo_single_usedefault')){
            $keywords = $tags;
            $description = $excerpt;
        }else{
            $keywords = ($tags=="" ? "" : $tags.",").$keywords;
            $description = ($excerpt=="" ? "" : $excerpt." - ").$description;
        }
    }
    ?>
    <meta name="keywords" content="<?php echo $keywords; ?>">
    <meta name="description" content="<?php echo $description; ?>">
    <meta name="author" content="Yiven" />
    <link href="<?php bloginfo('stylesheet_url'); ?>" rel="stylesheet" type="text/css">
    <?php
        if(_yi('site_total')){
            echo stripslashes(_yi('site_total_code'));
        }
    ?>
    <?php wp_head(); ?>
</head>
<body>
<div id="wrapper">
    <div id="header">
        <div id="header-main" class="header-main">
            <?php if(_yi("display_header_nav")): ?>
                <?php get_template_part( 'template/header-nav' ); ?>
            <?php endif; ?>
            <div id="menu-container" class="menu-container">
                <div id="navigation-top" class="navigation-top">
                    <span id="navigation-toggle" class="sidr-show">
                        <i class="yi yi-menu"></i>
                    </span>
                    <div class="logo">
                        <p class="site-title">
                            <a href="<?php bloginfo('url');//网站地址 ?>" alt="<?php bloginfo('name');?>" rel="home">
                                <span class="logo-small">
                                    <img id="logo" src="<?php _yi('logo_image') ? the_yi('logo_image') : (bloginfo('template_url').'/images/logo.png'); ?>" alt="<?php bloginfo('name');?>">
                                </span>
                                <?php bloginfo('name');//站点名称 ?>
                            </a>
                        </p>
                        <p class="site-description clear-small"><?php bloginfo('description');//站点描述 ?></p>
                    </div>
                    <?php if(_yi("display_search")){ ?>
                    <span class="nav-search">
                        <i class="yi yi-search"></i>
                    </span>
                    <?php } ?>
                    <span id="right-sider-toggle" class="right-sider-show">
                        <i class="yi yi-menu"></i>
                    </span>
                    <div id="site-nav-wrap">
                        <nav id="site-nav" class="main-nav">
                            <div class="main-menu-container">
                                <?php
                                wp_nav_menu(
                                    array(
                                        'theme_location'=>'top-menu', //菜单别名
                                        'depth'=>0, //菜单层级，0-全部
                                        'container'  => 'div',  //容器标签
                                        'container_id'  => '',  //ul父节点id值
                                        'container_class'=>'main-nav',//ul父节点className属性
                                        'menu'   => '', //期望显示的菜单
                                        'menu_id'   => 'main-menu',  //ul节点的id属性
                                        'menu_class'=>'down-menu nav-menu',//ul节点className属性
                                        'echo'  => true,//是否输出菜单，默认为真
                                        'fallback_cb' => 'wp_page_menu',  //菜单不存在时，返回默认菜单，设为false则不返回
                                        'before' => '', //链接前文本
                                        'after'  => '', //链接后文本
                                        'link_before'  => '',   //链接文本前
                                        'link_after'  => '',//链接文本后
                                        'items_wrap'  => '<ul id="%1$s" class="%2$s">%3$s</ul>',   //如何包装列表
                                        'walker' => ''  //自定义walker
                                    )
                                );
                                ?>
                            </div>
                        </nav>
                    </div>
                </div>
            </div>
        </div> 
    </div>
    <?php if(_yi("display_search")){ ?>
    <div id="search-main">
        <div class="search-off-btn"></div>
        <div class="search-off search-off-top"></div>
        <?php
            if(_yi("enable_baidu_searchbar")){
                get_template_part( 'template/search-all' );
            }else{
                get_template_part( 'template/search-local' );
            }
        ?>

        <div class="clear"></div>
        <div class="search-off search-off-bottom">
            <div class="clear"></div>
        </div>
    </div>
    <?php } ?>
    <div class="breadcrumb">
    <?php if(_yi("display_breadcrumb_nav")): ?>
        <?php //get_template_part( 'template/breadcrumb-nav' ); ?>
        <?php if (function_exists('yi_breadcrumb')) {yi_breadcrumb();} ?>
    <?php endif; ?>
    </div>
    <div id="header-widget-box" class="site-header">
        <?php if ( is_active_sidebar( 'sidebar_top' ) ) :
            dynamic_sidebar( 'sidebar_top' );
        endif; ?>
    </div>
    <div id="content">
    <?php get_template_part("template/sidebar-left") ?>