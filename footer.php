    <?php get_template_part("template/sidebar-right"); ?>
    </div>
    <div id="sidr-main" class="sidr left">
        <div class="sidr-inner">
            <span class="sidr-close"><i class="yi yi-cross"></i></span>
            <div class="sidr-user-profile mobile-login mobile-login-l show-layer" data-show-layer="login-layer" role="button">
                <?php if(is_user_logged_in()){ ?>
                    <?php if(current_user_can('manage_options')){//要修改成多个角色的判断 ?>
                    <?php } ?>
                        <span class="manage">
                            <i class="yi yi-home"></i>
                            <a href="<?php echo admin_url(); ?>" <?php echo yi_target_blank() ?>><?php echo yi_get_admin_title(); ?></a>
                        </span>
                        <span class="logout">
                            <i class="yi yi-shutdown"></i>
                            <a href="<?php echo wp_logout_url(); ?>">登出</a>
                        </span>
                <?php }else{ ?>
                    <span class="login">
                        <i class="yi yi-timerauto"></i>
                        <span class="mobile-login-t">登录/注册</span>
                    </span>
                <?php } ?>
            </div>
        </div>
        <?php
        wp_nav_menu(
            array(
                'theme_location'=>'mobile-menu', //菜单别名
                'depth'=>0, //菜单层级，0-全部
                'container'  => 'div',  //容器标签
                'container_id'  => '',  //ul父节点id值
                'container_class'=>'sidr-inner',//ul父节点className属性
                'menu'   => '', //期望显示的菜单
                'menu_id'   => 'sidr-menu',  //ul节点的id属性
                'menu_class'=>'down-menu nav-menu',//ul节点className属性
                'echo'  => true,//是否输出菜单，默认为真
                'fallback_cb' => 'wp_page_menu',  //菜单不存在时，返回默认菜单，设为false则不返回
                'before' => '', //链接前文本
                'after'  => '', //链接后文本
                'link_before'  => '',   //链接文本前
                'link_after'  => '',//链接文本后
                'items_wrap'  => '<ul id="%1$s" class="%2$s">%3$s</ul>',   //如何包装列表
                'walker' => ''  //自定义walker
            )
        );
        ?>
    </div>
    <div id="footer-widget-box" class="site-footer">
        <div class="footer-widget">
            <?php if ( is_active_sidebar( 'sidebar_bottom' ) ) :
                dynamic_sidebar( 'sidebar_bottom' );
            endif; ?>
            <div class="clear"></div>
        </div>
    </div>
    <footer id="footer" class="site-footer">
        <div class="site-info">
            <?php get_template_part( 'template/copy' ); ?>
        </div>
    </footer>
</div>
<?php get_template_part( 'template/login' ); ?><!-- 登录框 -->
<?php get_template_part( 'template/scroll' ); ?><!-- 回顶部 -->

<script type="text/javascript">
    /* <![CDATA[ */
    var ajax_sign_object = <?php echo yi_get_sign_ajax_object(); ?>;
    var ajax_theme_object = <?php echo yi_get_theme_ajax_object(); ?>;
    /* ]]> */
</script>
<?php wp_footer(); ?>
<?php if(!(is_home() || is_front_page())): ?>
<script type="text/javascript">
    jQuery(document).ready(function() {
        function unfocus(){
    	    if (location.href != "<?php bloginfo('url'); ?>"){
        	    document.title = document[typehidden] ? "您确定不进来看看吗？ - [" + title + "]" : title
        	}
        }
        var typehidden, typechange, title = document.title;
        "undefined" != typeof document.hidden 
        	? (typehidden = "hidden", typechange = "visibilitychange") 
        	: "undefined" != typeof document.mozHidden 
        		? (typehidden = "mozHidden", typechange = "mozvisibilitychange") 
        		: "undefined" != typeof document.webkitHidden 
        			&& (typehidden = "webkitHidden", typechange = "webkitvisibilitychange");
        "undefined" == typeof document.addEventListener 
        	&& "undefined" == typeof document[typehidden] || document.addEventListener(typechange, unfocus, false)
    });
</script>
<?php endif; ?>
</body>
</html>