<?php
define('YI_ROOT_PATH', dirname(dirname(dirname(dirname(dirname(__FILE__))))));

require(YI_ROOT_PATH.'/wp-blog-header.php');
$posts_to_show = 1000;

class sitemap{
    private $use_www;
    private $use_https;
    private $url = "";
    private static $_instance = null;

    public function __construct(){
        $this -> use_www = _yi("seo_sitemap_www");
        $this -> use_https = _yi("seo_sitemap_ssl");
    }

    public static function url($url) {
        if (!isset(self::$_instance)) {
            self::$_instance = new self();
        }
        self::$_instance -> url = $url;
        return self::$_instance;
    }
    
    public function add_www(){
        if($this -> use_www){
            $www_used = preg_match('/http(s)?:\/\/www./', $this -> url);
            if(!$www_used){
                $this -> url = preg_replace('/(http(s)?:\/\/)/', "\$1www.", $this -> url);
            }
        }
        return $this;
    }

    public function add_https(){
        if($this -> use_https){
            $https_used = preg_match('/https:\/\//', $this -> url);
            if(!$https_used){
                $this -> url = preg_replace('/(http)(:\/\/)/', "\$1s\$2", $this -> url);
            }
        }
        return $this;
    }

    public function res(){
        return $this -> url;
    }

    public function __destruct(){
        
    }
}
/*
<?xml version="1.0" encoding="UTF-8"?>
<!-- XML文件需以utf-8编码-->
<!--必填标签-->
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:mobile="http://www.baidu.com/schemas/sitemap-mobile/1/">
<!-- generated-on=<?php echo get_lastpostdate('blog'); ?> -->
    <!--必填标签,这是具体某一个链接的定义入口，每一条数据都要用<url>和</url>包含在里面，这是必须的 -->
    <url>
        <!--必填,URL链接地址,长度不得超过256字节-->
        <loc></loc>
        <!--选填,用来指定该链接的最后更新时间-->
        <lastmod></lastmod>
        <!--选填,用来告知此链接可能会出现的更新频率：always-每次 ，hourly-每小时 ，daily-每天 ，weekly-每周 ，monthly-每月 ，yearly-每年 ，never-从不  -->
        <changefreq></changefreq>
        <!--选填,用来指定此链接相较于其他链接的优先权，取值在0.0-1.0之间-->
        <priority></priority>
    </url>
</urlset>

array(
    array(
        "loc" => "",
        "lastmod" => "",
        "changefreq" => "",
        "priority" => "",
    ),
);
*/

$map = array();
// 主页
$last_update_time = get_lastpostmodified('GMT');
$last_update_time = gmdate('Y-m-d\TH:i:s+00:00', strtotime($last_update_time));
$arr_home = array(
    "loc" => sitemap::url(get_home_url())->add_www()->add_https()->res(),
    "lastmod" => $last_update_time,
    "changefreq" => 'daily',
    "priority" => '1.0',
);
array_push($map, $arr_home);

// 文章页-single
$myposts = get_posts("numberposts=".$posts_to_show);
foreach($myposts as $post){
    $arr_posts = array(
        "loc" => sitemap::url(get_the_permalink())->add_www()->add_https()->res(),
        "lastmod" => get_the_time('c'),
        "changefreq" => 'monthly',
        "priority" => '0.6',
    );
    array_push($map, $arr_posts);
} // 文章页-END

// 独立页-page
$mypages = get_pages();
if(count($mypages) > 0) {
    foreach($mypages as $page) { 
        $arr_pages = array(
            "loc" => sitemap::url(get_page_link($page->ID))->add_www()->add_https()->res(),
            "lastmod" => str_replace(" ","T",get_page($page->ID)->post_modified).'+00:00',
            "changefreq" => 'weekly',
            "priority" => '0.6',
        );
        array_push($map, $arr_pages);
    }
} // 独立页-END

// 分类-category
$terms = get_terms('category', 'orderby=name&hide_empty=0' );
$count = count($terms);
if($count > 0){
    foreach ($terms as $term) {
        $arr_terms = array(
            "loc" => sitemap::url(get_term_link($term, $term->slug))->add_www()->add_https()->res(),
            "changefreq" => 'weekly',
            "priority" => '0.8',
        );
        array_push($map, $arr_terms);
    }
} // 分类-END

// 标签-tag(可选)
$tags = get_terms("post_tag");
foreach ($tags as $key => $tag) {
    $link = get_term_link(intval($tag->term_id), "post_tag");
    if (is_wp_error($link)){return false;}
    $tags[ $key ]->link = $link;
    $arr_tags = array(
        "loc" => sitemap::url($link)->add_www()->add_https()->res(),
        "changefreq" => 'monthly',
        "priority" => '0.4',
    );
    array_push($map, $arr_tags);
} // 标签-END
/**/
$sitemap = new DOMDocument('1.0', 'utf-8');//创建XML文件时需要设置版本号version="1.0" encoding="utf-8"和编码格式
$sitemap -> formatOutput = true;//格式化输出
$urlset = $sitemap -> createElement('urlset');//创建一个元素节点
$urlset -> setAttribute('xmlns', 'http://www.sitemaps.org/schemas/sitemap/0.9');
$urlset -> setAttribute('xmlns:mobile', 'http://www.baidu.com/schemas/sitemap-mobile/1/');
foreach($map as $key => $value){
    $url = $sitemap -> createElement('url');
    $urlset -> appendChild($url);
    $loc = $sitemap -> createElement('loc', $value['loc']);
    $url -> appendChild($loc);
    if(isset($value['lastmod'])){
        $lastmod = $sitemap -> createElement('lastmod', $value['lastmod']);
        $url -> appendChild($lastmod);
    }
    if(isset($value['changefreq'])){
        $changefreq = $sitemap -> createElement('changefreq', $value['changefreq']);
        $url -> appendChild($changefreq);
    }
    if(isset($value['priority'])){
        $priority = $sitemap -> createElement('priority', $value['priority']);
        $url -> appendChild($priority);
    }
}
$sitemap -> appendChild($urlset);
$result = $sitemap -> save(YI_ROOT_PATH.'/sitemap.xml');
if(!$result){
    echo __("网站地图生成失败", "yii");
}else{
    $total = 1 + count($myposts) + count($mypages) + count($terms) + count($tags);
    echo __('网站地图生成成功，共计'.$total.'条，<a href="'.home_url('sitemap.xml').'" target="_blank">点击查看生成的内容</a>', 'yii');
}
?>