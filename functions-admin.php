<?php 
require_once(get_stylesheet_directory()."/classes/class-tgm-plugin-activation.php");
require (get_stylesheet_directory().'/admin/tgmpa.php');
require (get_stylesheet_directory().'/admin/theme-options.php');//add theme options setting for admin 
// 隐藏帮助浮动选项卡
if(_yi("hide_help")){
    add_filter('in_admin_header', 'yi_hide_help', 10, 3);
}
function yi_hide_help(){
    // global $current_screen;
    $screen = get_current_screen();
    $screen->remove_help_tabs();
}
// 隐藏显示选项浮动选项卡
if(_yi("hide_screen_options")){
    add_filter('screen_options_show_screen', 'yi_hide_screen_options');
}
function yi_hide_screen_options(){
    return false;
}
// 变更管理中心页脚文字
add_filter('admin_footer_text', 'yi_change_footer_admin', 999);
function yi_change_footer_admin () {  
    return 'welcome to use yii theme';  
}  
// 变更管理中心页脚版本号
add_filter( 'update_footer', 'yi_change_footer_version', 999);
function yi_change_footer_version() {  
    return version;  
}  

// 移除管理中心WordPresslogo图标
add_action('wp_before_admin_bar_render', 'yi_remove_admin_bar', 0);
function yi_remove_admin_bar() {  
    global $wp_admin_bar;
    $wp_admin_bar -> remove_menu('wp-logo');  
}  

// 文件上传自动重命名为"年月日时分秒"
add_filter('wp_handle_upload_prefilter', 'yi_rename_upload_file', 5, 1);
function yi_rename_upload_file($file){
    /**
    $file = array(
        "name" => "filename.jpg" 
        "type" => "image/jpeg" 
        "tmp_name" => "C:\Windows\phpA217.tmp" 
        "error" => false
        "size" => 51096
    )
    */
    $time = date("YmdHis");
    $file['name'] = $time."".mt_rand(1,100).".".pathinfo($file['name'] , PATHINFO_EXTENSION);
    return $file;
}
// 设置附件属性，对齐方式，链接类型，缩略图尺寸
if(_yi('aattachment_display_custom')){
    add_action('after_setup_theme', 'yi_set_attachment_display_custom');
}else{
    add_action('after_setup_theme', 'yi_set_attachment_display_default');
}
function yi_set_attachment_display_custom() {
    update_option('image_default_align', 'center'); //可选值[left:居左],[right:居右],[center:居中],[none:无]
    update_option('image_default_link_type', 'file'); //可选值[file:媒体文件本身],[post:附件页面，即文章地址],[custom:自定义URL],[none:无]
    update_option('image_default_size', 'full'); //可选值[thumbnail:缩略图尺寸],[medium:中等尺寸],[full:完整尺寸]
}
function yi_set_attachment_display_default(){
    update_option('image_default_align', 'left');
    update_option('image_default_link_type', 'none');
    update_option('image_default_size', 'thumbnail');
}

// 管理中心只显示作者自己的文章
add_filter('parse_query', 'yi_parse_query_useronly');
function yi_parse_query_useronly($wp_query) {
    if (strpos($_SERVER['REQUEST_URI'],'/wp-admin/edit.php') !== false ) {
        if (!current_user_can('level_10')) {
            global $current_user;
            $wp_query->set('author', $current_user->id);
        }
    }
}