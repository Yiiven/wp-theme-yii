<?php 
/**
 * The template for displaying single.
 * @package yii
 * 日志(文章)页
 */
 ?>
<?php get_header(); ?>
    <div class="container single-container">
        <?php if(have_posts()) : the_post(); //have_posts()检查博客是否有日志，the_post() 去调取日志 ?>
            <div class="main">
                <article id="post-<?php the_ID(); //调取日志ID ?>" class="post single">
                    <div class="entry">
                        <div class="article-inner">
                            <header class="entry-header">
                                <h2><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h2>
                                <div class="entry-meta">
                                    <span class="date"><i class="yi yi-date"></i><?php the_time('Y-m-d H:i:s'); ?></span>
                                    <span class="author"><i class="yi yi-author"></i><?php the_author();//文章作者 ?></span>
                                    <span class="cate"><i class="yi yi-cate"></i><?php the_category('/');//文章分类，多个分类用"指定符号"分割 ?></span>
                                    <span class="tag"><i class="yi yi-tag"></i><?php the_tags('<span>', '/', '</span>') ?></span>
                                    <span class="comment"><a href="<?php the_permalink(); ?>#comment"><i class="yi yi-comment"></i><?php comments_number("0", "1", "%");//评论数统计 ?></a></span>
                                    <span class="view"><i class="yi yi-eye"></i><?php echo yi_get_post_views($post -> ID); ?></span>
                                </div>
                                <div class="entry-count">
                                    <?php echo yi_count_words_read_time(); ?>
                                </div>
                            </header><!-- /header -->
                            <div class="entry-summary">
                                <?php the_content();//全文 ?>
                                <?php wp_link_pages('页码：', '', 'number');//当在页面中使用<!--nextpage-->进行分页时，下方显示页码 ?>
                            </div>
                            <div class="clear"></div>
                            <div class="post-continue-container">
                                <?php get_template_part("template/shang"); ?>
                            </div>
                        </div>
                    </div>
                    <?php get_template_part("template/single-nav"); ?>
                    <div id="comments" class="comments-area">
                        <?php comments_template();//评论模板 ?>
                    </div>
                </article>
            </div>
            <?php else : //博客没有日志的时候执行 ?>
            <div class="main">
                <div class="post">
                    <h2><?php _e('哎呀，你找的文章似乎已经不在地球上啦...'); ?></h2>
                </div>
            </div>
        <?php endif; ?>
    </div>
<?php get_footer(); ?>