<?php 
/**
 * The template for displaying the landing page/blog posts
 *
 * @package yii
 */

/*
if(is_home()){
    echo "动态首页";
}

if(is_front_page()){
    echo "静态首页";
}
*/

$landingpage = _yi('landingpage');//获取引导页设置（是否开启独立引导页）
//没有开启独立引导页
if (is_page() && !$landingpage) { 
    load_template(get_page_template());
    return true;
}
//显示首页内容
?>
<?php if ('posts' == get_option('show_on_front')): ?>
    <?php include(get_home_template()); ?>
<?php else: ?>
    <?php get_header(); ?>
        <div id="container" class="container front-container">
            <?php get_template_part("template/slider") ?>
            <main class="main">
                <?php if(have_posts()): //检查博客是否有日志 ?>
                    <?php if(_yi("show_sticky")){ ?>
                    <div class="block-title">
                        <div class="title textEllipsis" data-tipso="推荐阅读">推荐阅读</div>
                        <div class="more"><a target="_blank" href="#"></a></div>
                        <div class="line left-line"></div>
                        <div class="line right-line"></div>
                    </div>
                    <div class="block-content">
                    <?php get_template_part("template/sticky-posts") ?>
                    </div>
                    <?php } ?>
                <?php endif; ?>
                <?php get_template_part("template/f-twocat") ?>
                <?php get_template_part("template/f-tabs") ?>
            </main>
        </div>
    <?php get_footer(); ?>
<?php endif; ?>