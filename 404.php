<?php get_header(); ?>
    <div class="container 404-container">
        <main class="main page-main">
            <section class="error-404 not-found page">
                <header class="entry-header">
                    <h1 class="page-title"><?php echo stripslashes(_yi('404_title')); ?></h1>
                </header><!-- .page-header -->

                <div class="content page-content">
                    <div class="align-center">
                        <span class="domain"><?php echo preg_replace('/http(s)?\:\/\//iU', '', get_bloginfo('url')); ?></span>
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/404.png">
                        <p>
                            <?php echo stripslashes(_yi('404_content')); ?>
                        </p>
                    </div>
                    <?php get_search_form(); ?>
                    <br>
                    <br>
                    <br>
                </div><!-- .page-content -->
            </section><!-- .error-404 -->
        </main><!-- .site-main -->
    </div><!-- .content-area -->
<?php get_footer(); ?>