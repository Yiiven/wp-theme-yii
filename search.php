<?php 
/**
 * The template for displaying search.
 * @package yii
 * 搜索结果页
 */
 ?>
<?php get_header(); ?>
    <div id="container" class="container search-container">
        <?php if(have_posts()) : //检查博客是否有日志 ?>
        <main class="main">
            <div class="block-title">
                <div class="title textEllipsis">搜索结果</div>
                <div class="line left-line"></div>
                <div class="line right-line"></div>
            </div>
            <div class="block-content">
            <?php while(have_posts()) : the_post(); //执行 the_post() 去调取日志 ?>
                <?php get_template_part("template/article") ?>
            <?php endwhile; ?>
            </div>
        </main>
        <div class="page-navigation">
            <?php yi_paged(); ?>
        </div>
        <?php else : //博客没有日志的时候执行 ?>
        <div class="main">
            <div class="block-title">
                <div class="title textEllipsis">搜索结果</div>
                <div class="line left-line"></div>
                <div class="line right-line"></div>
            </div>
            <div class="block-content">
                <h2><?php _e('似乎没有找到您想要的呢...'); ?></h2>
            </div>
        </div>
        <?php endif; ?>
    </div>
<?php get_footer(); ?>