<?php
/**
 * This file represents an example of the code that themes would use to register
 * the required plugins.
 *
 * It is expected that theme authors would copy and paste this code into their
 * functions.php file, and amend to suit.
 *
 * @see http://tgmpluginactivation.com/configuration/ for detailed documentation.
 *
 * @package    yii
 */

add_action( 'tgmpa_register', 'yi_register_required_plugins' );

/**
 * Register the required plugins for this theme.
 *
 * In this example, we register five plugins:
 * - one included with the TGMPA library
 * - two from an external source, one from an arbitrary source, one from a GitHub repository
 * - two from the .org repo, where one demonstrates the use of the `is_callable` argument
 *
 * The variables passed to the `tgmpa()` function should be:
 * - an array of plugin arrays;
 * - optionally a configuration array.
 * If you are not changing anything in the configuration array, you can remove the array and remove the
 * variable from the function call: `tgmpa( $plugins );`.
 * In that case, the TGMPA default settings will be used.
 *
 * This function is hooked into `tgmpa_register`, which is fired on the WP `init` action on priority 10.
 */
function yi_register_required_plugins() {
	/*
	 * Array of plugin arrays. Required keys are name and slug.
	 * If the source is NOT from the .org repo, then source is also required.
	 */
	$plugins = array(

		array(
			'name'               => 'Classic Editor', // 插件名称.
			'slug'               => 'classic-editor', // 插件别名(一般是文件夹名称).
			'required'           => false, // false-推荐，非必须，true-必须.
			'version'            => '1.5', // 如果已设置，则此插件必须是该版本或更高版本. 
			'force_activation'   => false, // 如果为true，则插件在主题激活时被激活，并且在主题切换之前不能被禁用.
			'force_deactivation' => false, // 如果为true，则在主题切换时停用插件，这对于特定于主题的插件很有用.
		),
		array(
			'name'               => 'TinyMCE Advanced', // The plugin name.
			'slug'               => 'tinymce-advanced', // The plugin slug (typically the folder name).
			'required'           => false, // If false, the plugin is only 'recommended' instead of required.
			'version'            => '5.2.1', // If set, the active plugin must be this version or higher. 
			'force_activation'   => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch.
			'force_deactivation' => false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins.
		),
		array(
			'name'               => 'Simple Post Series',
			'slug'               => 'simple-post-series',
			'required'           => false,
			'version'            => '2.4.4',
			'force_activation'   => false,
			'force_deactivation' => false,
		),
		array(
			'name'               => 'WPJAM Basic', // WPJAM Basic使用了PHP7.2的新特性，你需要确认你的服务器使用的是PHP7.2+.
			'slug'               => 'wpjam-basic',
			'required'           => false,
			'version'            => '3.7.7',
			'force_activation'   => false,
			'force_deactivation' => false,
		),

	);

	/*
	 * Array of configuration settings. Amend each line as needed.
	 *
	 * TGMPA will start providing localized text strings soon. If you already have translations of our standard
	 * strings available, please help us make TGMPA even better by giving us access to these translations or by
	 * sending in a pull-request with .po file(s) with the translations.
	 *
	 * Only uncomment the strings in the config array if you want to customize the strings.
	 */
	$config = array(
		'id'           => 'yii',                 // Unique ID for hashing notices for multiple instances of TGMPA.
		'default_path' => '',                      // Default absolute path to bundled plugins.
		'menu'         => 'yii-addons', // Menu slug.
		'has_notices'  => true,                    // Show admin notices or not.
		'dismissable'  => true,                    // If false, a user cannot dismiss the nag message.
		'dismiss_msg'  => '',                      // If 'dismissable' is false, this message will be output at top of nag.
		'is_automatic' => false,                   // Automatically activate plugins after installation or not.
		'message'      => '',                      // Message to output right before the plugins table.

		/**/
		'strings'      => array(
			'page_title'                      => __( 'yii推荐安装的插件', 'yii' ),
			'menu_title'                      => __( '插件安装', 'yii' ),
			// translators: %s: plugin name.
			'installing'                      => __( '正在安装插件: %s', 'yii' ),
			// translators: %s: plugin name.
			'updating'                        => __( '插件更新: %s', 'yii' ),
			'oops'                            => __( '插件API出了问题.', 'yii' ),
			'notice_can_install_required'     => _n_noop(
				// translators: 1: plugin name(s).
				'此主题需要以下插件: %1$s.',
				'此主题需要以下插件: %1$s.',
				'yii'
			),
			'notice_can_install_recommended'  => _n_noop(
				// translators: 1: plugin name(s).
				'此主题推荐以下插件: %1$s.',
				'此主题推荐以下插件: %1$s.',
				'yii'
			),
			'notice_ask_to_update'            => _n_noop(
				// translators: 1: plugin name(s).
				'以下插件需要更新到其最新版本，以确保与此主题的最大兼容性: %1$s.',
				' 以下插件需要更新到其最新版本，以确保与此主题的最大兼容性 : %1$s.',
				'yii'
			),
			'notice_ask_to_update_maybe'      => _n_noop(
				// translators: 1: plugin name(s).
				'有可用的更新: %1$s.',
				'以下插件有可用的更新: %1$s.',
				'yii'
			),
			'notice_can_activate_required'    => _n_noop(
				// translators: 1: plugin name(s).
				'以下必需的插件当前处于非活动状态: %1$s.',
				'以下必需的插件当前处于非活动状态: %1$s.',
				'yii'
			),
			'notice_can_activate_recommended' => _n_noop(
				// translators: 1: plugin name(s).
				'以下推荐的插件当前处于非活动状态: %1$s.',
				'以下推荐的插件当前处于非活动状态: %1$s.',
				'yii'
			),
			'install_link'                    => _n_noop(
				'开始安装插件',
				'开始安装插件',
				'yii'
			),
			'update_link' 					  => _n_noop(
				'开始更新插件',
				'开始更新插件',
				'yii'
			),
			'activate_link'                   => _n_noop(
				'开始启用插件',
				'开始启用插件',
				'yii'
			),
			'return'                          => __( '返回所需的插件安装程序', 'yii' ),
			'plugin_activated'                => __( '插件启用成功。', 'yii' ),
			'activated_successfully'          => __( '以下插件启用成功:', 'yii' ),
			// translators: 1: plugin name.
			'plugin_already_active'           => __( '没有采取行动。插件%1$s已处于活动状态。', 'yii' ),
			// translators: 1: plugin name.
			'plugin_needs_higher_version'     => __( '插件未激活。此主题需要%s的更高版本。请更新插件。', 'yii' ),
			// translators: 1: dashboard link.
			'complete'                        => __( '所有插件均已安装并激活。 %1$s', 'yii' ),
			'dismiss'                         => __( '驳回本通知', 'yii' ),
			'notice_cannot_install_activate'  => __( '有一个或多个必需或推荐的插件可安装、更新或激活。', 'yii' ),
			'contact_admin'                   => __( '请与此网站的管理员联系以获取帮助。', 'yii' ),

			'nag_type'                        => '', // Determines admin notice type - can only be one of the typical WP notice classes, such as 'updated', 'update-nag', 'notice-warning', 'notice-info' or 'error'. Some of which may not work as expected in older WP versions.
		),
		
	);

	tgmpa( $plugins, $config );
}
