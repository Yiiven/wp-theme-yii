<?php
// 主题选项配置项目
function yi_theme_option_setting() {
    $options = array();

//--------------------公共参数---------------------------
    // 主题默认图片文件目录
    $imagepath = get_template_directory_uri()."/images";
    // 将所有分类（categories）加入数组
    $options_categories = array();
    $options_categories_obj = get_categories();
    foreach ($options_categories_obj as $category) {
        $options_categories[$category->cat_ID] = $category->cat_name;
    }

//--------------------分组---------------------------
    $options[] = array(
        "name" => __("基本设置","yii"),
        "type" => "heading", // 每当遇到heading时，将创建新的选项卡
    );
    $options[] = array(
        "id" => "landingpage", 
        "name" => __("启用引导页(独立主页)","yii"),
        "type" => "checkbox",
        "std" => true,
        "desc" => __("开启（需要设置为静态首页才生效）","yii"),
    );
    $options[] = array(
        "id" => "logo_image", 
        "name" => __("标志","yii"),
        "type" => "upload",
        "std" => $imagepath."/logo-x.png",
        "notice" => __("建议尺寸：60x60，建议格式：png","yii"),
    );
    $options[] = array(
        'id' => 'connector',
        'name' => __('全站连接符', 'yii'),
        'class' => 'mini',
        'type' => 'text',
        'std' => _yi('connector') ? _yi('connector') : '-',
        'notice' => __('设置后切勿随意更改(对SEO不友好)，一般为"-"或"_"', 'yii'),
    );
    $options[] = array(
        "id" => "target_blank", 
        "name" => __("在新窗口打开链接","yii"),
        "type" => "checkbox",
        "std" => true,
        "desc" => __("开启","yii"),
    );
    $options[] = array(
        "id" => "display_header_nav", 
        "name" => __("显示顶部导航条","yii"),
        "type" => "checkbox",
        "std" => true,
        "desc" => __("开启","yii"),
    );
    $options[] = array(
        "id" => "site_total", 
        "name" => __("站点统计","yii"),
        "type" => "checkbox",
        "std" => true,
        "desc" => __("开启","yii"),
        "notice" => __("启用站点统计后，需要在下方填写您的统计代码","yii"),
    );
    $options[] = array(
        "id" => "site_total_code", 
        "name" => __("统计代码","yii"),
        "type" => "textarea",
        "std" => _yi('site_total_code') ? _yi('site_total_code') : "",
        "desc" => __("填写您的统计代码（支持百度统计等）","yii"),
    );
    /**
    <script>
        var _hmt = _hmt || [];
        (function(){
            var hm = document.createElement("script");
            hm.src = "https://hm.baidu.com/hm.js?8ff50eefeb1629e70c89e0398bf4f1ef";
            var s = document.getElementsByTagName("script")[0]; 
            s.parentNode.insertBefore(hm, s);
        })();
    </script>
    */

//--------------------分组---------------------------
    $options[] = array(
        "name" => __("SEO设置","yii"),
        "type" => "heading", // 每当遇到heading时，将创建新的选项卡
    );
    $options[] = array(
        "id" => "seo_sitemap_ssl",
        "name" => __("网站地图（SSL）","yii"),
        "type" => "checkbox",
        "std" => false,
        "desc" => "在sitemap中统一使用https://"
    );
    $options[] = array(
        "id" => "seo_sitemap_www",
        "name" => __("网站地图（WWW）","yii"),
        "type" => "checkbox",
        "std" => true,
        "desc" => "在sitemap中使用“www”二级域名"
    );
    $options[] = array(
        "id" => "seo_sitemap_create",
        "name" => __("生成网站地图（sitemap.xml）","yii"),
        "type" => "request",
        "std" => get_template_directory_uri()."/app/sitemap.php",
    );
    $options[] = array(
        "id" => "seo_keywords", 
        "name" => __("关键词","yii"),
        "type" => "text",
        "std" => _yi("seo_keywords") ? _yi("seo_keywords") : "yii",
        "desc" => __("多个关键词之间使用英文“,”逗号分隔","yii"),
    );
    $options[] = array(
        "id" => "seo_description", 
        "name" => __("页面描述","yii"),
        "type" => "textarea",
        "std" => _yi("seo_description") ? _yi("seo_description") : "本站基于主题“yii”，如有需要，可访问https://www.yiven.vip获取",
        "desc" => __("一段简短的描述性文本","yii"),
    );
    $options[] = array(
        "id" => "seo_single", 
        "name" => __("文章页使用独立的SEO","yii"),
        "type" => "checkbox",
        "std" => true,
        "desc" => __("启用","yii"),
        "notice" => __("文章页若使用独立的SEO，关键词将被tag标签替代，描述信息将使用文章摘要","yii"),
    );
    $options[] = array(
        "id" => "seo_single_usedefault", 
        "name" => __("追加SEO设置","yii"),
        "type" => "checkbox",
        "std" => true,
        "desc" => __("启用","yii"),
        "notice" => __("启用后将追加上述SEO设置到文章页的关键词和描述信息中","yii"),
    );


//--------------------分组---------------------------
    $options[] = array(
        "name" => __("页脚","yii"),
        "type" => "heading", // 每当遇到heading时，将创建新的选项卡
    );
    $options[] = array(
        "id" => "site_create_date", 
        "name" => __("站点建立时间","yii"),
        "type" => "date",
        "std" => "",
        "desc" => __("用于计算站点运行天数","yii"),
    );
    $options[] = array(
        "id" => "site_status_days", 
        "name" => __("显示站点运行天数","yii"),
        "type" => "checkbox",
        "std" => false,
        "desc" => __("启用后在页脚显示站点共运行多少天，须设置上面的站点建立时间","yii"),
    );
    $options[] = array(
        "id" => "site_status_sql", 
        "name" => __("显示当前页查询次数","yii"),
        "type" => "checkbox",
        "std" => false,
        "desc" => __("启用后在页脚显示本页查询次数","yii"),
    );
    $options[] = array(
        "id" => "site_status_sql_time", 
        "name" => __("显示当前页查询用时","yii"),
        "type" => "checkbox",
        "std" => false,
        "desc" => __("启用后在页脚显示本页查询用时","yii"),
    );
    $options[] = array(
        "id" => "site_status_sql_memery", 
        "name" => __("显示当前页查询占用内存","yii"),
        "type" => "checkbox",
        "std" => false,
        "desc" => __("启用后在页脚显示本页查询占用内存","yii"),
    );
    $options[] = array(
        "id" => "beian_icp", 
        "name" => __("ICP/IP备案号","yii"),
        "type" => "text",
        "std" => _yi('beian_icp') ? _yi('beian_icp') : '蜀ICP备18036165号',
        "desc" => __("http://beian.miit.gov.cn","yii"),
    );
    $options[] = array(
        "id" => "beian_wangan", 
        "name" => __("网安备字","yii"),
        "type" => "text",
        "std" => _yi('beian_wangan') ? _yi('beian_wangan') : '川公网安备 51060302510805号',
        "desc" => __("http://www.beian.gov.cn","yii"),
    );


//--------------------分组---------------------------
    $options[] = array(
        "name" => __("首页焦点图","yii"),
        "type" => "heading", // 每当遇到heading时，将创建新的选项卡
    );
    $options[] = array(
        "id" => "index_slider", 
        "name" => __("显示焦点图","yii"),
        "type" => "checkbox",
        "std" => true,
        "desc" => __("开启","yii"),
        "notice" => __("建议尺寸：820x200，建议格式：png","yii"),
    );
    $options[] = array(
        "id" => "index_slider_show", 
        "name" => __("焦点图数量","yii"),
        "type" => "text",
        "std" => 5,
        "desc" => __("要显示的焦点图数量","yii"),
    );
    $silder_num = ["一","二","三","四","五"];
    for ($i=1;$i<6;$i++){
        $options[] = array(
            "id" => "index_slider".$i, 
            "name" => __("焦点图".$silder_num[$i-1],"yii"),
            "type" => "upload_sorted",
            "std" => "",
            "std_sort" => _yi("index_slider".$i."_sort") ? _yi("index_slider".$i."_sort") : 0,
            "std_link" => _yi("index_slider".$i."_link") ? _yi("index_slider".$i."_link") : "#",
        );
    }

//--------------------分组---------------------------
    $options[] = array(
        "name" => __("文章设置","yii"),
        "type" => "heading", // 每当遇到heading时，将创建新的选项卡
    );
    $options[] = array(
        "id" => "show_sticky", 
        "name" => __("显示置顶文章","yii"),
        "type" => "checkbox",
        "std" => true,
        "desc" => __("开启","yii"),
    );
    $options[] = array(
        "id" => "aattachment_display_custom", 
        "name" => __("媒体属性","yii"),
        "type" => "checkbox",
        "std" => true,
        "desc" => __("媒体链接到自身，水平居中对齐，完整尺寸","yii"),
    );
    $options[] = array(
        "id" => "image_lazyload", 
        "name" => __("图像懒加载","yii"),
        "type" => "checkbox",
        "std" => true,
        "desc" => __("启用后将延迟加载图片","yii"),
    );
    $options[] = array(
        "id" => "copyright_by", 
        "name" => __("版权申明","yii"),
        "type" => "checkbox",
        "std" => true,
        "desc" => __("启用后将在文章底部显示版权标识板块","yii"),
    );
    $options[] = array(
        "id" => "history_today", 
        "name" => __("历史上的今天","yii"),
        "type" => "checkbox",
        "std" => true,
        "desc" => __("启用后将在文章底部显示历史上的今天板块","yii"),
    );
    $options[] = array(
        "id" => "about_info", 
        "name" => __("关于信息","yii"),
        "type" => "radio",
        "std" => _yi("about_info") ? _yi("about_info") : "disable",
        "desc" => __("关于作者显示作者相关信息，关于本站显示站点设置信息","yii"),
        "options" => array(
            "disable" => __("禁用","yii"),
            "author" => __("关于作者","yii"),
            "site" => __("关于本站","yii"),
        ),
    );
    $options[] = array(
        "id" => "site_description", 
        "name" => __("关于本站","yii"),
        "type" => "text",
        "std" => "zhangsan",
        "desc" => __("在关于信息选择“关于本站”时调取显示的内容","yii"),
    );

//--------------------分组---------------------------
    $options[] = array(
        "name" => __("评论设置","yii"),
        "type" => "heading", // 每当遇到heading时，将创建新的选项卡
    );
    $options[] = array(
        "id" => "comment_ajax_qt", 
        "name" => __("开启ajax-qt","yii"),
        "type" => "checkbox",
        "std" => true,
        "desc" => __("开启","yii"),
    );
    $options[] = array(
        "id" => "comment_ajax", 
        "name" => __("开启ajax评论","yii"),
        "type" => "checkbox",
        "std" => true,
        "desc" => __("开启","yii"),
    );
    $options[] = array(
        "id" => "comment_meta_info", 
        "name" => __("评论者额外信息","yii"),
        "type" => "checkbox",
        "std" => true,
        "desc" => __("启用评论者额外信息（含电话与QQ）","yii"),
    );
    

//--------------------分组---------------------------
    $options[] = array(
        "name" => __("邮件设置","yii"),
        "type" => "heading", // 每当遇到heading时，将创建新的选项卡
    );
    $options[] = array(
        "id" => "notify_service_email", 
        "name" => __("邮箱设置","yii"),
        "type" => "text",
        "std" => 'no-reply@'.preg_replace('#^www.#', '', strtolower($_SERVER['SERVER_NAME'])),
        "desc" => __("设置用于发送邮件通知的邮箱","yii"),
    );
    $options[] = array(
        "id" => "comment_reply_notify", 
        "name" => __("评论回复邮件通知","yii"),
        "type" => "checkbox",
        "std" => true,
        "desc" => __("启用","yii"),
    );
    $options[] = array(
        "id" => "comment_reply_notify_admin", 
        "name" => __("评论回复邮件通知（管理员）","yii"),
        "type" => "checkbox",
        "std" => true,
        "desc" => __("启用后，即使被评论者为管理员，也接收邮件通知","yii"),
    );
    $options[] = array(
        "id" => "comment_approved_notify", 
        "name" => __("评论过审邮件通知","yii"),
        "type" => "checkbox",
        "std" => true,
        "desc" => __("启用","yii"),
    );

//--------------------分组---------------------------
    $options[] = array(
        "name" => __("搜索设置","yii"),
        "type" => "heading", // 每当遇到heading时，将创建新的选项卡
    );
    $options[] = array(
        "id" => "display_search", 
        "name" => __("在导航栏显示搜索框","yii"),
        "type" => "checkbox",
        "std" => true,
        "desc" => __("开启","yii"),
    );
    $options[] = array(
        "id" => "enable_baidu_searchbar", 
        "name" => __("启用百度搜索","yii"),
        "type" => "checkbox",
        "std" => true,
        "desc" => __("开启","yii"),
    );

    $options[] = array(
        'id' => 'wp_s',
        'name' => __('默认搜索设置', 'yii'),
        'type' => 'checkbox',
        'std' => true,
        'desc' => __('使用', 'yii'),
    );
    $options[] = array(
        'id' => 'search_cat',
        'name' => '',
        'type' => 'checkbox',
        'class' => 'hidden',
        'std' => true,
        'desc' => __('选择分类搜索', 'yii'),
    );
    $options[] = array(
        'id' => 'search_title',
        'name' => '',
        'type' => 'checkbox',
        'class' => 'hidden',
        'std' => false,
        'desc' => __('只搜索标题', 'yii'),
    );
    $options[] = array(
        'id' => 'not_search_cat',
        'name' => '',
        'class' => 'hidden',
        'type' => 'text',
        'std' => '',
        'desc' => __('排除的分类', 'yii'),
    );

//--------------------分组---------------------------
    $options[] = array(
        "name" => __("面包屑导航","yii"),
        "type" => "heading", // 每当遇到heading时，将创建新的选项卡
    );
    $options[] = array(
        "id" => "display_breadcrumb_nav", 
        "name" => __("启用面包屑导航","yii"),
        "type" => "checkbox",
        "std" => true,
        "desc" => __("开启","yii"),
    );
    $options[] = array(
        "id" => "show_crumbs_on_home", 
        "name" => __("在首页显示","yii"),
        "type" => "checkbox",
        "std" => true,
        "desc" => __("开启","yii"),
    );
    $options[] = array(
        "id" => "show_crumbs_homelink", 
        "name" => __("显示首页链接","yii"),
        "type" => "checkbox",
        "std" => true,
        "desc" => __("开启","yii"),
    );
    $options[] = array(
        "id" => "show_crumbs_current", 
        "name" => __("显示当前页标题","yii"),
        "type" => "checkbox",
        "std" => true,
        "desc" => __("开启","yii"),
    );
    $options[] = array(
        "id" => "show_crumbs_last_sep", 
        "name" => __("显示末尾导向符","yii"),
        "type" => "checkbox",
        "std" => true,
        "desc" => __("开启","yii"),
    );

//--------------------分组---------------------------
    $options[] = array(
        "name" => __("插件集成","yii"),
        "type" => "heading", // 每当遇到heading时，将创建新的选项卡
    );
	$options[] = array(
        "id" => "code_block", 
        "name" => __("CODE BLOCK代码高亮","yii"),
        "type" => "checkbox",
        "std" => false,
        "desc" => __("启用","yii"),
        "notice" => __("开启后将在【设置】中增加CODE BLOCK的设置菜单","yii"),
    );
    $options[] = array(
        "id" => "wp_smtp", 
        "name" => __("WP SMTP邮件客户端","yii"),
        "type" => "checkbox",
        "std" => false,
        "desc" => __("启用","yii"),
        "notice" => __("开启后将在【设置】中增加WP SMTP的设置菜单","yii"),
    );

//--------------------分组---------------------------
    $options[] = array(
        "name" => __("广告位管理","yii"),
        "type" => "heading", // 每当遇到heading时，将创建新的选项卡
    );
    $options[] = array(
        "id" => "ads_single_inside", 
        "name" => __("文章广告","yii"),
        "type" => "checkbox",
        "std" => true,
        "desc" => __("在文章之前插入一个广告","yii"),
    );
    $options[] = array(
        "id" => "ads_single_inside_where", 
        "name" => __("文章广告显示位置","yii"),
        "type" => "select",
        "options" => array(
            //"p0" => "独立于标题前",
            "p1" => "文章中，摘要后",
            "pr" => "文章中，段落随机",
        ),
        "desc" => __("在文章中插入一个广告","yii"),
    );
    $options[] = array(
        "id" => "ads_single_inside_code", 
        "name" => __("文章广告代码","yii"),
        "type" => "textarea",
        "std" => "",
        "desc" => __("支持HTML/CSS/JS，img标签必须携带class属性，推荐使用异步加载的广告代码","yii"),
    );

//--------------------分组---------------------------
    $options[] = array(
        "name" => __("管理中心","yii"),
        "type" => "heading", // 每当遇到heading时，将创建新的选项卡
    );
    $options[] = array(
        "id" => "hide_help", 
        "name" => __("隐藏“帮助”","yii"),
        "type" => "checkbox",
        "std" => true,
        "desc" => __("隐藏","yii"),
    );
    $options[] = array(
        "id" => "hide_screen_options", 
        "name" => __("隐藏“显示选项”","yii"),
        "type" => "checkbox",
        "std" => true,
        "desc" => __("隐藏","yii"),
    );

//--------------------分组---------------------------
    $options[] = array(
        "name" => __("其他设置","yii"),
        "type" => "heading", // 每当遇到heading时，将创建新的选项卡
    );
    //$options[] = array(
    //    "id" => "yi_debug", 
    //    "name" => __("显示调试信息","yii"),
    //    "type" => "checkbox",
    //    "std" => true,
    //    "desc" => __("开启后将在页面底部显示使用yi_dump()打印的调试信息", "yii")
    //);
    $options[] = array(
        "id" => "404_title", 
        "name" => __("自定义404页面标题","yii"),
        "type" => "text",
        "std" => "亲，你迷路了！",
    );
    $options[] = array(
        "id" => "404_content", 
        "name" => __("自定义404页面内容","yii"),
        "type" => "text",
        "std" => "亲，该网页可能搬家了！",
    );

    return $options;
}