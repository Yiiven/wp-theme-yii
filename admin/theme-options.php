<?php
//注册数据
function yi_register_theme_settings() {
    register_setting("theme_mods_yii","theme_mods_yii");
}
add_action('admin_init', 'yi_register_theme_settings');
//添加后台外观主题选项菜单
function yi_add_theme_options_menu() {
    add_theme_page(
        '主题设置',//页面标题
        '主题选项',//菜单标题
        'edit_theme_options',//访问该页面需要的权限
        'theme-options',//Handle（句柄）：当前文件
        'yi_theme_settings_admin'//要执行的函数，下方已申明
    );
}
add_action('admin_menu', 'yi_add_theme_options_menu');

function yi_theme_options(){
	$options = null;
	if (!$options){
        // Load options from options.php file (if it exists)
        $location = apply_filters('options_location', array('admin/options.php'));
        if ($optionsfile = locate_template($location)){
            $maybe_options = require_once $optionsfile;
            if (is_array($maybe_options)){
				$options = $maybe_options;
            } else if (function_exists('yi_theme_option_setting')){
				$options = yi_theme_option_setting();
			}
        }
        // Allow setting/manipulating options via filters
        $options = apply_filters( 'of_options', $options );
	}
	return $options;
}

function yi_theme_option_name(){
    return "yi";
}

function yi_theme_option_tabs(){
    $options = yi_theme_options();//yi_theme_option_setting();
    $counter = 0;
    $tabs = "";
    foreach ($options as $key => $val) {
        if ($val["type"] == "heading"){
            $counter++;
            $tabs .= '<a id="options-group-'.$counter.'-tab" class="nav-tab" href="#options-group-'.$counter.'" title="'.esc_attr($val["name"]).'">'.esc_html($val["name"]).'</a>';
        }
    }
    return $tabs;
}
function yi_theme_option_fields(){
    $option_name = yi_theme_option_name();
    $settings = get_option($option_name);
    $options = yi_theme_options();//yi_theme_option_setting();
    $counter = 0;
    $output = "";
    foreach ($options as $key => $val) {
        if ($val["type"] != "heading"){
            $val["id"] = preg_replace('/[^a-zA-Z0-9._\-]/', '', strtolower($val['id']));
            $id = 'section-'.$val['id'];
            $class = "section";
            if (isset($val["type"])){
                $class .= " section-".$val["type"];
            }
            if (isset($val["class"])){
                $class .= " ".$val["class"];
            }
            $output .= '<div id="'.esc_attr($id).'" class="'.esc_attr($class).'">';
            if (isset($val["name"])){
                $output .= '<h4 class="heading">'.esc_html($val["name"]).'</h4>';
            }
            $output .= '<div class="option"><div class="controls">';
        }
        // 取得默认值
        if (isset($val['std'])){
            $value = $val['std'];
        }
        if (isset($val['std_sort'])){
            $sort_value = $val['std_sort'];
        }
        if (isset($val['std_link'])){
            $link_value = $val['std_link'];
        }
        // 获取输入提示placeholder
        $placeholder = '';
		if (isset($val['placeholder'])){
			$placeholder = ' placeholder="'.esc_attr($val['placeholder']).'"';
		}
        // 替换已存储的值,当再次需要使用$val['std']时，直接替换为$value即可
        if ($val["type"] != "heading"){
            if (isset($settings[($val['id'])])){
                $value = $settings[($val['id'])];
                // 非数组选项，剔除反斜杠"\"
                if (!is_array($value)){
                    $value = stripslashes($value);
                }
            }
        }


        // checked($checked, $current = true, $echo = true)
        // selected($checked, $current = true, $echo = true)
        // disabled($checked, $current = true, $echo = true)
        // readonly($checked, $current = true, $echo = true)
        switch ($val["type"]){
	        // tabs
            case "heading":
                $counter++;
                if ($counter >= 2){
                    $output .= '</div>';
                }
                $output .= '<div id="options-group-'.$counter.'" class="group" style="'.($counter!=1?"display: none;":"").'">';
                break;
            // text
            case "text":
                $output .= '<input id="'.esc_attr($val["id"]).'" class="'.esc_attr($val["type"]).' of-input" type="'.esc_attr($val["type"]).'" name="'.esc_attr($option_name.'['.$val['id'].']').'" value="'.esc_attr($value).'"'.$placeholder.'>';
                break;
            // datetext
            case "date":
                $output .= '<input id="'.esc_attr($val["id"]).'" class="'.esc_attr($val["type"]).' of-input" type="'.esc_attr($val["type"]).'" name="'.esc_attr($option_name.'['.$val['id'].']').'" value="'.esc_attr($value).'"'.$placeholder.'>';
                break;
			// password
            case "password":
                $output .= '<input id="'.esc_attr($val["id"]).'" class="'.esc_attr($val["type"]).' of-input" type="'.esc_attr($val["type"]).'" name="'.esc_attr($option_name.'['.$val['id'].']').'" value="'.esc_attr($value).'">';
                break;
            // 单选框
            case "radio":
                $name = $option_name .'['. $val['id'] .']';
                foreach ($val['options'] as $key => $option) {
                    $id = $option_name.'-'.$val['id'].'-'.$key;
                    $output .= '<span class="radio"><input class="of-input of-radio" type="radio" name="'.esc_attr($name).'" id="'.esc_attr($id).'" value="'.esc_attr($key).'" '.checked($value, $key, false).' />';
                    $output .= '<label for="'.esc_attr($id).'">'.esc_html($option).'</label></span>';
                }
                break;
            // 选择列表
            case "select":
                $output .= '<select class="of-input" name="'.esc_attr($option_name.'['.$val['id'].']').'" id="'.esc_attr($val['id']).'">';
				foreach ($val['options'] as $key => $option){
					$output .= '<option'.selected($value, $key, false).' value="'.esc_attr($key).'">'.esc_html($option).'</option>';
				}
				$output .= '</select>';
                break;
            // 多选框
            case 'checkbox':
                $output .= '<input id="'.esc_attr($val["id"]).'" class="'.esc_attr($val["type"]).' of-input" type="'.esc_attr($val["type"]).'" name="'.esc_attr($option_name.'['.$val['id'].']').'"'.checked($value, "1", false).' />';
                break;
            // 文件上传
            case "upload":
                $output .= options_uploader($val["id"], $value);
                break;
            // 文件上传(支持排序和链接定义)
            case "upload_sorted":
                $output .= options_uploader_sorted($val["id"], $value, $sort_value, $link_value);
                break;
            // 文本域
            case "textarea":
                $rows = '8';
                $cols = '50';
				if (isset($val['settings']['rows'])){
					$custom_rows = $val['settings']['rows'];
					if (is_numeric($custom_rows)){
						$rows = $custom_rows;
					}
				}
				if (isset($val['settings']['rowscols'])){
					$custom_cols = $val['settings']['rowscols'];
					if (is_numeric($custom_cols)){
						$cols = $custom_cols;
					}
				}
				$value = stripslashes($value);
				$output .= '<textarea id="'.esc_attr($val['id']).'" class="of-input" name="'.esc_attr($option_name.'['.$val['id'].']').'" rows="'.$rows.'" cols="'.$cols.'" style="width:100%;"'.$placeholder.'>'.esc_textarea($value).'</textarea>';
				break;
			// 使用图片替代 radio 选择
            case "images":
                $name = $option_name.'['.$val['id'].']';
				foreach ($val['options'] as $key => $option){
					$selected = '';
					if ($value != '' && ($value == $key)){
						$selected = ' of-radio-img-selected';
					}
					$output .= '<div>';
					$output .= '<input type="radio" id="'.esc_attr($val['id'].'_'.$key).'" class="of-radio-img-radio" value="'.esc_attr($key).'" name="'.esc_attr($name).'" '.checked($value, $key, false).' />';
					$output .= '<label for="'.esc_attr($val['id'].'_'.$key).'">'.esc_html($key).'</label>';
					$output .= '<img src="'.esc_url($option).'" alt="'.$option.'" class="of-radio-img-img'.$selected.'" onclick="document.getElementById(\''.esc_attr($val['id'].'_'.$key).'\').checked=true;" />';
					$output .= '</div>';
				}
                break;
            // colorradio Selectors
			case "colorradio":
				$name = $option_name.'['.$val['id'].']';
				foreach ($val['options'] as $k => $v){
					$selected = '';
					$checked = '';
					if ($value != ''){
						if ($value == $k){
							$selected = ' of-radio-img-selected';
							$checked = ' checked="checked"';
						}
					}
					$output .= '<span class="colorradio-item">';
					$output .= '<input type="radio" id="'.esc_attr($val['id'].'_'.$k).'" class="of-radio-img-radio" value="'.esc_attr($k).'" name="'.esc_attr($name).'" '.$checked.' />';
					$output .= '<a style="background-color:#'.$k.';" href="javascript:;" class="of-radio-img-img of-radio-color'.$selected.'" onclick="document.getElementById(\''.esc_attr($val['id'].'_'.$k) .'\').checked=true;">';
					$output .= '<span class="of-radio-img-label">'.esc_html($k).'</span>';
					$output .= '</a>';
					// $output .= '<img src="'.esc_url($option).'" alt="'.$option.'" class="of-radio-img-img'.$selected.'" onclick="document.getElementById(\''.esc_attr($val['id'].'_'.$k) .'\').checked=true;" />';
					$output .= '</span>';
				}
				break;
            // Color picker
			case "color":
				$default_color = '';
				if (isset($val['std'])){
					if ($value != $val['std']) $default_color = ' data-default-color="'.$val['std'].'" ';
				}
				$output .= '<input name="'.esc_attr($option_name.'['.$val['id'].']').'" id="'.esc_attr($val['id']).'" class="of-color" type="text" value="'.esc_attr($value).'"'.$default_color.' />';
				break;
			// Multicheck
			case "multicheck":
				foreach ($val['options'] as $k => $v) {
					$checked = '';
					$label = $v;
					$v = preg_replace('/[^a-zA-Z0-9._\-]/', '', strtolower($k));

					$id = $option_name . '-' . $val['id'] . '-'. $v;
					$name = $option_name . '[' . $val['id'] . '][' . $v .']';

					if ( isset($value[$v]) ) {
						$checked = checked($value[$v], 1, false);
					}
					$output .= '<input id="' . esc_attr( $id ) . '" class="checkbox of-input" type="checkbox" name="' . esc_attr( $name ) . '" ' . $checked . ' /><label for="' . esc_attr( $id ) . '">' . esc_html( $label ) . '</label>';
				}
				break;
			case "request":
			    $output .= '<a id="'.esc_attr($val["id"]).'" class="'.esc_attr($val['type']).' btn" data-url="'.esc_attr($val['std']).'" href="javascript:void(0);">'.esc_html($val['name']).'</a>';
            default:
                break;
        }
        if ($val["type"] != "heading"){
            if (isset($val["desc"])){
                $output .= '<label class="explain" for="'.esc_attr($val["id"]).'">'.esc_html($val["desc"]).'</label>';
            }
            if (isset($val["notice"])){
                $output .= '<label class="explain" for="'.esc_attr($val["id"]).'">'.esc_html($val["notice"]).'</label>';
            }
            $output .= '</div></div></div>';
        }
    }
    return $output;
}


// 主题选项设置页
function yi_theme_settings_admin() {
    // 判断是否有post数据
    if (isset($_POST['update_theme_options'])) {
        yi_theme_settings_update(); 
    }
    // 这里写选项页面内容
    $page = get_template_part('/admin/template/options');
    echo $page;
}

// 主题选项设置更新
function yi_theme_settings_update(){
    $option_name = yi_theme_option_name();
    $options_default = yi_theme_options();// 默认的选项yi_theme_option_setting()
    // checkbox过滤
    $options_temp = array();
    foreach ($options_default as $key => $value) {
        if (isset($value["id"])){
            if ($value["type"]=="checkbox"){
                $options_temp[$value["id"]] = isset($value["std"]) ? $value["std"] : 0;
            }
        }
    }
    
    $yi = $_POST["yi"];// 提交的选项
    // checkbox修正(未提交的项修正为0)
    foreach ($options_temp as $key => $value) {
        if (isset($yi[$key])){
            $key_in = yi_filter_by_value($options_default,'id',$key);//取得当前key对应的数组集合，索引不受影响
            $key_in = array_column($key_in,null);//重置key集合索引为0
            if (isset($options_temp[$key]) && $key_in[0]["type"]=="checkbox"){
                $options_temp[$key] = ($yi[$key]=="on") ? 1 : 0;
            }
        }else{
            $options_temp[$key] = 0;
        }
    }
    $options_new = array_merge($yi, $options_temp);// 值修正完毕，合并数据
    // 更新到数据表
    update_option($option_name, $options_new);
}


function options_uploader($id, $value, $desc="", $name=""){
    $option_name = yi_theme_option_name();
    $output = "";
    $class = "";
    $id = strip_tags(strtolower($id));// 去除id中的html标签，并转为小写
    if ($name == '') {
        $name = $option_name.'['.$id.']';
    }

    if ($value) {
        $class = ' has-file';
    }
    // 输出预览图
    $output .= '<div class="screenshot" id="'.$id.'-image">';
    if ($value != '') {
        $remove = '<a class="remove-image" style="text-align:center;">'.__("移除", "yii").'</a>';
        $image = preg_match('/(^.*\.jpg|jpeg|png|gif|ico*)/i', $value);
        if ($image){
            $output .= '<img src="'.$value.'" alt="" />'/*.$remove*/;
        }else{
            $parts = explode("/", $value);
            for ($i = 0; $i < sizeof($parts); ++$i) {
                $title = $parts[$i];
            }

            // 非图像文件不输出内容
            $output .= '';

            // 非图像的标准通用输出
            $title = __('预览', 'yii');
            $output .= '<div class="no-image"><span class="file_link"><a href="'.$value.'" target="_blank" rel="external">'.$title.'</a></span></div>';
        }
    }
    $output .= '</div>';// 预览图输出完毕

    $output .= '<input id="'.$id.'" class="upload'.$class.'" type="text" name="'.$name.'" value="'.$value.'" placeholder="'.__('请选择文件', 'yii').'" />';
    if (function_exists('wp_enqueue_media')) {
        if (($value == '')) {
            $output .= '<input id="upload-'.$id.'" class="upload-button button" type="button" value="'.__('上传', 'yii').'" />';
        }else{
            $output .= '<input id="remove-'.$id.'" class="remove-file button" type="button" value="'.__('移除', 'yii').'" />';
        }
    }else{
        $output .= '<p><i>'.__('请升级您的WordPress版本以获得完整的媒体支持。', 'yii').'</i></p>';
    }
    if ($desc != '') {
        $output .= '<span class="of-metabox-desc">'.$desc.'</span>';
    }

    return $output;
}

function options_uploader_sorted($id, $value, $sort=0, $link="", $name=""){
    $option_name = yi_theme_option_name();
    $output = "";
    $class = "";
    $id = strip_tags(strtolower($id));// 去除id中的html标签，并转为小写
    if ($name == '') {
        $name = $option_name.'['.$id.']';
        $sortname = $option_name.'['.$id.'_sort]';
        $linkname = $option_name.'['.$id.'_link]';
    }

    if ($value) {
        $class = ' has-file';
    }
    // 输出预览图
    $output .= '<div class="screenshot" id="'.$id.'-image">';
    if ($value != '') {
        $remove = '<a class="remove-image" style="text-align:center;">'.__("移除", "yii").'</a>';
        $image = preg_match('/(^.*\.jpg|jpeg|png|gif|ico*)/i', $value);
        if ($image){
            $output .= '<img src="'.$value.'" alt="" />'/*.$remove*/;
        }else{
            $parts = explode("/", $value);
            for ($i = 0; $i < sizeof($parts); ++$i) {
                $title = $parts[$i];
            }

            // 非图像文件不输出内容
            $output .= '';

            // 非图像的标准通用输出
            $title = __('预览', 'yii');
            $output .= '<div class="no-image"><span class="file_link"><a href="'.$value.'" target="_blank" rel="external">'.$title.'</a></span></div>';
        }
    }
    $output .= '</div>';// 预览图输出完毕

    $output .= '<div class="sub-item"><span class="sub-heading">'.__('上传/浏览附件', 'yii').'</span>：<input id="'.$id.'" class="upload'.$class.'" type="text" name="'.$name.'" value="'.$value.'" placeholder="'.__('请选择文件', 'yii').'" />';
    if (function_exists('wp_enqueue_media')) {
        if (($value == '')) {
            $output .= '<input id="upload-'.$id.'" class="upload-button button" type="button" value="'.__('上传', 'yii').'" />';
        }else{
            $output .= '<input id="remove-'.$id.'" class="remove-file button" type="button" value="'.__('移除', 'yii').'" />';
        }
        $output .= '</div><div class="sub-item"><span class="sub-heading">'.__('设置排列顺序', 'yii').'</span>：<input type="number" name="'.$sortname.'" value="'.$sort.'" placeholder="'.__('设置顺序', 'yii').'"></div>';
        $output .= '<div class="sub-item"><span class="sub-heading">'.__('设置链接地址', 'yii').'</span>：<input type="text" name="'.$linkname.'" value="'.$link.'" placeholder="'.__('设置链接', 'yii').'"></div>';
    }else{
        $output .= '</div><p><i>'.__('请升级您的WordPress版本以获得完整的媒体支持。', 'yii').'</i></p>';
    }
    return $output;
}

function options_script(){
    if (function_exists('wp_enqueue_media')) wp_enqueue_media();
    wp_enqueue_style('admin', get_template_directory_uri().'/admin/css/admin.css', array(), version);
    wp_enqueue_style('options', get_template_directory_uri().'/admin/css/options.css', array(), version);
    wp_register_script('uploader', get_template_directory_uri().'/admin/js/uploader.js', version, false);//注册脚本
    wp_enqueue_script('uploader');
    // wp_localize_script($handle-要附加数据的脚本名称, $object_name-包含数据的对象名称, $l10n-要本地化的数据本身(WP3.3+支持多个值));
    wp_localize_script('uploader', 'options_l10n', array(
        'upload' => __('上传', 'yii'),
        'remove' => __('移除', 'yii')
    ));
}
function options_footer_script(){
    wp_enqueue_script('lazyload', get_template_directory_uri().'/js/jquery.lazyload.js',array(), version, true);//注册脚本
    wp_enqueue_script('admin-style', get_template_directory_uri().'/admin/js/admin-style.js',array(), version, true);//注册脚本
}
add_action('admin_enqueue_scripts', 'options_footer_script', -1);
add_action('admin_enqueue_scripts', 'options_script', -1);