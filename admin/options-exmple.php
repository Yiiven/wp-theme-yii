<?php
/**
 * 主题选项配置项目
 * @param string id 配置项id，唯一,_yi()将以此查找该项是否存在(注：除type=heading外，其他所有项目均须包含此项)
 * @param string name 配置项名称，为了使得能后翻译，使用__()函数包裹
 * @param string class 配置项的className
 * @param string type 配置项类型，支持的类型如下
 *                    heading - 分组，选项卡标题
 *                    text - 文本输入框
 *                    password - 密码输入框
 *                    textarea - 文本域
 *                    radio - 单选框
 *                    clolorradio - 颜色选择器
 *                    color - 自定义颜色，一般与颜色选择器配合使用
 *                    images - 使用图片替代 radio 选择
 *                    select - 选择列表
 *                    checkbox - 多选框
 *                    muticheck - 多个多选框
 *                    upload - 文件上传
 *                    upload_sorted - 多文件上传，支持排序，支持自定义链接地址
 *                    request - 发起一个http请求
 * @param string std 配置项默认值
 * @param string desc 配置项描述信息，在配置项结束时出现，跟在配置项目后，可以与notice交换位置
 * @param string notice 配置项提示信息，在配置项结束时出现，跟在配置项目后，可以与desc交换位置
 * @param array options 配置项选项，当类型为radio,clolorradio,images,select,muticheck时读取
 *                      格式 'value' => 'name'
 * @param array settings 配置项设置，当类型为textarea时读取，默认8/50
 *                       格式 'rows/cols' => row_num/cols_num
 * 下面是写法示例和完整的exmple
 $options[] = array(
     "id" => "",
     "name" => __("message", "yii"),
     "class" => "",
     "type" => "",
     "std" => "",
     "desc" => __(),
     "notice" => __(),
     "options" => array("value" => "option_name"),
     "settings" => array("rows"=>1,"cols"=>50),
 );
 */

function yi_theme_option_setting() {
    $options = array();
    //--------------------分组---------------------------
    $options[] = array(
        "name" => __("type演示","yii"),
        "type" => "heading", // 每当遇到heading时，将创建新的选项卡
    );
    $options[] = array(
		'id' => 'test_text',
		'name' => __("文本输入框", 'yii'),
		'std' => "",
		'type' => "text"
	);
	$options[] = array(
		'id' => 'test_password',
		'name' => __("密码输入框", 'yii'),
		'std' => "",
		'type' => "password"
	);
	$options[] = array(
		'id' => 'test_select',
		'name' => __('选择列表', 'yii'),
		'type' => 'select',
		'options' => array(
		    's1'=>'选项1',
		    's2'=>'选项2',
		),
	);
	$options[] = array(
		'name' => __('文本域', 'yii'),
		'id' => 'test_textarea',
		'std' => "欢迎光临\n我们一直在努力",
		'type' => 'textarea',
		'desc' => __('显示在Logo旁边的两个短文字，请换行填写两句文字（短文字介绍）', 'yii'),
		'settings' => array(
			'rows' => 2
		),
	);
	$options[] = array(
		'name' => __('单个多选框', 'haoui'),
		'id' => 'test_checkbox',
		'type' => 'checkbox',
		'std' => true,
		'desc' => "描述信息",
	);
	$options[] = array(
		'name' => __('多个多选框', 'haoui'),
		'id' => 'test_multicheck',
		'type' => 'multicheck',
		'options' => array(
		    'c1'=>'选项1',
		    'c2'=>'选项2',
		),
	);
	$options[] = array(
		'id' => 'test_radio',
		'name' => __("单选框", 'yii'),
		'std' => "r1",
		'type' => "radio",
		'options' => array(
		    'r1' => "选项1",
		    'r2' => "选项2",
		)
	);
    $options[] = array(
		'name' => __("颜色选择器", 'yii'),
		'desc' => __("14种颜色供选择", 'yii'),
		'id' => "test_colorradio",
		'std' => "45B6F7",
		'type' => "colorradio",
		'options' => array(
			'45B6F7' => 100,
			'FF5E52' => 1,
			'2CDB87' => 2,
			'00D6AC' => 3,
			'16C0F8' => 4,
			'EA84FF' => 5,
			'FDAC5F' => 6,
			'FD77B2' => 7,
			'76BDFF' => 8,
			'C38CFF' => 9,
			'FF926F' => 10,
			'8AC78F' => 11,
			'C7C183' => 12,
			'555555' => 13
		)
	);
	$options[] = array(
		'id' => 'test_color',
		'name' => __("颜色自定义", 'yii'),
		'std' => "",
		'desc' => __('如果不用自定义颜色清空即可（默认不用自定义）', 'yii'),
		'type' => "color"
	);
	$options[] = array(
		'name' => __('使用图片替代 radio 选择', 'haoui'),
		'id' => 'test_images',
		'type' => 'images',
		'std' => 'i1',
		'options' => array(
		    'i1'=>$imagepath.'/fancy.png',
		    'i2'=>$imagepath.'/fancy.png',
		),
	);
	$options[] = array(
        "id" => "test_upload", 
        "name" => __("文件上传1","yii"),
        "type" => "upload",
        "std" => "",
    );
    $options[] = array(
        "id" => "test_upload_sorted".$i, 
        "name" => __("文件上传2","yii"),
        "type" => "upload_sorted",
        "std" => "",
        "std_sort" => 0,
        "std_link" => "#",
    );
    $options[] = array(
        "id" => "seo_sitemap", 
        "name" => __("生成网站地图（sitemap）","yii"),
        "type" => "request",
        "std" => "request-url",
    );

    return $options;
}