<?php
$tabs = yi_theme_option_tabs();
$fields = yi_theme_option_fields();
?>
<div id="options-wrap" class="wrap">
    <h2>主题选项设置</h2>
    <h2 class="nav-tab-wrapper">
        <?php echo $tabs; ?>
    </h2>
    <div class="metabox-holder">
        <div class="postbox">
            <form action="" method="post">
                <input type="hidden" name="update_theme_options" value="true" />
                <?php echo $fields; ?>
                <?php if ($tabs != "") echo "</div>"; ?>
                <div class="sub-btn-box">
                    <input type="submit" class="button button-primary" value="更新数据"/>
                </div>
            </form>
        </div>
    </div>
    <div class="modal"><div class="header">操作提示</div><div class="body"></div><div class="footer"><a href="javascript:void(0);" class="modal-close btn">确定</a></div></div>
</div>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/admin/js/options.js"></script>