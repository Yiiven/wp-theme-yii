<?php 
/**
 * options的模板
 */

?>
<div id="options-wrap" class="wrap">
    <h2>主题选项设置</h2>
    <h2 class="nav-tab-wrapper">
        <a id="options-group-1-tab" class="nav-tab nav-tab-active" title="基本" href="#options-group-1">基本</a>
        <a id="options-group-2-tab" class="nav-tab" title="更多" href="#options-group-2">更多</a>
    </h2>
    <div class="metabox-holder">
        <div class="postbox">
            <form action="" method="post">
                <div id="options-group-1" class="group" style="display: none;">
                    <div id="section-text_justify_s" class="section section-checkbox">
                        <h4 class="heading">字段1</h4>
                        <div class="option">
                            <div class="controls">
                                <input id="field1" class="checkbox of-input" type="checkbox" name="yi[field1]" checked="checked">
                                <label class="explain" for="field1">开启</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="options-group-2" class="group" style="display: none;">
                    <div id="section-text_justify_s" class="section section-checkbox">
                        <h4 class="heading">字段2</h4>
                        <div class="option">
                            <div class="controls">
                                <input id="field2" class="checkbox of-input" type="checkbox" name="yi[field2]" checked="checked">
                                <label class="explain" for="field2">开启</label>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>