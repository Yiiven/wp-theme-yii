jQuery(document).ready(function($){
    $('.fixed .column-title').css('width', '30%');
    // 图片懒加载
    $("img.lazy").lazyload({
        effect: "fadeIn",
        threshold: 100,
        failure_limit: 70
    });
    //取消网页预览，但保留新窗口打开链接的属性，此事件在/wp-content/plugins/akismet/_inc/akismet.js中大概86行的位置注册
    var mshotEnabledLinkSelector = 'a[id^="author_comment_url"], tr.pingback td.column-author a:first-of-type, td.comment p a';
    $('#the-comment-list').off('mouseover',mshotEnabledLinkSelector);
    $(mshotEnabledLinkSelector).attr('target', '_blank');
})