/**
 * Custom scripts needed for the colorpicker, image button selectors,
 * and navigation tabs.
 */

jQuery(document).ready(function($) {
    // 显示tab对应的内容
    if ( $('.nav-tab-wrapper').length > 0 ) {
        show_options_tab();
    }
    // 如何显示
    function show_options_tab() {

        var $group = $('.group'),// 字段DOM
            $navtabs = $('.nav-tab-wrapper a'),//标签DOM
            active_tab = '';// 激活的标签DOM

        // 隐藏所有设置项
        $group.hide();

        // 查找所选选项卡是否保存在本地存储中
        if ( typeof(localStorage) != 'undefined' ) {
            active_tab = localStorage.getItem('active_tab');
        }

        // 如果活动选项卡已保存并存在，则加载它的分组
        if ( active_tab != '' && $(active_tab).length ) {
            $(active_tab).fadeIn();
            $(active_tab + '-tab').addClass('nav-tab-active');
        } else {
            $('.group:first').fadeIn();
            $('.nav-tab-wrapper a:first').addClass('nav-tab-active');
        }

        // tabs点击事件
        $navtabs.on("click", function(e) {

            e.preventDefault();

            // 移除全部标签的激活状态
            $navtabs.removeClass('nav-tab-active');
            // 当前标签添加激活属性
            $(this).addClass('nav-tab-active').blur();
            // 本地存储
            if (typeof(localStorage) != 'undefined' ) {
                localStorage.setItem('active_tab', $(this).attr('href') );
            }
            // 被选择的分组
            var selected = $(this).attr('href');

            $group.hide();//隐藏全部分组
            $(selected).fadeIn();//显示当前分组

        });
    }

	// 请求方式
	$('.request').on('click',function(){
		var obj = $(this);
		var ajax_url = obj.data('url');
		var modal = $(".modal");
		$.ajax({
			url: ajax_url,
			data: null,
			type: 'post',
			success: function(res){
				modal.show().find(".body").html(res);
			}
		});
	})

	// modal隐藏并清空内容
	$(".modal-close,.modal .header").on("click",function(){
		var modal = $(".modal");
		modal.hide().find(".body").html("");//
	})
});