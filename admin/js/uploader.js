jQuery(document).ready(function($){
    var options_upload,
        options_selector;
    // 上传图像
    function options_add_file(event, selector) {
        var upload = $(".uploaded-file"),
            frame;
        var elem = $(this);
        options_selector = selector;
        event.preventDefault();
        // 如果媒体框已存在，重新打开
        if ( options_upload ) {
            options_upload.open();
        } else {
            // 创建媒体模态框
            options_upload = wp.media.frames.options_upload =  wp.media({
                // 设置模态框标题
                title: elem.data('choose'),

                // 定义提交按钮颜色k1v566.
                button: {
                    // 定义提交按钮文本
                    text: elem.data('update'),
                    // 告诉按钮不要关闭模态框，因为我们将在选择图像时刷新页面。
                    close: false
                }
            });
            // 选择图像后的回调
            options_upload.on( 'select', function() {
                // 抓取附件.
                var attachment = options_upload.state().get('selection').first();
                options_upload.close();
                options_selector.find('.upload').val(attachment.attributes.url);
                // 变更图像信息
                if ( attachment.attributes.type == 'image' ) {
                    // options_selector.find('.screenshot').empty().hide().append('<img src="' + attachment.attributes.url + '"><a class="remove-image">Remove</a>').slideDown('fast');
                    options_selector.find('.screenshot img').attr("src", attachment.attributes.url);
                }
                options_selector.find('.upload-button').unbind().addClass('remove-file').removeClass('upload-button').val(options_l10n.remove);
                options_selector.find('.of-background-properties').slideDown();
                options_selector.find('.remove-image, .remove-file').on('click', function() {
                    options_remove_file( $(this).parents('.section') );
                });
            });
        }
        // 打开模态框
        options_upload.open();
    }
    // 移除图像信息及预览窗格
    function options_remove_file(selector) {
        selector.find('.remove-image').hide();
        selector.find('.upload').val('');
        selector.find('.of-background-properties').hide();
        //selector.find('.screenshot').slideUp();// 预览窗格
        selector.find('.screenshot img').attr("src", "");// 预览窗格
        selector.find('.remove-file').unbind().addClass('upload-button').removeClass('remove-file').val(options_l10n.upload);
        // 如果.upload-notice已存在将不显示上传按钮，这说明用户没有WP3.5+的媒体库支持
        if ( $('.section-upload .upload-notice').length > 0 ) {
            $('.upload-button').remove();
        }
        selector.find('.upload-button').on('click', function(event) {
            options_add_file(event, $(this).parents('.section'));
        });
    }

    $('.remove-image, .remove-file').on('click', function() {
        options_remove_file( $(this).parents('.section') );
    });

    $('.upload-button').click( function( event ) {
        options_add_file(event, $(this).parents('.section'));
    });
});