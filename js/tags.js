var radius = 80;//旋转半径
var diameter = 160;// 直径
var dtr = Math.PI / 180;//单位弧度1°所对应的的圆周率约等于3.14/180
var tagsList = [];//存储标签的数组
var lasta = 1;// 存储最后一次更新后的a或b
var lastb = 1;// 存储最后一次更新后的a或b
var distr = true;//反余弦路径开关
var revs = 10;//旋转速度
var size = 160;//画布尺寸
var mouseX = 0;
var mouseY = 10;
var howElliptical = 1;
var tagsLinks = null;// 标签链接a的对象
var tagsBox = null;// 标签外层div的对象

window.onload = function (){
    var tagsObject = null;
    tagsBox = document.getElementById('widget-tags-cloud');
    tagsLinks = tagsBox.getElementsByTagName('a');
    for(i=0; i<tagsLinks.length; i++){
        tagsObject = {};        
        tagsLinks[i].onmouseover = (function (obj) {
                return function () {
                    obj.on = true;
                    this.style.zIndex = 9999;
                    this.style.color = '#fff';
                    this.style.background = '#0099ff';
                    this.style.padding = '5px 5px';
                    this.style.filter = "alpha(opacity=100)";
                    this.style.opacity = 1;
                }
            })(tagsObject);// 将匿名海曙的返回值赋值给 tagsObject
            tagsLinks[i].onmouseout = (function (obj) {
                return function () {
                    obj.on = false;
                    this.style.zIndex = obj.zIndex;
                    this.style.color = '#fff';
                    this.style.background = '#30899B';
                    this.style.padding = '5px';
                    this.style.filter = "alpha(opacity=" + 100 * obj.alpha + ")";
                    this.style.opacity = obj.alpha;
                    this.style.zIndex = obj.zIndex;
                }
            })(tagsObject)
            tagsObject.offsetWidth = tagsLinks[i].offsetWidth;
            tagsObject.offsetHeight = tagsLinks[i].offsetHeight;
            tagsList.push(tagsObject);//push tags to tagsList
    }
    sine_and_cosine(0,0,0);
    position_all();
    (function () {
        update();
        setTimeout(arguments.callee, 40);
    })();
};

function update(){
    var a, b, c = 0;
    a = (Math.min(Math.max(-mouseY, -size), size) / radius) * revs;
    b = (-Math.min(Math.max(-mouseX, -size), size) / radius) * revs;
    //lasta = a;
    //lastb = b;
    if (Math.abs(a) <= 0.01 && Math.abs(b) <= 0.01) {
        return;
    }
    sine_and_cosine(a, b, c);
    for (var i = 0; i < tagsList.length; i++) {
        if (tagsList[i].on) {
            continue;
        }
        var rx1 = tagsList[i].coordinate_x;
        var ry1 = tagsList[i].coordinate_y * cosine_a + tagsList[i].coordinate_z * (-sine_a);
        var rz1 = tagsList[i].coordinate_y * sine_a + tagsList[i].coordinate_z * cosine_a;

        var rx2 = rx1 * cosine_b + rz1 * sine_b;
        var ry2 = ry1;
        var rz2 = rx1 * (-sine_b) + rz1 * cosine_b;

        var rx3 = rx2 * cosine_c + ry2 * (-sine_c);
        var ry3 = rx2 * sine_c + ry2 * cosine_c;
        var rz3 = rz2;

        tagsList[i].coordinate_x = rx3;// x坐标
        tagsList[i].coordinate_y = ry3;// y坐标
        tagsList[i].coordinate_z = rz3;// z坐标

        per = diameter / (diameter + rz3);

        tagsList[i].x = (howElliptical * rx3 * per) - (howElliptical * 2);
        tagsList[i].y = ry3 * per;
        tagsList[i].scale = per;
        var alpha = per;
        alpha = (alpha - 0.6) * (10 / 6);
        tagsList[i].alpha = alpha * alpha * alpha - 0.2;
        tagsList[i].zIndex = Math.ceil(100 - Math.floor(tagsList[i].coordinate_z));
    }
    do_position();
}

function depthSort(){
    //var i = 0;// 计数器
    var tagsLinksTemp=[];
    for(i=0; i<tagsLinks.length; i++){
        tagsLinksTemp.push(tagsLinks[i]);
    }
    tagsLinksTemp.sort(
        function (vItem1, vItem2){
            if(vItem1.coordinate_z > vItem2.coordinate_z){
                return -1;
            }else if(vItem1.coordinate_z < vItem2.coordinate_z){
                return 1;
            }else{
                return 0;
            }
        }
    );
    for(i=0; i<tagsLinksTemp.length; i++){
        tagsLinksTemp[i].style.zIndex=i;
    }
}

function position_all(){
    var phi = 0;
    var theta = 0;
    var max = tagsList.length;//tags数量
    for (var i = 0; i < max; i++) {
        if (distr) {
            phi = Math.acos((2 * i + 1) / max - 1);//反余弦值(单位:弧度)，对 <-1 或 >1 的值，返回 NaN。
            theta = Math.sqrt(max * Math.PI) * phi;
        }else{
            phi = Math.random() * (Math.PI);//返回 >=0 且 <1 的一个随机数
            theta = Math.random() * (2 * Math.PI);//Math.PI=π，弧度制=180°，角度制=3.14159265...
        }
        //坐标变换
        tagsList[i].coordinate_x = radius * Math.cos(theta) * Math.sin(phi); // 横坐标
        tagsList[i].coordinate_y = radius * Math.sin(theta) * Math.sin(phi); // 纵坐标
        tagsList[i].coordinate_z = radius * Math.cos(phi); // 景深

        tagsLinks[i].style.left = tagsList[i].coordinate_x + tagsBox.offsetWidth / 2 - tagsList[i].offsetWidth / 2 + 'px'; // 顶部距离
        tagsLinks[i].style.top = tagsList[i].coordinate_y + tagsBox.offsetHeight / 2 - tagsList[i].offsetHeight / 2 + 'px'; // 左边距离
    }
}

function do_position(){
    var left = tagsBox.offsetWidth / 2;
    var top = tagsBox.offsetHeight / 2;
    for (var i = 0; i < tagsList.length; i++) {
        if (tagsList[i].on) {
            continue;
        }
        var tagsStyle = tagsLinks[i].style;
        if (tagsList[i].alpha > 0.1) {
            if (tagsStyle.display != '') tagsStyle.display = '';
        }else{
            if (tagsStyle.display != 'none') tagsStyle.display = 'none';
            continue;// 跳过一次循环迭代
        }
        tagsStyle.left = tagsList[i].coordinate_x + left - tagsList[i].offsetWidth / 2 + 'px';
        tagsStyle.top = tagsList[i].coordinate_y + top - tagsList[i].offsetHeight / 2 + 'px';
        //tagsStyle.fontSize = Math.ceil(12 * tagsList[i].scale/2 ) + 8 + 'px';
        //tagsStyle.filter = "progid:DXImageTransform.Microsoft.Alpha(opacity=" + 100 * tagsList[i].alpha + ")";
        tagsStyle.filter = "alpha(opacity=" + 100 * tagsList[i].alpha + ")";
        tagsStyle.zIndex = tagsList[i].zIndex;
        tagsStyle.opacity = tagsList[i].alpha;
    }
}

function sine_and_cosine(a, b, c){
    sine_a = Math.sin(a * dtr);
    sine_b = Math.sin(b * dtr);
    sine_c = Math.sin(c * dtr);
    cosine_a = Math.cos(a * dtr);
    cosine_b = Math.cos(b * dtr);
    cosine_c = Math.cos(c * dtr);
}