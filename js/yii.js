/*headroom.js v0.9.4*/
!function(a,b){"use strict";"function"==typeof define&&define.amd?define([],b):"object"==typeof exports?module.exports=b():a.Headroom=b()}(this,function(){"use strict";function a(a){this.callback=a,this.ticking=!1}function b(a){return a&&"undefined"!=typeof window&&(a===window||a.nodeType)}function c(a){if(arguments.length<=0)throw new Error("Missing arguments in extend function");var d,e,f=a||{};for(e=1;e<arguments.length;e++){var g=arguments[e]||{};for(d in g)"object"!=typeof f[d]||b(f[d])?f[d]=f[d]||g[d]:f[d]=c(f[d],g[d])}return f}function d(a){return a===Object(a)?a:{down:a,up:a}}function e(a,b){b=c(b,e.options),this.lastKnownScrollY=0,this.elem=a,this.tolerance=d(b.tolerance),this.classes=b.classes,this.offset=b.offset,this.scroller=b.scroller,this.initialised=!1,this.onPin=b.onPin,this.onUnpin=b.onUnpin,this.onTop=b.onTop,this.onNotTop=b.onNotTop,this.onBottom=b.onBottom,this.onNotBottom=b.onNotBottom}var f={bind:!!function(){}.bind,classList:"classList"in document.documentElement,rAF:!!(window.requestAnimationFrame||window.webkitRequestAnimationFrame||window.mozRequestAnimationFrame)};return window.requestAnimationFrame=window.requestAnimationFrame||window.webkitRequestAnimationFrame||window.mozRequestAnimationFrame,a.prototype={constructor:a,update:function(){this.callback&&this.callback(),this.ticking=!1},requestTick:function(){this.ticking||(requestAnimationFrame(this.rafCallback||(this.rafCallback=this.update.bind(this))),this.ticking=!0)},handleEvent:function(){this.requestTick()}},e.prototype={constructor:e,init:function(){if(e.cutsTheMustard)return this.debouncer=new a(this.update.bind(this)),this.elem.classList.add(this.classes.initial),setTimeout(this.attachEvent.bind(this),100),this},destroy:function(){var a=this.classes;this.initialised=!1;for(var b in a)a.hasOwnProperty(b)&&this.elem.classList.remove(a[b]);this.scroller.removeEventListener("scroll",this.debouncer,!1)},attachEvent:function(){this.initialised||(this.lastKnownScrollY=this.getScrollY(),this.initialised=!0,this.scroller.addEventListener("scroll",this.debouncer,!1),this.debouncer.handleEvent())},unpin:function(){var a=this.elem.classList,b=this.classes;!a.contains(b.pinned)&&a.contains(b.unpinned)||(a.add(b.unpinned),a.remove(b.pinned),this.onUnpin&&this.onUnpin.call(this))},pin:function(){var a=this.elem.classList,b=this.classes;a.contains(b.unpinned)&&(a.remove(b.unpinned),a.add(b.pinned),this.onPin&&this.onPin.call(this))},top:function(){var a=this.elem.classList,b=this.classes;a.contains(b.top)||(a.add(b.top),a.remove(b.notTop),this.onTop&&this.onTop.call(this))},notTop:function(){var a=this.elem.classList,b=this.classes;a.contains(b.notTop)||(a.add(b.notTop),a.remove(b.top),this.onNotTop&&this.onNotTop.call(this))},bottom:function(){var a=this.elem.classList,b=this.classes;a.contains(b.bottom)||(a.add(b.bottom),a.remove(b.notBottom),this.onBottom&&this.onBottom.call(this))},notBottom:function(){var a=this.elem.classList,b=this.classes;a.contains(b.notBottom)||(a.add(b.notBottom),a.remove(b.bottom),this.onNotBottom&&this.onNotBottom.call(this))},getScrollY:function(){return void 0!==this.scroller.pageYOffset?this.scroller.pageYOffset:void 0!==this.scroller.scrollTop?this.scroller.scrollTop:(document.documentElement||document.body.parentNode||document.body).scrollTop},getViewportHeight:function(){return window.innerHeight||document.documentElement.clientHeight||document.body.clientHeight},getElementPhysicalHeight:function(a){return Math.max(a.offsetHeight,a.clientHeight)},getScrollerPhysicalHeight:function(){return this.scroller===window||this.scroller===document.body?this.getViewportHeight():this.getElementPhysicalHeight(this.scroller)},getDocumentHeight:function(){var a=document.body,b=document.documentElement;return Math.max(a.scrollHeight,b.scrollHeight,a.offsetHeight,b.offsetHeight,a.clientHeight,b.clientHeight)},getElementHeight:function(a){return Math.max(a.scrollHeight,a.offsetHeight,a.clientHeight)},getScrollerHeight:function(){return this.scroller===window||this.scroller===document.body?this.getDocumentHeight():this.getElementHeight(this.scroller)},isOutOfBounds:function(a){var b=a<0,c=a+this.getScrollerPhysicalHeight()>this.getScrollerHeight();return b||c},toleranceExceeded:function(a,b){return Math.abs(a-this.lastKnownScrollY)>=this.tolerance[b]},shouldUnpin:function(a,b){var c=a>this.lastKnownScrollY,d=a>=this.offset;return c&&d&&b},shouldPin:function(a,b){var c=a<this.lastKnownScrollY,d=a<=this.offset;return c&&b||d},update:function(){var a=this.getScrollY(),b=a>this.lastKnownScrollY?"down":"up",c=this.toleranceExceeded(a,b);this.isOutOfBounds(a)||(a<=this.offset?this.top():this.notTop(),a+this.getViewportHeight()>=this.getScrollerHeight()?this.bottom():this.notBottom(),this.shouldUnpin(a,c)?this.unpin():this.shouldPin(a,c)&&this.pin(),this.lastKnownScrollY=a)}},e.options={tolerance:{up:0,down:0},offset:0,scroller:window,classes:{pinned:"headroom--pinned",unpinned:"headroom--unpinned",top:"headroom--top",notTop:"headroom--not-top",bottom:"headroom--bottom",notBottom:"headroom--not-bottom",initial:"headroom"}},e.cutsTheMustard="undefined"!=typeof f&&f.rAF&&f.bind&&f.classList,e});

/*yii.js*/
(function($){
    var width = window.screen.height;
    var height = window.screen.width;
    $.post(ajax_theme_object.ajaxurl,{width:width, height:height});//这里向你的统计文件里面传入相关的参数
    // 搜索
    $(".nav-search, .search-off, .search-off-btn, .search-close").click(function() {
        $("#search-main").fadeToggle(300);
    });
    $("#search-main .searchbar button[type='submit']").click(function(){
        var wd = $(this).siblings("input[type='text']").val("");
    });

    // mobile-menu
    $("#sidr-main li.menu-item-has-children").each(function(){
        $(this).append("<i class='triangle-change dropdown-toggle'></i>")
    });
    $("#sidr-main li.menu-item-has-children .dropdown-toggle").on("click", function(){
        $(this).toggleClass("triangle-changed");
        $(this).siblings(".sub-menu").slideToggle(300);
    });
    $(".sidr-close, .sidr-show").on("click",function(){
        $("#sidr-main").toggleClass("toggled-on");
    })
    $(".right-sider-show, .right-sidebar-close").on("click",function(){
	    if($(".right-sidebar").hasClass('slideLeft')){
		    $(".right-sidebar").removeClass('slideLeft');
		    $(".right-sidebar").addClass('slideRight');
	    }else{
		    $(".right-sidebar").removeClass('slideRight');
		    $(".right-sidebar").addClass('slideLeft');
	    }
	    $(".right-sidebar").toggleClass("toggled-on");
    })
    // normal-menu are in superfish

    // login
    $('.show-layer').on('click', function () {
        var layerid = $(this).data('show-layer');
        showLayer(layerid)
    });
    $('.login-overlay').on('click', function (event) {
        if (event.target == this) {
          hideLayer()
        }
    });
    $('#login-tab span:first').addClass('login-current');
    $('#login-tab .login-tab-bd-con:gt(0)').hide();
    $('#login-tab span').click(function () {
        $(this).addClass('login-current').siblings('span').removeClass('login-current');
        $('#login-tab .login-tab-bd-con:eq(' + $(this).index() + ')').show().siblings('.login-tab-bd-con').hide().addClass('login-current')
    });
    function showLayer(id) {
        var layer = $('#' + id),
        layerwrap = layer.find('#login');
        layer.fadeIn();
        layerwrap.css({
          'margin-top': - layerwrap.outerHeight() / 2
        })
    }
    function hideLayer() {
        $('.login-overlay').fadeOut()
    }

    // Refresh captcha
	$('img.captcha_img').on('click',function(){
	    var captcha = $('#tplUrl').val()+'/inc/captcha.php?'+Math.random();
	    $(this).attr('src',captcha);
	});

	//禁止右键，禁止拖拽
    $('html').on('contextmenu dragstart',function(){
	    //return false;
    })

    //绘制当前页二维码
    /*
     * 圆角矩形，填充背景图像
     * @parama int/float ctx        画布
     * @parama int/float x          矩形位置x坐标
     * @parama int/float y          矩形位置y坐标
     * @parama int/float w          矩形宽度
     * @parama int/float h          矩形高度
     * @parama int/float r          圆角半径
     * @parama RGB bg               矩形背景图
     */
    function drawRoundedImg(ctx,x,y,w,h,r,bdWidth,bdColor,bgcolor,bgimg){
        ctx.save();
        ctx.beginPath();
        ctx.moveTo(x+r,y);
        ctx.lineWidth = bdWidth;
        ctx.strokeStyle = bdColor;
        ctx.fillStyle = bgcolor;
        ctx.arcTo(x+w,y,x+w,y+h,r);
        ctx.arcTo(x+w,y+h,x,y+h,r);
        ctx.arcTo(x,y+h,x,y,r);
        ctx.arcTo(x,y,x+w,y,r);
        ctx.stroke();
        ctx.clip();
        ctx.drawImage(bgimg, x, y, w, h);
        ctx.restore();
        ctx.closePath();
    }
    /*
     * 圆角矩形，填充背景色
     * @parama int/float ctx        画布
     * @parama int/float x          矩形位置x坐标
     * @parama int/float y          矩形位置y坐标
     * @parama int/float w          矩形宽度
     * @parama int/float h          矩形高度
     * @parama int/float r          圆角半径
     * @parama int/float bdWidth    矩形边框尺寸
     * @parama RGB bdColor          矩形边框颜色
     * @parama RGB bgcolor          矩形背景色
     */
    function drawRoundedRect(ctx,x,y,w,h,r,bdWidth,bdColor,bgcolor){
        ctx.beginPath();
        ctx.moveTo(x+r,y);
        ctx.lineWidth = bdWidth;
        ctx.strokeStyle = bdColor;
        ctx.fillStyle = bgcolor;
        ctx.arcTo(x+w,y,x+w,y+h,r);
        ctx.arcTo(x+w,y+h,x,y+h,r);
        ctx.arcTo(x,y+h,x,y,r);
        ctx.arcTo(x,y,x+w,y,r);
        ctx.stroke();
        ctx.fill();
        ctx.closePath();
    }
    function makeQrcode(){
        //生成二维码
        var url = window.location.href;
        var qrcode = $('#qrcode_init').qrcode(url).hide();//生成二维码
        var ercode = qrcode.find('canvas').get(0);//查找jq的canvas对象并转换为原生的dom对象
        //canvas绘图
        var canvas=document.getElementById("qrcode_canvas");//获取画布
        var canW = 200;//设置画布宽度
        var canH = 200;//设置画布高度
        var erw = 180;//二维码宽度
        var erh = 180;//二维码高度
        var erx = 10;//左边距
        var ery = 10;//上边距
        canvas.width = canW;
        canvas.height = canH;
        var ctx = canvas.getContext("2d");//设置二维绘图
        //---
        drawRoundedRect(ctx,0,0,canW,canH,0,0,"#fff","#fff");//绘制底层(画布)

        ctx.drawImage(ercode,erx,(canH-ery-erh),erw,erh);//二维码
        drawRoundedRect(ctx,(erx+((erw-48)/2)),((canH-ery-48)-((erh-48)/2)),48,48,5,0,"#fff","#fff");//绘制LOGO背景
        drawRoundedRect(ctx,(erx+((erw-48)/2)+2),((canH-ery-44)-((erh-44)/2)),44,44,5,1,"#444","#fff");//绘制LOGO边框
        

        //---logo图像填充
        var sharelogo = new Image();
        sharelogo.crossOrigin = "Anonymous";//允许跨域
        //sharelogo.setAttribute('crossOrigin', 'Anonymous');
        sharelogo.onload = function(){
            drawRoundedImg(ctx,(erx+((erw-48)/2)+4),((canH-ery-40)-((erh-40)/2)),40,40,5,0,"#fff","#fff",sharelogo);//LOGO图像
            $("#qrcode_canvas").hide();// 隐藏画布
            $(".qrcode img").attr("src",canvas.toDataURL('image/jpg'));//将canva转为base64并赋值给img标签
        }
        sharelogo.src = document.getElementById("logo").src;
        //注：在预加载模式下，onload应该放在为src赋值的上面，以避免已有缓存的情况下无法触发onload事件从而导致onload中的事件不执行的情况发生
        
    }
    makeQrcode();

    //$.fn.test = function(){
    //    console.log(this);
    //}
    //var tst = $("body");
    //tst.test();

    // 评论跟随
    $('body').on('click', '.comment-reply-link', function(){
        addComment.moveForm("div-comment-"+$(this).attr('data-commentid'), $(this).attr('data-commentid'), "respond", $(this).attr('data-postid') );
        return false;
    });

    //获取url中的参数
    function getUrlParam(name) {
        var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)"); //构造一个含有目标参数的正则表达式对象
        var r = window.location.search.substr(1).match(reg); //匹配目标参数
        if (r != null) return unescape(r[2]); return null; //返回参数值
    }

    // 显示超长文字
    $(".textEllipsis, .thumb .cat").on('mouseover', function(){
	    $(this).data('tipso', $(this).text().trim());
    });
    $(".textEllipsis, .thumb .cat").tipso({
        useTitle: false,
        background: "#000",
        color: "#fff"
        /*,content: $(".textEllipsis").text().trim()*/
    });

    // 图片懒加载
    $("img.lazy").lazyload({
        effect: "fadeIn",
        threshold: 100,
        failure_limit: 70
    });

    //顶部菜单显示\隐藏
    $.fn.headroom = function (option) {
        return this.each(function () {
            var $this = $(this),
            data = $this.data('headroom'),
            options = typeof option === 'object' && option;
            options = $.extend(true, {
            }, Headroom.options, options);
            if (!data) {
                data = new Headroom(this, options);
                data.init();
                $this.data('headroom', data)
            }
            if (typeof option === 'string') {
                data[option]();
                if (option === 'destroy') {
                    $this.removeData('headroom')
                }
            }
        })
    }

    //
    $('.weixin-box').on('mouseover',function(){
        $(this).children(".weixin-qr").show()
    })
    $('.weixin-box').on('mouseout',function(){
        $(this).children(".weixin-qr").hide()
    })


    // 跑马器
    $.fn.running = function () {
        function n() {
            var t = $('.animateNum');//需要使用跑马器的数字容器className
            var n = {
                top: $(window).scrollTop(),
                bottom: $(window).scrollTop() + $(window).height()
            };
            t.each(function () {
                var t = $(this).data('animatetarget');
                n.top <= $(this).offset().top + $(this).height() && n.bottom >= $(this).offset().top && !$(this).data('start') && ($(this).data('start', !0), new AnimateNum({
                    obj: $(this),
                    target: t,
                    totalTime: 1000
                }))
            })
        }
        $(window).bind('scroll', function () {
            n();
        });
        function AnimateNum(t) {
            this.obj = t.obj,
            this.target = t.target.toString(),
            this.totalTime = t.totalTime || 1000,
            this.init()
        }
        AnimateNum.prototype = {
            init: function () {
                return this.target ? (this.animation(), void 0)    : !1
            },
            animation: function () {
                var t = this,
                i = this.target.indexOf('.'),
                e = 0;
                i >= 0 && (e = this.target.length - i - 1);
                var n = this.target.replace('.', ''),
                s = this.totalTime / 10 | 0,
                a = n / s | 0,
                r = 0,
                h = 0;
                t.timer = setInterval(function () {
                    r++,
                    h += a,
                    t.obj.html(h / Math.pow(10, e)),
                    r >= s && (clearInterval(t.timer), t.obj.html(t.target))
                }, 100)
            }
        }
    }
    // 开始跑马
    $('.animateNum').running();
    
    $('#header-main').headroom({
        'tolerance': 10,
        'offset': 89,
        'classes': {
            'initial': 'sliding',
            'pinned': 'slideDown',
            'unpinned': 'slideUp'
        }
    });

    //sidebar-tabs
    function active_tab_content(tab_name) {
		var tab_content = $("#" + tab_name + "-container");
		tab_content.addClass('actived').siblings().removeClass('actived');
	}
    $('.widget_yi_tabs .tabs-nav .tab-title:first-child').addClass('selected');
    $('.widget_yi_tabs .tabs-nav a').on('click', function(){
	    $(this).parent('span').addClass('selected').siblings().removeClass("selected");
	    var tab_name = this.id.slice(0, -4);
	    active_tab_content(tab_name);
    })
    // front-tabs
    $('#layout-tab .tab-hd-con:first-child').addClass('current');
    $('#layout-tab .tab-hd-con').on('click', function(){
	    $(this).addClass("current").siblings("span").removeClass("current");
  		$("#layout-tab .tab-bd-con:eq(" + $(this).index() + ")").show().siblings(".tab-bd-con").hide().addClass("current");
    })
})(jQuery);

var swiper_index = new Swiper('.swiper-container', {
    loop : true,
    spaceBetween: 30,
    centeredSlides: true,
    autoplay: {
        delay: 4500,
        disableOnInteraction: false,
    },
    pagination: {
        el: '.swiper-pagination',
        clickable: true,
    },
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
});