/*社会主义核心价值观*/
var a_idx = 0;
jQuery(document).ready(function($) {
    $("html").click(function(e) {
        var area = $(window);
        var a = new Array("富强", "民主", "文明", "和谐", "自由", "平等", "公正" ,"法治", "爱国", "敬业", "诚信", "友善");
        var $i = $("<span/>").text(a[a_idx]);
        a_idx = (a_idx + 1) % a.length;
        var x = e.pageX,
        y = e.pageY;
        var domWidth = 40,
            domHeight = 30,
            domTop = y+domHeight>area.height() ? y-domHeight : y;
            domLeft = x+domWidth>area.width() ? x-domWidth : x;
        $i.css({
            "width": 40,
            "height": 30,
            "line-height": "30px",
            "text-align": "center",
            "font-weight": "bold",
            "color": "#ff6651",
            "white-space":"nowrap",
            "position": "absolute",
            "top": domTop,
            "left": domLeft,
            "z-index": 9999
        });
        $("body").append($i);
        $i.animate({
            "top": y - 180,
            "opacity": 0
        },
        1500,
        function() {
            $i.remove();
        });
    });
});