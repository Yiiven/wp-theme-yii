<?php 
/**
 * Template Name: categroy
 * The template for displaying categroy.
 * @package yii
 * 分类页面，在访问分类时，使用此页替代进行展示(如果本页不存在，将使用archive?:index展示)
 */
 ?>
<?php get_header(); ?>
    <div class="container categroy-container">
        <?php if(have_posts()) : //检查博客是否有日志 ?>
        <main class="main">
            <div class="block-title">
                <div class="title textEllipsis" data-tipso="文章分类">文章分类</div>
                <div class="line left-line"></div>
                <div class="line right-line"></div>
            </div>
            <div class="block-content">
            <?php while(have_posts()) : the_post(); //执行 the_post() 去调取日志 ?>
                <?php get_template_part("template/article") ?>
            <?php endwhile; ?>
            </div>
        </main>
        <div class="page-navigation">
            <?php yi_paged(); ?>
        </div>
        <?php else : //博客没有日志的时候执行 ?>
        <div class="main">
            <div class="post">
                <h2><?php _e('博主很懒，什么也没留下...'); ?></h2>
            </div>
        </div>
        <?php endif; ?>
    </div>
<?php get_footer(); ?>