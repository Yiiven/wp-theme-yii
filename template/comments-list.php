<?php
    // 载入评论列表处理类
    require get_template_directory().'/classes/class-yi-walker-comment.php';
    
    $comments_args = array(
        'walker'            => new yi_walker_comment(),// 自定义样式类名
        'max_depth'         => '',// 最大评论级数
        'style'             => 'ul',// 列表方式，ul-无序列表，ol-有序列表
        'reply_text'        => __('回复', 'yii'),// 回复链接的文本
        //'login_text'        => '',// 登录按钮文本
        //'callback'          => null,// 回调函数
        //'end-callback'      => null,// 列表完成后的回调函数
        //'type'              => 'all',// 列出留言的方式，all-全部, comment-评论, pingback, trackback-引用, 'pings'
        //'page'              => '',// 显示分页中指定页
        //'per_page'          => '',// 每页显示的留言条数
        'avatar_size'       => 50,// 头像尺寸
        'reverse_top_level' => false,// 排序方式，true-最新留言，false-最早留言，null-面板设置
        //'reverse_children'  => '',// 是否反转列表中的子留言
        //'format'            => current_theme_supports( 'html5', 'comment-list' ) ? 'html5' : 'xhtml',
        //'short_ping'        => false,// 是否输出短ping
        //'echo'              => true,// 是否直接输出到页面，true-输出，false-返回给另一个函数做为变量使用
    );
    wp_list_comments($comments_args);//评论\留言列表 

