<p>
    CopyRight &#169; 2017 - <?php _e(date("Y",time())); ?>
    <a href="<?php bloginfo('url'); ?>"><?php bloginfo('name'); ?></a> 基于 WP主题 <a href="<?php bloginfo('url'); ?>">yii</a>
    托管于 <a href="//www.huaweicloud.com" target="_blank">华为云</a>
</p>
<p>
    <?php 
    if(_yi('site_create_date')):
        if(_yi('site_status_days')):
        ?>
        本站已安全运行<?php echo floor((time()-strtotime(trim(_yi('site_create_date'))))/86400); ?>天
        <span style="position: relative;top: 0.125rem;width:1px;border-left:2px solid #666;height:40%;margin:0 5px;display:inline-block;"></span>
        <?php
        endif;
    endif;
    if(_yi('site_status_sql')):
        ?>
        本页共查询<?php echo get_num_queries(); ?>次
        <span style="position: relative;top: 0.125rem;width:1px;border-left:2px solid #666;height:40%;margin:0 5px;display:inline-block;"></span>
        <?php
    endif;
    if(_yi('site_status_sql_time')):
        ?>
        用时<?php echo timer_stop(0, 3); ?>秒
        <span style="position: relative;top: 0.125rem;width:1px;border-left:2px solid #666;height:40%;margin:0 5px;display:inline-block;"></span>
        <?php
    endif;
    if(_yi('site_status_sql_memery')):
        ?>
        消耗内存<?php echo round(memory_get_peak_usage()/1024/1024, 2); ?>MB
        <?php
    endif;
    ?>
</p>
<p>
    <a style="display: inline-block;" href="http://beian.miit.gov.cn/" target="_blank" rel="noopener noreferrer">
        <span style="float: left;height: 20px;line-height: 20px;">
            <img style="float: left;" src="<?php echo get_template_directory_uri(); ?>/images/ICPB.png"><?php the_yi('beian_icp'); ?>
        </span>
    </a>
    <a style="display: inline-block;" target="_blank" href="http://www.beian.gov.cn/portal/registerSystemInfo?recordcode=<?php echo getStringNum(_yi('beian_wangan'))?>" rel="noopener noreferrer">
        <span style="float: left;height: 20px;line-height: 20px;">
            <img style="float: left;" src="<?php echo get_template_directory_uri(); ?>/images/GAWB.png"><?php the_yi('beian_wangan'); ?>
        </span>
    </a>
</p>