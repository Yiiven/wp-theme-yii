<div class="login-overlay" id="login-layer">
    <div id="login">
        <div id="login-tab" class="login-tab-product fadeInDown animated">
            <div class="login-tab-hd">
                <span class="login-tab-hd-con login-tab-hd-con-b login-current">
                    <a href="javascript:">登录</a>
                </span>
                <span class="login-tab-hd-con">
                    <a href="javascript:">注册</a>
                </span>
                <span class="login-tab-hd-con">
                    <a href="javascript:">找回密码</a>
                </span>
            </div>
            <div class="login-tab-bd login-dom-display">
                <div class="login-tab-bd-con login-current">
                    <div id="tab1_login" class="tab_content_login">
                        <form id="login-box" class="wp-user-form" action="<?php bloginfo('url'); ?>/wp-login.php" method="post" novalidate="novalidate">
                            <p class="status"></p>
                            <p>
                                <label class="icon" for="user_name">用户名</label>
                                <input class="input-control" id="user_name" type="text" placeholder="" name="user_name" required="" aria-required="true">
                            </p>
                            <p>
                                <label for="user_pass">密码</label>
                                <input class="input-control" id="user_pass" type="password" placeholder="" name="user_pass" required="" aria-required="true">
                            </p>
                            <p class="login-form"><?php do_action('login_form'); ?></p>
                            <p class="login_fields">
                                <p class="rememberme">
                                    <label for="rememberme">
                                    <input type="checkbox" name="rememberme" value="forever" checked="checked" id="rememberme" class="keep-input">记住我的登录信息</label>
                                </p>
                                <input class="user-submit" type="submit" name="user-submit" value="登录">
                                <input type="hidden" id="security" name="security" value="<?php echo wp_create_nonce('signin');?>">
                                <input type="hidden" name="redirect_to" value="<?php echo yi_get_current_page_url(); ?>">
                                <input type="hidden" name="user-cookie" value="1">
                            </p>
                        </form>
                    </div>
                </div>
                <div class="login-tab-bd-con">
                    <div id="tab2_login" class="tab_content_register">
                        <form id="register" method="post" action="<?php bloginfo('url') ?>/wp-login.php?action=register" class="wp-user-form">
                            <p class="status"></p>
                            <p class="username">
                                <label for="user_name" class="hide">用户名</label>
                                <input type="text" name="user_name" value="" size="20" id="user_name" required="" aria-required="true">
                            </p>
                            <p class="user_email">
                                <label for="user_email" class="hide">电子邮件地址</label>
                                <input type="text" name="user_email" value="" size="20" id="user_email" required="" aria-required="true">
                            </p>
                            <p class="user_pass">
                                <label for="user_pass" class="hide">密码</label>
                                <input type="password" name="user_pass" value="" size="20" id="user_pass" required="" aria-required="true">
                            </p>
                            <p class="user_pass">
                                <label for="user_re_pass" class="hide">重复密码</label>
                                <input type="password" name="user_re_pass" value="" size="20" id="user_re_pass" required="" aria-required="true">
                            </p>
                            <p class="captcha">
                                <label for="captcha">验证码</label><br>
                                <input class="input-control inline captcha-input" type="text" id="captcha" name="captcha" placeholder="输入验证码" required>
                                <img src="<?php echo get_bloginfo('template_url'). '/inc/captcha.php'; ?>" class="captcha_img inline" alt="验证码" title="点击刷新验证码">
                                <input type="hidden" value="<?php echo get_bloginfo('template_url'); ?>" id="tplUrl">
                            </p>
                            <div class="login_fields">
                                <div class="login-form"></div>
                                <input type="submit" name="user-submit" value="注册" class="user-submit">
                                <input type="hidden" id="security" name="security" value="<?php echo wp_create_nonce('signup');?>">
                                <input type="hidden" name="redirect_to" value="<?php echo yi_get_current_page_url(); ?>?register=true">
                                <input type="hidden" name="user-cookie" value="1">
                            </div>
                        </form>
                    </div>
                </div>
                <div class="login-tab-bd-con">
                    <div id="tab3_login" class="tab_content_login">
                        <p class="message">输入用户名或电子邮箱地址，您会收到一封含有密码重置链接的电子邮件。</p>
                        <form method="post" action="<?php bloginfo('url') ?>/wp-login.php?action=lostpassword" class="wp-user-form">
                            <div class="username">
                                <label for="user_login" class="hide">用户名或电子邮件地址</label>
                                <input type="text" name="user_login" value="" size="20" id="user_log" required="required">
                            </div>
                            <div class="login_fields">
                                <div class="login-form"></div>
                                <input type="submit" name="user-submit" value="获取新密码" class="user-submit">
                                <input type="hidden" name="redirect_to" value="<?php echo yi_get_current_page_url(); ?>?reset=true">
                                <input type="hidden" name="user-cookie" value="1">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>