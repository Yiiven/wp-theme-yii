<ul id="scroll" class="scroll-box">
    <li><a class="scroll-h" title="返回顶部"><i class="yi yi-arrowup"></i></a></li>
    <li><a class="scroll-b" title="转到底部"><i class="yi yi-arrowdown"></i></a></li>
    <?php if (!is_front_page()) { ?>
    <li><a class="scroll-home" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php _e( '首页', 'begin' ); ?>" rel="home"><i class="yi yi-home"></i></a></li>
    <?php } ?>
    <?php if (is_single() || (is_page() && !is_front_page())) { ?>
    <li><a class="scroll-c" title="评论/留言"><i class="yi yi-speechbubble"></i></a></li>
    <?php } ?>
    <li><a id="translateLink" title="简体/繁体互转">繁</a></li>
    <li>
        <a class="page-qrcode" title="本页二维码">
            <i class="yi yi-qr-code"></i>
        </a>
        <span class="qr-img qrcode">
            <div id="qrcode_init" class="hidden"></div>
            <canvas id="qrcode_canvas" class="hidden">您的浏览器不支持 HTML5 canvas 标签。</canvas>
            <img src="" alt=""/>
            <div class="align-center">扫码用手机看</div>
            <!-- <span class="qrcode-output">
                <img id="qrcode" src="<?php //echo get_stylesheet_directory_uri(); ?>/qrcode.php?text=<?php //echo home_url(add_query_arg(array(),$wp->request)); ?>" alt="">
            </span> -->
        </span>
    </li>
</ul>
<script type="text/javascript">
    jQuery(document).ready(function($){
        //显示二维码
        $('.page-qrcode').mouseover(function () {
            $(this).siblings('.qr-img').show()
        });
        //隐藏二维码
        $('.page-qrcode').mouseout(function () {
            $(this).siblings('.qr-img').hide()
        });
        
        //回顶部
        $(".scroll-h").on("click",function(){
            $('html,body').animate({
                scrollTop: '0px'
            },get_scroll_timer("top"));
        });
        //去底部
        $(".scroll-b").on("click",function(){
            $('html,body').animate({
                scrollTop: $('.site-info').offset().top
            },get_scroll_timer("bottom"));
        });
        //写评论
        $(".scroll-c").on("click",function(){
            $('html,body').animate({
                scrollTop: $('#comments').offset().top
            },800);
        });
        $(".comment-scroll").on("click",function(){
            $('html,body').animate({
                scrollTop: $('#respond').offset().top
            },800);
        });
        //获取当前可视区域距离文档底部的距离
        function get_scroll_timer(point=""){
            var doc_h = $(document).height();//文档高度
            var view_h = $(window).height();//可视区域高度
            switch(point){
                case "top":
                    var scroll = $(window).scrollTop();//顶部不可视区域高度，即需要滚动的距离
                break;
                case "bottom":
                    var scroll = doc_h - $(window).scrollTop() - view_h;//底部不可视区域高度，即需要滚动的距离
                break;
                case "speech":
                    $('#comments').offset().top;
                break;
                default:
                break;
            }
            var timer = scroll/view_h * 1000;
            timer = timer < 1500 ? timer : 1000;
            return timer;
        }
    });

    // 简\繁互转
    var defaultEncoding = 0; 
    var translateDelay = 1000;
    var cookieDomain = '<?php bloginfo("url"); ?>';
    var msgToTraditionalChinese = '繁';
    var msgToSimplifiedChinese = '简';
    var translateButtonId = 'translateLink';
    translateInitilization();
    /**
     * 
    $(document).height()：整个网页的文档高度
    $(window).height()：浏览器可视窗口的高度
    $(window).scrollTop()：浏览器可视窗口顶端距离网页顶端的高度（垂直偏移）
    $(document.body).height();//浏览器当前窗口文档body的高度
    $(document.body).outerHeight(true);//浏览器当前窗口文档body的总高度 包括border padding margin
    $(window).width(); //浏览器当前窗口可视区域宽度
    $(document).width();//浏览器当前窗口文档对象宽度
    $(document.body).width();//浏览器当前窗口文档body的宽度
    $(document.body).outerWidth(true);//浏览器当前窗口文档body的总宽度 包括border padding margin
     */
  
</script>