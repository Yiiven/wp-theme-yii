<div class="tab-site wow fadeInUp sort" data-wow-delay="0.3s" name="">
	<div id="layout-tab" class="tab-product">
	    <h2 class="tab-hd">
		    <span class="tab-hd-con"><a href="javascript:">推荐</a></span>
		    <span class="tab-hd-con"><a href="javascript:">前端</a></span>
		    <span class="tab-hd-con"><a href="javascript:">主题</a></span>
	    </h2>

		<div class="tab-bd dom-display">

			<ul class="tab-bd-con current">
				<?php query_posts('showposts=8&cat=14'); while (have_posts()) : the_post(); ?>
				<?php the_title( sprintf( '<li class="list-title"><i class="yi yi-arrowright"></i><a href="%s" rel="bookmark">', esc_url(get_permalink())), '</a></li>'); ?>
				<?php endwhile; ?>
				<?php wp_reset_query(); ?>
			</ul>

			<ul class="tab-bd-con">
				<?php query_posts('showposts=8&cat=41'); while (have_posts()) : the_post(); ?>
				<?php the_title( sprintf( '<li class="list-title"><i class="yi yi-arrowright"></i><a href="%s" rel="bookmark">', esc_url(get_permalink())), '</a></li>'); ?>
				<?php endwhile; ?>
				<?php wp_reset_query(); ?>
		    </ul>

			<ul class="tab-bd-con">
				<?php query_posts('showposts=8&cat=43'); while (have_posts()) : the_post(); ?>
				<?php the_title( sprintf( '<li class="list-title"><i class="yi yi-arrowright"></i><a href="%s" rel="bookmark">', esc_url(get_permalink())), '</a></li>'); ?>
				<?php endwhile; ?>
				<?php wp_reset_query(); ?>
			</ul>

		</div>
	</div>
</div>
<div class="clear"></div>