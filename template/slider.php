<?php if(_yi('index_slider')): ?>
<div id="slideshow" class="swiper-container fadeInUp">
    <ul id="slider" class="swiper-wrapper">
        <?php
        $slider_show = _yi("index_slider_show");
        for($i=1;$i<($slider_show+1);$i++){
            $images[$i-1]['url'] = _yi("index_slider".$i) ? _yi("index_slider".$i) : get_template_directory_uri()."/images/banner.jpg";
            $images[$i-1]['sort'] = _yi("index_slider".$i."_sort") ? _yi("index_slider".$i."_sort") : 0;
            $images[$i-1]['link'] = _yi("index_slider".$i."_link") ? _yi("index_slider".$i."_link") : "#";
        }
        // array_column ( array $input , mixed $column_key [, mixed $index_key = null ] ) : array
        // 从多维数组中返回单列数组。
        $slider_sort = array_column($images,'sort');
        // array_multisort(array $array1 [, mixed $array1_sort_order = SORT_ASC [, mixed $array1_sort_flags = SORT_REGULAR [, mixed $... ]]] ) : bool 
        // 对多个数组或多维数组进行排序，关联(string)键名保持不变，数字键名会被重新索引。 
        // $array1 - 要排序的数组
        // array1_sort_order - 排序方式，可与$array1_sort_flags互换
            // SORT_ASC-顺序
            // SORT_DESC-倒序
        // array1_sort_flags - 排序类型，参数可以和 array1_sort_order 交换或者省略，默认：SORT_REGULAR。
            // SORT_REGULAR-不修改
            // SORT_NUMERIC-按数字
            // SORT_STRING-按字符串
            // SORT_LOCALE_STRING-按本地化设置,可通过setlocale() 修改信息
            // SORT_NATURAL - 以字符串的"自然排序"，类似 natsort() 
            // SORT_FLAG_CASE - 可以组合 (按位或 OR) SORT_STRING 或者 SORT_NATURAL 大小写不敏感的方式排序字符串。
            //  ... 可选，可提供更多的数组，这些数组必须和$array1拥有相同的元素
        array_multisort($slider_sort,SORT_ASC,SORT_NUMERIC,$images);
        ?>
        <?php foreach ($images as $key => $value) { ?>
            <li class="swiper-slide"><a href="<?php echo $value['link'] ?>"><img src="<?php echo $value['url'] ?>" alt=""></a></li>
        <?php } ?>
    </ul>
    <div class="swiper-pagination"></div>
    <div class="yi yi-arrowleft swiper-button-prev"></div>
    <div class="yi yi-arrowright swiper-button-next"></div>
</div>
<?php endif; ?>