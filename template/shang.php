<div class="social">
    <div class="reward">
        <span class="love-box">
            <a data-action="love" data-id="<?php the_ID(); ?>" class="reward-button favorite<?php if(isset($_COOKIE['yi_love_'.$post->ID])) echo ' done';?>">
                <i class="yi yi-thumbs-up-o"></i>赞<em class="count-love">
                   <?php if( get_post_meta($post->ID,'yi_love', true) ){            
                       echo get_post_meta($post->ID,'yi_love', true);
                   } else {
                       echo '0';
                   }?>
                </em>
            </a>
        </span>
        <div class="share-box">
            <span class="reward-button share">
                <a><i class="yi yi-share"></i>分享</a>
            </span>
            <div id="share">
                <div class="yishare share-component social-share">
                    <?php
                        $url = urlencode(urldecode(get_the_permalink()));
                        $title = urlencode(get_the_title().'|'.get_bloginfo('name'));
                        $excerpt = urlencode(get_the_excerpt());
                        $thumb = urlencode(has_post_thumbnail() ? get_the_post_thumbnail() : (_yi('logo_image') ? _yi('logo_image') : get_bloginfo('template_url').'/images/logo.png'));
                    ?>
                    <a class="social-share-icon icon-qq" href="http://connect.qq.com/widget/shareqq/index.html?url=<?php echo $url; ?>&title=<?php echo $title; ?>&source=<?php echo $title; ?>&desc=<?php echo $excerpt; ?>&pics=<?php echo $thumb; ?>" target="_blank"></a>
                    <a class="social-share-icon icon-qzone" href="http://sns.qzone.qq.com/cgi-bin/qzshare/cgi_qzshare_onekey?url=<?php echo $url; ?>&title=<?php echo $title; ?>&desc=<?php echo $excerpt; ?>&summary=<?php echo $excerpt; ?>&site=<?php echo $title; ?>" target="_blank"></a>
                    <a class="social-share-icon icon-douban" href="http://shuo.douban.com/!service/share?href=<?php echo $url; ?>&name=<?php echo $title; ?>&text=<?php echo $excerpt; ?>&image=<?php echo $thumb; ?>&starid=0&aid=0&style=11" target="_blank"></a>
                    <a class="social-share-icon icon-weibo" href="http://service.weibo.com/share/share.php?url=<?php echo $url; ?>&title=<?php echo $title; ?>&pic=<?php echo $thumb; ?>&appkey=" target="_blank"></a>
                    <a class="social-share-icon icon-wechat" href="javascript:;" tabindex="-1">
                        <div class="wechat-qrcode bottom">
                            <h4>分享到微信朋友圈</h4>
                            <div class="qrcode">
                                <img src="">
                            </div>
                            <div class="help">
                                <p>微信扫一下</p>
                                <p>将本文分享至朋友圈</p>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
        <div class="shang-box">
            <div class="reward-button shang-empty"><span></span></div>
            <div class="shang-str">
                <span class="reward-button shang"><a title="赞助本站"><!-- &yen;打 -->赏</a></span>
            </div>
            <div class="reward-code">
                <a class="close-btn"><i class="yi yi-cross"></i></a>
                <h4>
                    <a href="tencent://message/?uin=461619545&Site=qq&Menu=yes" title="求安慰、求抱抱~~" <?php echo yi_target_blank(); ?>>
                        请站长喝杯茶吧！
                    </a>
                </h4>
                <span class="code-span alipay-code">
                    <b>支付宝扫这边</b>
                    <img class="alipay-img" src="<?php bloginfo('template_url'); ?>/images/pay-alipay.png">
                </span>
                <span class="code-span wechat-code">
                    <b>微信扫这边</b>
                    <img class="wechat-img" src="<?php bloginfo('template_url'); ?>/images/pay-weixin.png">
                </span>
            </div>
        </div>
        <div class="clear"></div>
    </div>
    <p class="reward-notice">如果文章对您有帮助，欢迎移至上方按钮打赏<strong style="color: #ff8700;"><?php the_author(); ?></strong></p>
    <div class="reward-close"></div>
</div>
<script type="text/javascript">
    (function($){
        $.fn.postLike = function(){
            if ($(this).hasClass('disabled')) {
                return false;
            }else{
                $(this).addClass('disabled');
                var id = $(this).data("id"),
                    action = $(this).data('action'),
                    rateHolder = $(this).children('.count-love');
                var ajax_data = {
                    action: "yi_posts_love",
                    um_id: id,
                    um_action: action
                };
                $.post("<?php echo admin_url(); ?>admin-ajax.php", ajax_data, function(data) {
                    $(rateHolder).html(data);
                });
                return false;
            }
        };
        $.fn.share = function(){
            $(this).parent().find("#share").toggle();
        }
        $.fn.shang = function(){
            $(".love-box,.share-box,.shang-empty").toggle();
        }
        $(".favorite").on("click", function() {
            $(this).postLike();
        });

        $(".reward-button.shang").on("click", function(){
            $(".reward-code, .reward-close").show();
        })
        $(".reward-button.shang").on("mouseover",function(){
            $(this).shang();
        })
        $(".reward-button.shang").on("mouseout",function(){
            $(this).shang();
        })
        $(".reward-button.share").on("click",function(){
            $(this).share();
        })
        $(".reward-close, .close-btn").on("click", function(){
            $(".reward-code, .reward-close").hide();
        })
    })(jQuery);  
</script>