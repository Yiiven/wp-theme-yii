<div class="smiley">
    <a href="javascript:;" id="comment-smiley" title="表情"><i class="yi yi-insertemoticon"></i></a>
    <a href="javascript:" id="font-color" title="颜色"><i class="yi yi-typeface"></i></a>
    <a href="javascript:SIMPALED.Editor.strong()" title="粗体"><i class="yi yi-bold"></i></a>
    <a href="javascript:SIMPALED.Editor.em()" title="斜体"><i class="yi yi-italic"></i></a>
    <a href="javascript:SIMPALED.Editor.quote()" title="引用"><i class="yi yi-quote-left"></i></a>
    <a href="javascript:SIMPALED.Editor.ahref()" title="插链接"><i class="yi yi-link"></i></a>
    <a href="javascript:SIMPALED.Editor.del()" title="删除线"><i class="yi yi-delete-line"></i></a>
    <a href="javascript:SIMPALED.Editor.underline()" title="下划线"><i class="yi yi-underline"></i></a>
    <a href="javascript:SIMPALED.Editor.code()" title="插代码"><i class="yi yi-code"></i></a>
    <a href="javascript:SIMPALED.Editor.img()" title="插图片"><i class="yi yi-picture"></i></a>
</div> 