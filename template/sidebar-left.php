<div id="left-widget-box" class="left-sidebar">
    <?php if ( is_active_sidebar( 'sidebar_left' ) ) :
        dynamic_sidebar( 'sidebar_left' );
    endif; ?>
</div>