<div id="right-widget-box" class="right-sidebar">
    <span class="right-sidebar-close"><i class="yi yi-cross"></i></span>
    <?php //get_sidebar("sidebar_right"); ?>
    <?php if ( is_active_sidebar( 'sidebar_right' ) ) :
        dynamic_sidebar( 'sidebar_right' );
    endif; ?>
</div>