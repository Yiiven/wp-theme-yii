<nav class="header-top">
    <div class="nav-top">
        <div id="user-profile">
            <div class="user-login">
            欢迎光临！
            </div>
            <div class="nav-set">
                <div class="nav-login">
                    <?php if(is_user_logged_in()){ ?>
                        <?php if(current_user_can('manage_options')){//要修改成多个角色的判断 ?>
                        <?php } ?>
                            <span class="manage" data-show-layer="manage" role="button">
                                <i class="yi yi-home"></i>
                                <a href="<?php echo admin_url(); ?>" <?php echo yi_target_blank() ?>><?php echo yi_get_admin_title(); ?></a>
                            </span>
                            <span class="logout" data-show-layer="logout" role="button">
                                <i class="yi yi-shutdown"></i>
                                <a href="<?php echo wp_logout_url(); ?>">登出</a>
                            </span>
                    <?php }else{ ?>
                        <span class="show-layer" data-show-layer="login-layer" role="button">
                            <i class="yi yi-timerauto"></i>登录/注册
                        </span>
                    <?php } ?>
                </div>
            </div>
            <div class="clear"></div>
        </div>
        <div class="top-menu">
            <div class="top-menu-nav">
                <a href="//mail.qq.com/cgi-bin/qm_share?t=qm_mailme&email=vance_720@foxmail.com" title="给管理员发送邮件" <?php echo yi_target_blank(); ?>>
                    <i class="yi yi-email"></i>
                </a>
                <a href="tencent://message/?uin=461619545&Site=qq&Menu=yes" title="广告投放联系" <?php echo yi_target_blank(); ?>>
                    <i class="yi yi-qq"></i>
                </a>
                <a href="<?php bloginfo("url"); ?>/feed" title="文章订阅" <?php echo yi_target_blank(); ?>>
                    <i class="yi yi-rss"></i>
                </a>
            </div>
        </div>
    </div>
</nav>