<?php
    $prev_post_link = yi_get_previous_post_link();
    $prev_post_title = yi_get_previous_post_title();
    $next_post_link = yi_get_next_post_link();
    $next_post_title = yi_get_next_post_title();
?>
<nav class="single-nav">
    <?php if($prev_post_link){ ?>
    <a href="<?php echo $prev_post_link; ?>" rel="prev" class="single-nav-prev">
        <span class="meta-nav">
            <span class="post-nav"><i class="yi yi-arrowleft"></i>上一篇</span>
            <span class="post-title"><?php echo $prev_post_title; ?></span>
        </span>
    </a>
    <?php }else{ ?>
        <span class="meta-nav">
            <span class="post-nav">没有了</span>
            <span class="post-title">已经是最早的文章了</span>
        </span>
    <?php } ?>
    <?php if($next_post_link){ ?>
    <a href="<?php echo $next_post_link; ?>" rel="next" class="single-nav-next">
        <span class="meta-nav">
            <span class="post-nav">下一篇<i class="yi yi-arrowright"></i></span>
            <span class="post-title"><?php echo $next_post_title; ?></span>
        </span>
    </a>
    <?php }else{ ?>
        <span class="meta-nav">
            <span class="post-nav">没有了</span>
            <span class="post-title">已经是最新文章了</span>
        </span>
    <?php } ?>
</nav>


<nav class="navigation post-navigation">
    <h2 class="screen-reader-text">文章导航</h2>
    <div class="nav-links">
        <div class="nav-prev">
            <a href="<?php echo $prev_post_link; ?>" rel="prev" class="single-nav-prev">
                <span class="meta-nav-left"><i class="yi yi-arrowleft"></i></span>
            </a>
        </div>
        <div class="nav-next">
            <a href="<?php echo $next_post_link; ?>" rel="next" class="single-nav-next">
                <span class="meta-nav-right"><i class="yi yi-arrowright"></i></span>
            </a>
        </div>
    </div>
</nav>
<div class="clear"></div>