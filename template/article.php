<article id="post-<?php the_ID(); //调取日志ID ?>" class="post">
    <div class="entry">
        <div class="article-inner">
            <?php if(has_post_thumbnail()){ ?>
            <figure class="thumb">
                <?php if(!in_category(1)){ ?>
                <span class="cat">
                    <?php the_category('/');//文章分类，多个分类用"指定符号"分割 ?>
                </span>
                <?php } ?>
                <!-- category end -->
                <?php if(has_post_thumbnail()){ ?>
                <a class="entry-img" href="<?php the_permalink(); ?>">
                    <?php the_post_thumbnail();//默认尺寸的特色图 ?>
                    <?php //the_post_thumbnail( 'mobile-post-thumbnail' );//插入自定义名称和尺寸的特色图，一般使用小图片为手机端提供支持 ?> 
                </a>
                <?php } ?>
                <!-- image -->
            </figure>
            <?php } ?>
            <header class="entry-header">
                <h2 class="entry-title"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h2>
            </header><!-- header -->
            <div class="entry-content">
                <div class="entry-summary">
                    <?php the_excerpt();//摘要 ?>
                    <?php //the_content();//全文 ?>
                </div><!-- summary -->
                <span class="<?php if(has_post_thumbnail()): ?>entry-meta <?php else: ?>entry-meta-no<?php endif; ?>">
                    <span class="date"><i class="yi yi-date"></i><?php the_time('m-d'); ?></span>
                    <span class="time"><i class="yi yi-time"></i><?php the_time('H:i:s'); ?></span>
                    <span class="author"><i class="yi yi-author"></i><?php the_author();//文章作者 ?></span>
                    <span class="comment"><a href="<?php the_permalink(); ?>#comment"><i class="yi yi-comment"></i><?php comments_number("0", "1", "%");//评论数统计 ?></a></span>
                    <span class="view"><i class="yi yi-eye"></i><?php echo yi_get_post_views($post -> ID); ?></span>
                </span><!-- meta -->
                <div class="clear"></div>
            </div>
            <span class="entry-more more-roll">
                <a href="<?php the_permalink(); ?>">全文阅读</a>
            </span><!-- more -->
        </div>
    </div>
</article>