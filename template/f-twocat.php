<?php
    while (have_posts()):
        the_post();
        $do_not_duplicate[] = $post->ID;
        $do_show[] = $post->ID;
    endwhile;
    $recent = new WP_Query(array(
        'posts_per_page' => 5,
        'post__not_in' => $do_show,
        'category__not_in' => explode(',', '14,41'),
    ));
    while($recent->have_posts()) :
        $recent->the_post();
        $do_not_duplicate[] = $post->ID;
    endwhile;
?>
<div class="line-small sort" name="序号">
	<?php
	    $display_categories = explode(',', '14,41');
	    foreach ($display_categories as $category){
        query_posts(array(
            'showposts' => 1,
            'cat' => $category,
            'post__not_in' => $do_not_duplicate,
        ));
    ?>
    	<div class="md2">
    		<div class="cat-container wow fadeInUp" data-wow-delay="0.3s">
    			<h3 class="cat-title">
        			<a href="<?php echo get_category_link($category);?>">
        		    	<i class="title-icon yi yi-qq"></i>
            			<?php single_cat_title(); ?>
            			<span class="more-i">
                			<span></span>
                			<span></span>
                			<span></span>
            			</span>
            		</a>
            	</h3>
    			<div class="clear"></div>
    			<div class="cat-site">
    				<?php while (have_posts()): the_post();//不显示第一篇摘要 ?>
				        <figure class="small-thumbnail">
    				        <a href="<?php echo esc_url(get_permalink()); ?>" rel="bookmark">
        				        <?php echo yi_get_post_thumbnail(); ?>
        				    </a>
    				    </figure>
    				    <h2 class="entry-small-title">
        				    <a href="<?php echo esc_url(get_permalink()); ?>" rel="bookmark"><?php the_title();?></a>
        				</h2>
    				<?php endwhile; ?>
    				<div class="clear"></div>
    				<ul class="cat-list">
    					<?php
    					query_posts(array(
    					    'showposts' => 4,//分类列表显示篇数
    					    'cat' => $category,//分类ID
    					    'offset' => 1,
    					    'post__not_in' => $do_not_duplicate,
    					));
    					?>
    					<?php while (have_posts()) : the_post(); ?>
    						<?php if (1){ //是否显示日期 ?>
    							<li class="list-date"><?php the_time('m/d'); ?></li>
    							<?php if (get_post_meta($post->ID, 'mark', true)){
    								the_title( sprintf( '<li class="list-title"><i class="be be-arrowright"></i><a href="%s" rel="bookmark">', esc_url( get_permalink())), '</a><span class="t-mark">'.$mark = get_post_meta($post->ID, 'mark', true).'</span></li>');
    							}else{
    								the_title( sprintf( '<li class="list-title"><i class="be be-arrowright"></i><a href="%s" rel="bookmark">', esc_url( get_permalink())), '</a></li>');
    							} ?>
    						<?php }else{ ?>
    							<?php if (get_post_meta($post->ID, 'mark', true)){
    								the_title(sprintf('<li class="list-title-date"><i class="be be-arrowright"></i><a href="%s" rel="bookmark">', esc_url( get_permalink())), '</a><span class="t-mark">'.$mark = get_post_meta($post->ID, 'mark', true).'</span></li>');
    							} else {
    								the_title(sprintf('<li class="list-title-date"><i class="be be-arrowright"></i><a href="%s" rel="bookmark">', esc_url( get_permalink())), '</a></li>');
    							} ?>
    						<?php } ?>
    					<?php endwhile; ?>
    					<?php wp_reset_query(); ?>
    				</ul>
    			</div>
    		</div>
    	</div>
	<?php } ?>
	<div class="clear"></div>
</div>