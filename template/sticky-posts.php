<?php
$sticky = get_option('sticky_posts');
rsort( $sticky );
$args = array(
    'post__in' => $sticky,
    'showposts'        => 3,
    'ignore_sticky_posts' => 1
);
query_posts($args);
while (have_posts()) : the_post();
    get_template_part( 'template/article' ); 
endwhile;
wp_reset_query();
