<?php

class widget_yi_total extends WP_Widget {
    function __construct(){
        $widget_ops = array(
            'classname' => 'widget_yi_total',
            'description' => __('显示日志统计等信息', 'yii'),
            'customize_selective_refresh' => true,
        );
        parent::__construct('widget_yi_total', 'YI-网站统计', $widget_ops);
    }

    function widget( $args, $instance ) {
        extract( $args );
        global $wpdb;

        $title = apply_filters('widget_name', $instance['title']);
        $code  = isset($instance['code']) ? $instance['code'] : '';

        $total = "";
        $total .= $before_widget;
        $total .= $before_title.$title.$after_title; 
        $total .= '<ul>';
        
        if( isset($instance['post']) ){
            $count_posts = wp_count_posts();
            $total .= '<li><strong>日志总数：</strong>'.$count_posts->publish.'</li>';
        }

        if( isset($instance['comment']) ){
            $comments = $wpdb->get_var("SELECT COUNT(*) FROM $wpdb->comments");
            $total .= '<li><strong>评论总数：</strong>'.$comments.'</li>';
        }

        if( isset($instance['tag']) ){
            $total .= '<li><strong>标签总数：</strong>'.wp_count_terms('post_tag').'</li>';
        }

        if( isset($instance['page']) ){
            $count_pages = wp_count_posts('page');
            $total .= '<li><strong>页面总数：</strong>'.$count_pages->publish.'</li>';
        }

        if( isset($instance['cat']) ){
            $total .= '<li><strong>分类总数：</strong>'.wp_count_terms('category').'</li>';
        }

        if( isset($instance['link']) ){
            $links = $wpdb->get_var("SELECT COUNT(*) FROM $wpdb->links WHERE link_visible = 'Y'");
            $total .= '<li><strong>链接总数：</strong>'.$links.'</li>';
        }

        if( isset($instance['user']) ){
            $users = $wpdb->get_var("SELECT COUNT(ID) FROM $wpdb->users");
            $total .= '<li><strong>用户总数：</strong>'.$users.'</li>';
        }

        if( isset($instance['last']) ){
            $last = $wpdb->get_results("SELECT MAX(post_modified) AS MAX_m FROM $wpdb->posts WHERE (post_type = 'post' OR post_type = 'page') AND (post_status = 'publish' OR post_status = 'private')");
            $last = date('Y-m-d', strtotime($last[0]->MAX_m));
            $total .= '<li><strong>最后更新：</strong>'.$last.'</li>';
        }

        $total .= '</ul>';
        $total .= $after_widget;
        echo $total;
    }

    function form($instance) {
        $defaults = array( 
            'title' => '网站统计',
            'post' => 0,//日志统计
            'comment' => 0,//评论统计
            'tag' => 0,//标签统计
            'page' => 0,//页面统计
            'cat' => 0,//分类统计
            'link' => 0,//链接统计
            'user' => 0,//用户统计
            'last' => 0,//最后更新
        );
        $instance = wp_parse_args( (array) $instance, $defaults );
    ?>
    <p>
        <label for="<?php echo $this->get_field_id('title'); ?>">标题：</label>
        <input id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $instance['title']; ?>" class="widefat" />
    </p>
    <p>
        <input class="checkbox" type="checkbox" <?php checked( $instance['post'], 'on' ); ?> id="<?php echo $this->get_field_id('post'); ?>" name="<?php echo $this->get_field_name('post'); ?>">
        <label for="<?php echo $this->get_field_id('post'); ?>">显示日志总数</label>
    </p>
    <p>
        <input class="checkbox" type="checkbox" <?php checked( $instance['comment'], 'on' ); ?> id="<?php echo $this->get_field_id('comment'); ?>" name="<?php echo $this->get_field_name('comment'); ?>">
        <label for="<?php echo $this->get_field_id('comment'); ?>">显示评论总数</label>
    </p>
    <p>
        <input class="checkbox" type="checkbox" <?php checked( $instance['tag'], 'on' ); ?> id="<?php echo $this->get_field_id('tag'); ?>" name="<?php echo $this->get_field_name('tag'); ?>">
        <label for="<?php echo $this->get_field_id('tag'); ?>">显示标签总数</label>
    </p>
    <p>
        <input class="checkbox" type="checkbox" <?php checked( $instance['page'], 'on' ); ?> id="<?php echo $this->get_field_id('page'); ?>" name="<?php echo $this->get_field_name('page'); ?>">
        <label for="<?php echo $this->get_field_id('page'); ?>">显示页面总数</label>
    </p>
    <p>
        <input class="checkbox" type="checkbox" <?php checked( $instance['cat'], 'on' ); ?> id="<?php echo $this->get_field_id('cat'); ?>" name="<?php echo $this->get_field_name('cat'); ?>">
        <label for="<?php echo $this->get_field_id('cat'); ?>">显示分类总数</label>
    </p>
    <p>
        <input class="checkbox" type="checkbox" <?php checked( $instance['link'], 'on' ); ?> id="<?php echo $this->get_field_id('link'); ?>" name="<?php echo $this->get_field_name('link'); ?>">
        <label for="<?php echo $this->get_field_id('link'); ?>">显示链接总数</label>
    </p>
    <p>
        <input class="checkbox" type="checkbox" <?php checked( $instance['user'], 'on' ); ?> id="<?php echo $this->get_field_id('user'); ?>" name="<?php echo $this->get_field_name('user'); ?>">
        <label for="<?php echo $this->get_field_id('user'); ?>">显示用户总数</label>
    </p>
    <p>
        <input class="checkbox" type="checkbox" <?php checked( $instance['last'], 'on' ); ?> id="<?php echo $this->get_field_id('last'); ?>" name="<?php echo $this->get_field_name('last'); ?>">
        <label for="<?php echo $this->get_field_id('last'); ?>">显示最后更新</label>
    </p>
    <?php
    }
}
