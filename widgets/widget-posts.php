<?php

class widget_yi_posts extends WP_Widget {
    function __construct(){
        $widget_ops = array(
            'classname' => 'widget_yi_posts',
            'description' => __('图文展示(最新文章+热门文章+随机文章)', 'yii'),
            'customize_selective_refresh' => true,
        );
        parent::__construct('widget_yi_posts', 'YI-推荐阅读', $widget_ops);
    }

    function widget( $args, $instance ) {
        extract( $args );// 导入数组，以key作为变量名
        $title   = apply_filters('widget_name', $instance['title']);
        $limit   = isset($instance['limit']) ? $instance['limit'] : 6;
        $cate     = isset($instance['cate']) ? $instance['cate'] : '';
        $orderby = isset($instance['orderby']) ? $instance['orderby'] : 'comment_count';
        $img     = isset($instance['img']) ? $instance['img'] : '';
        $comn     = isset($instance['comn']) ? $instance['comn'] : '';

        $style='';
        if( !$img ) {
            $style = ' class="nopic"';
        }
        $recommend = $before_widget;
        $recommend .= $before_title . $title . $after_title; 
        $recommend .= '<ul' . $style . '>';
        $recommend .= yi_recommend_posts( $orderby, $limit, $cate, $img, $comn );
        $recommend .= '</ul>';
        $recommend .= $after_widget;
        echo $recommend;
    }

    function form( $instance ) {
        $defaults = array( 
            'title' => '推荐阅读', 
            'limit' => 6, 
            'cate' => '', 
            'orderby' => 'comment_count', 
            'img' => '',
            'comn' => ''
        );
        $instance = wp_parse_args( (array) $instance, $defaults );
    ?>
    <p>
        <label for="<?php echo $this->get_field_id('title'); ?>">标题：</label>
        <input id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $instance['title']; ?>" />
    </p>
    <p>
        <label for="<?php echo $this->get_field_id('orderby'); ?>">排序：</label>
        <select id="<?php echo $this->get_field_id('orderby'); ?>" name="<?php echo $this->get_field_name('orderby'); ?>" style="width:100%;">
            <option value="comment_count" <?php selected('comment_count', $instance['orderby']); ?>>评论数</option>
            <option value="post_views_count" <?php selected('post_views_count', $instance['orderby']); ?>>浏览量</option>
            <option value="date" <?php selected('date', $instance['orderby']); ?>>发布时间</option>
            <option value="rand" <?php selected('rand', $instance['orderby']); ?>>随机</option>
        </select>
    </p>
    <p>
        <label for="<?php echo $this->get_field_id('cate'); ?>">
            分类限制：
            <a style="font-weight:bold;color:#f60;text-decoration:none;" href="javascript:;" title="格式：1,2 &nbsp;表限制ID为1,2分类的文章&#13;格式：-1,-2 &nbsp;表排除分类ID为1,2的文章&#13;也可直接写1或者-1；注意逗号须是英文的">？</a>
        </label>
        <input id="<?php echo $this->get_field_id('cate'); ?>" name="<?php echo $this->get_field_name('cate'); ?>" type="text" value="<?php echo $instance['cate']; ?>" size="24" />
    </p>
    <p>
        <label for="<?php echo $this->get_field_id('limit'); ?>">显示数目：</label>
        <input id="<?php echo $this->get_field_id('limit'); ?>" name="<?php echo $this->get_field_name('limit'); ?>" type="number" value="<?php echo $instance['limit']; ?>" size="24" />
    </p>
    <p>
        <input class="checkbox" type="checkbox" <?php checked( $instance['img'], 'on' ); ?> id="<?php echo $this->get_field_id('img'); ?>" name="<?php echo $this->get_field_name('img'); ?>">
        <label for="<?php echo $this->get_field_id('img'); ?>">显示图片</label>
    </p>
    <p>
        <input class="checkbox" type="checkbox" <?php checked( $instance['comn'], 'on' ); ?> id="<?php echo $this->get_field_id('comn'); ?>" name="<?php echo $this->get_field_name('comn'); ?>">
        <label for="<?php echo $this->get_field_id('comn'); ?>">显示评论数</label>
    </p>
    <?php
    }
}

/**
 * 推荐阅读文章列表
 * @param  [type] $orderby 排序方式
 * @param  [type] $limit   显示数量
 * @param  [type] $cate    限制分类-ID
 * @param  [type] $img     显示特色图
 * @param  [type] $comn    显示评论数
 */
function yi_recommend_posts($orderby, $limit, $cate, $img, $comn) {
    $args = array(
        'cate'             => $cate,
        'order'            => 'DESC',
        'showposts'        => $limit,
        'ignore_sticky_posts' => 1,
    );

    // 是否按浏览量排序
    if( $orderby !== 'post_views_count' ){
        $args['orderby'] = $orderby;
    }else{
        $args['orderby'] = 'meta_value_num';//将meta_value作为一个数值来对待
        $args['meta_query'] = array(
            array(
                'key'   => 'post_views_count',//自定义字段-阅读量
                'order' => 'DESC',// 倒序
            )
        );
    }
    $br = '<br/>';
    if(!$img){
        $br = '';
    }

    query_posts($args);// 取得文章列表

    $recommend_posts = "";
    while (have_posts()) : the_post();
        $recommend_posts .= '<article id="post-'.get_the_ID().'" class="post">';
            $recommend_posts .= '<div class="entry">';
                $recommend_posts .= '<div class="article-inner">';
                    if( $img ){
                        if(has_post_thumbnail()){
                            $recommend_posts .= '<figure class="thumb">';
                                if(has_post_thumbnail()){
                                    $recommend_posts .= '<a class="entry-img" href="'.get_the_permalink().'">';
                                    $recommend_posts .= get_the_post_thumbnail();//默认尺寸的特色图
                                    //the_post_thumbnail( 'mobile-post-thumbnail' );//插入自定义名称和尺寸的特色图，一般使用小图片为手机端提供支持
                                    $recommend_posts .= '</a>';
                                }
                            $recommend_posts .= '</figure>';
                        }
                    }
                    $recommend_posts .= '<header class="entry-header">';
                        $recommend_posts .= '<span class="entry-title"><a href="'.get_the_permalink().'" title="'.get_the_title().'"'.yi_target_blank().'>'.get_the_title().'</a></span>';
                    $recommend_posts .= '</header>';
                    $recommend_posts .= '<div class="entry-content">';
                        $recommend_posts .= '<span class="'.(has_post_thumbnail() ? "entry-meta" : "entry-meta-no").'">';
                            $recommend_posts .= '<span class="date"><i class="yi yi-date"></i>'.get_the_time('Y-m-d').'</span>'.(has_post_thumbnail() ? $br : "");
                            $recommend_posts .= '<span class="comment"><a href="'.get_the_permalink().'#comment"><i class="yi yi-comment"></i>'.get_comments_number("0", "1", "%").'</a></span>';//评论数统计
                            $recommend_posts .= '<span class="view"><i class="yi yi-eye"></i>'.yi_get_post_views(get_the_ID()).'</span>';// 浏览量
                        $recommend_posts .= '</span>';
                        $recommend_posts .= '<div class="clear"></div>';
                    $recommend_posts .= '</div>';
                $recommend_posts .= '</div>';
            $recommend_posts .= '</div>';
        $recommend_posts .= '</article>';
    endwhile;
    wp_reset_query();
    return $recommend_posts;
}