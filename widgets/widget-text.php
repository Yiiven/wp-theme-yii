<?php

class widget_yi_text extends WP_Widget {
    function __construct(){
        $widget_ops = array(
            'classname' => 'widget_yi_text',
            'description' => __('显示一个文本特别推荐', 'yii'),
            'customize_selective_refresh' => true,
        );
        parent::__construct('widget_yi_text', 'YI-特别推荐(文本)', $widget_ops);
    }

    function widget( $args, $instance ) {
        extract( $args );

        $title   = apply_filters('widget_name', $instance['title']);
        $tag     = isset($instance['tag']) ? $instance['tag'] : '';
        $content = isset($instance['content']) ? $instance['content'] : '';
        $link    = isset($instance['link']) ? $instance['link'] : '';
        $style   = isset($instance['style']) ? $instance['style'] : '';
        $blank   = isset($instance['blank']) ? $instance['blank'] : '';

        $lank = '';
        if( $blank ) $lank = ' target="_blank"';

        $text = "";
        $text .= $before_widget;
        $text .= '<a class="'.$style.'" href="'.$link.'"'.$lank.'>';
        $text .= '<strong>'.$tag.'</strong>';
        $text .= '<h2>'.$title.'</h2>';
        $text .= '<p>'.$content.'</p>';
        $text .= '</a>';
        $text .= $after_widget;
        echo $text;
    }

    function form($instance){
        $defaults = array( 
            'title' => 'yii主题', 
            'tag' => '吐血推荐', 
            'blank' => 'on', 
            'content' => 'yii 扁平化、简洁风、多功能配置，优秀的电脑、平板、手机支持，响应式布局，不同设备不同展示效果...', 
            'link' => 'https://www.yiven.vip', 
            'style' => 'color_orange',
        );
        $instance = wp_parse_args((array)$instance, $defaults);
    ?>
    <p>
        <label for="<?php echo $this->get_field_id('title'); ?>">名称：</label>
        <input id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $instance['title']; ?>" class="widefat" />
    </p>
    <p>
        <label for="<?php echo $this->get_field_id('content'); ?>">描述：</label>
        <textarea id="<?php echo $this->get_field_id('content'); ?>" name="<?php echo $this->get_field_name('content'); ?>" class="widefat" rows="3"><?php echo $instance['content']; ?></textarea>
    </p>
    <p>
        <label for="<?php echo $this->get_field_id('tag'); ?>">标签：</label>
        <input id="<?php echo $this->get_field_id('tag'); ?>" name="<?php echo $this->get_field_name('tag'); ?>" type="text" value="<?php echo $instance['tag']; ?>" class="widefat" />
    </p>
    <p>
        <label for="<?php echo $this->get_field_id('link'); ?>">链接：</label>
        <input id="<?php echo $this->get_field_id('link'); ?>" name="<?php echo $this->get_field_name('link'); ?>" type="url" value="<?php echo $instance['link']; ?>" size="24" />
    </p>
    <p>
        <label for="<?php echo $this->get_field_id('style'); ?>">样式：</label>
        <select id="<?php echo $this->get_field_id('style'); ?>" name="<?php echo $this->get_field_name('style'); ?>" style="width:100%;">
            <option value="color_blue" <?php selected('color_blue', $instance['style']); ?>>蓝色</option>
            <option value="color_orange" <?php selected('color_orange', $instance['style']); ?>>橘红色</option>
            <option value="color_green" <?php selected('color_green', $instance['style']); ?>>绿色</option>
            <option value="color_purple" <?php selected('color_purple', $instance['style']); ?>>紫色</option>
            <option value="color_cyan" <?php selected('color_cyan', $instance['style']); ?>>青色</option>
        </select>
    </p>
    <p>
        <input id="<?php echo $this->get_field_id('blank'); ?>" name="<?php echo $this->get_field_name('blank'); ?>" class="checkbox" type="checkbox" <?php checked( $instance['blank'], 'on' ); ?>>
        <label for="<?php echo $this->get_field_id('blank'); ?>">新标签页/窗口打开链接</label>
    </p>
    <?php
    }
}