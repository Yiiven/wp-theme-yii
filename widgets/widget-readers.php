<?php

class widget_yi_readers extends WP_Widget {
    function __construct(){
        $widget_ops = array(
            'classname' => 'widget_yi_readers',
            'description' => __('显示近期评论频繁的网友头像等', 'yii'),
            'customize_selective_refresh' => true,
        );
        parent::__construct('widget_yi_readers', 'YI-活跃读者', $widget_ops);
    }

    function widget( $args, $instance ) {
        extract( $args );

        $title   = apply_filters('widget_name', $instance['title']);
        $outer   = isset($instance['outer']) ? $instance['outer'] : 1;
        $timer   = isset($instance['timer']) ? $instance['timer'] : 500;
        $limit   = isset($instance['limit']) ? $instance['limit'] : 32;
        $addlink = isset($instance['addlink']) ? $instance['addlink'] : '';

        $reader = "";
        $reader .= $before_widget;
        $reader .= $before_title.$title.$after_title; 
        $reader .= '<ul>';
        $reader .= yi_readers( $outer, $timer, $limit, $addlink );
        $reader .= '</ul>';
        $reader .= $after_widget;
        echo $reader;
    }

    function form($instance) {
        $defaults = array( 
            'title' => '活跃读者', 
            'limit' => 32, 
            'outer' => 1,
            'timer' => 500
        );
        $instance = wp_parse_args( (array) $instance, $defaults );
    ?>
    <p>
        <label for="<?php echo $this->get_field_id('title'); ?>">标题：</label>
        <input id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $instance['title']; ?>" class="widefat" />
    </p>
    <p>
        <label for="<?php echo $this->get_field_id('limit'); ?>">显示数目：</label>
        <input id="<?php echo $this->get_field_id('limit'); ?>" name="<?php echo $this->get_field_name('limit'); ?>" type="number" value="<?php echo $instance['limit']; ?>" class="widefat" />
    </p>
    <p>
        <label for="<?php echo $this->get_field_id('outer'); ?>">排除某人：</label>
        <input id="<?php echo $this->get_field_id('outer'); ?>" name="<?php echo $this->get_field_name('outer'); ?>" type="text" value="<?php echo $instance['outer']; ?>" class="widefat" />
    </p>
    <p>
        <label for="<?php echo $this->get_field_id('timer'); ?>">几天内：</label>
        <input id="<?php echo $this->get_field_id('timer'); ?>" name="<?php echo $this->get_field_name('timer'); ?>" type="number" value="<?php echo $instance['timer']; ?>" class="widefat" />
    </p>
    <p>
        <input id="<?php echo $this->get_field_id('addlink'); ?>" name="<?php echo $this->get_field_name('addlink'); ?>" class="checkbox" type="checkbox" <?php checked( $instance['addlink'], 'on' ); ?>>
        <label for="<?php echo $this->get_field_id('addlink'); ?>">加链接</label>
    </p>
    <?php
    }
}

/* 
 * 读者墙
 * yi_readers( $outer='name', $timer='3', $limit='14' );
 * $outer 不显示某人
 * $timer 几个月时间内
 * $limit 显示条数
*/
function yi_readers($outer,$timer,$limit,$addlink){
    global $wpdb;
    $comments = $wpdb->get_results("SELECT count(comment_author) AS cnt, user_id, comment_author, comment_author_url, comment_author_email FROM $wpdb->comments WHERE comment_date > date_sub( now(), interval $timer day ) AND user_id!='1' AND comment_author!=$outer AND comment_approved='1' AND comment_type='' GROUP BY comment_author ORDER BY cnt DESC LIMIT $limit");

    $html = '';
    foreach ($comments as $comment) {
        $c_url = $comment->comment_author_url;
        if ($c_url == '') $c_url = 'javascript:;';

        if($addlink == 'on'){
            $c_urllink = ' href="'. $c_url . '"';
        }else{
            $c_urllink = '';
        }
        $html .= '<li><a title="['.$comment->comment_author.'] 近期点评'. $comment->cnt .'次" target="_blank"'.$c_urllink.'>'.yi_get_avatar($user_id=$comment->user_id, $user_email=$comment->comment_author_email).'</a></li>';
    }
    echo $html;
}