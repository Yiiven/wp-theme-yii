<?php
// Tab
class widget_yi_tabs extends WP_Widget {
    function __construct() {
        $widget_ops = array(
            'classname' => 'widget_yi_tabs',
            'description' => __('最新文章、热评文章、热门文章、最近留言', 'yii'),
            'customize_selective_refresh' => true,
        );
        parent::__construct('widget_yi_tabs', 'YI-Tab组合小工具', $widget_ops);
    }

    public function defaults() {
        return array(
            'title'       => '',
            'tabs_category'   => 1,
            'tabs_date'     => 1,
            // 最新文章
            'recent_enable'   => 1,
            'recent_thumbs'   => 1,
            'recent_cat_id'   => '0',
            'recent_num'    => '5',
            // 热评文章
            'popular_enable'  => 1,
            'popular_thumbs'  => 1,
            'popular_cat_id'  => '0',
            'popular_time'    => '0',
            'popular_num'     => '5',
            // 最近留言
            'comments_enable'   => 1,
            'comments_avatars'  => 1,
            'comments_num'    => '5',
            // 热门文章
            'viewe_enable'     => 1,
            'viewe_thumbs'  => 1,
            'viewe_number'  => '5',
            'viewe_days'    => '90',
        );
    }


/*  Create tabs-nav
/* ------------------------------------ */
    private function create_tabs($tabs,$count) {
        // Borrowed from Jermaine Maree, thanks mate!
        $titles = array(
            'recent'    => __('最新文章', 'yii'),
            'popular'   => __('热评文章', 'yii'),
            'viewe'     => __('热门文章', 'yii'),
            'comments'  => __('最近留言', 'yii')
        );
        $icons = array(
            'recent'   => 'yi yi-stack',
            'popular'  => 'yi yi-diamond',
            'viewe'     => 'yi yi-eye',
            'comments' => 'yi yi-speechbubble'
        );
        $output = sprintf('<div class="tabs-nav group has-%s-tabs">', $count);
        foreach ($tabs as $tabname){
            $output .= sprintf(
                '<span class="tab-title tab-%1$s"><a id="%2$s" title="%3$s" href="javascript:;"><i class="%4$s"></i></a></span>',
                $tabname,//1%
                $tabname.'-tab',//2%
                $titles[$tabname],//3%
                $icons[$tabname]//4%
            );
        }
        $output .= '</div>';
        return $output;
    }

    public function widget($args, $instance){
        extract($args);
        $defaults = $this -> defaults();
        $instance = wp_parse_args((array)$instance, $defaults);
        $title = apply_filters('widget_name',$instance['title']);
        $title = empty($title) ? '' : $title;

        $output = $before_widget;
        if (!empty($title))
            $output .= $before_title.$title.$after_title;
        //ob_start();

        // 设置tabs顺序并输出tabs
        $tabs = array();
        $count = 0;
        $order = array(
            'recent'    => 1,
            'popular'   => 2,
            'viewe'     => 3,
            'comments'  => 4
        );
        asort($order);
        foreach ($order as $key => $value){
            if ($instance[$key.'_enable']){
                $tabs[] = $key;
                $count++;
            }
        }
        $output .= '<div id="widget_yi_tabs_content" class="widget_yi_tabs_content">';
        if ($tabs && ($count > 1)){
            $output .= $this->create_tabs($tabs,$count);
        }

        $output .= '<div class="tabs-container">';
        //1
        if($instance['recent_enable']) { // 显示最新文章?
            $style='';
            $br = '<br/>';
            if(!$instance['recent_thumbs']) {
                $style = ' class="nopic"';
                $br = "";
            }
            $recent=new WP_Query();
            $recent->query('showposts='.$instance["recent_num"].'&cat='.$instance["recent_cat_id"].'&ignore_sticky_posts=1');
            $output .= '<div id="recent-container" class="tab-content actived">';
            $output .= '<ul id="tab-recent-'.$this -> number.'" class="tab group '.($instance['recent_thumbs'] ? 'thumbs-enabled' : '').'" style="display:block;">';
            //$output .= '<h4>'._e( '最新文章', 'begin' ).'</h4>';
            while ($recent->have_posts()):
                $recent->the_post();
                $output .= '<article id="post-'.get_the_ID().'" class="post">';
                $output .= '<div class="entry">';
                $output .= '<div class="article-inner">';
                if ($instance['recent_thumbs']){
                    if(has_post_thumbnail()){
                        $output .= '<figure class="thumb">';
                        if(has_post_thumbnail()){
                            $output .= '<a class="entry-img" href="'.get_the_permalink().'">';
                            $output .= get_the_post_thumbnail();//默认尺寸的特色图
                            $output .= '</a>';
                        }
                        $output .= '</figure>';
                    }
                }
                $output .= '<header class="entry-header">';
                $output .= '<span class="entry-title"><a href="'.get_the_permalink().'" title="'.get_the_title().'"'.yi_target_blank().'>'.get_the_title().'</a></span>';
                $output .= '</header>';
                $output .= '<div class="entry-content">';
                $output .= '<span class="'.(has_post_thumbnail() ? "entry-meta" : "entry-meta-no").'">';
                $output .= '<span class="date"><i class="yi yi-date"></i>'.get_the_time('Y-m-d').'</span>'.(has_post_thumbnail() ? $br : "");
                $output .= '<span class="comment"><a href="'.get_the_permalink().'#comment"><i class="yi yi-comment"></i>'.get_comments_number("0", "1", "%").'</a></span>';//评论数统计
                $output .= '<span class="view"><i class="yi yi-eye"></i>'.yi_get_post_views(get_the_ID()).'</span>';// 浏览量
                $output .= '</span>';
                $output .= '<div class="clear"></div>';
                $output .= '</div>';
                $output .= '</div>';
                $output .= '</div>';
                $output .= '</article>';
            endwhile;
            wp_reset_postdata();
            $output .= '</ul>';//tab end
            $output .= '</div>';
        }
        //2
        if($instance['popular_enable']) { // 显示热评文章?
            $style='';
            $br = '<br/>';
            if(!$instance['popular_thumbs']) {
                $style = ' class="nopic"';
                $br = "";
            }
            $popular = new WP_Query(array(
                'post_type'             => array('post'),
                'showposts'             => $instance['popular_num'],
                'cat'                   => $instance['popular_cat_id'],
                'ignore_sticky_posts'   => true,
                'orderby'               => 'comment_count',
                'order'                 => 'desc',
                'date_query' => array(
                    array(
                        'after' => $instance['popular_time'],
                    ),
                ),
            ));
            $output .= '<div id="popular-container" class="tab-content">';//hot
            $output .= '<ul id="tab-popular-'.$this -> number.'" class="tab group '.($instance['popular_thumbs'] ? 'thumbs-enabled' : '').'">';
            //$output .= '<h4>'._e( '热评文章', 'begin' ).'</h4>';
            while ($popular->have_posts()):
                $popular->the_post();
                $output .= '<article id="post-'.get_the_ID().'" class="post">';
                $output .= '<div class="entry">';
                $output .= '<div class="article-inner">';
                if ($instance['popular_thumbs']){
                    if(has_post_thumbnail()){
                        $output .= '<figure class="thumb">';
                        if(has_post_thumbnail()){
                            $output .= '<a class="entry-img" href="'.get_the_permalink().'">';
                            $output .= get_the_post_thumbnail();//默认尺寸的特色图
                            $output .= '</a>';
                        }
                        $output .= '</figure>';
                    }
                }
                $output .= '<header class="entry-header">';
                $output .= '<span class="entry-title"><a href="'.get_the_permalink().'" title="'.get_the_title().'"'.yi_target_blank().'>'.get_the_title().'</a></span>';
                $output .= '</header>';
                $output .= '<div class="entry-content">';
                $output .= '<span class="'.(has_post_thumbnail() ? "entry-meta" : "entry-meta-no").'">';
                $output .= '<span class="date"><i class="yi yi-date"></i>'.get_the_time('Y-m-d').'</span>'.(has_post_thumbnail() ? $br : "");
                $output .= '<span class="comment"><a href="'.get_the_permalink().'#comment"><i class="yi yi-comment"></i>'.get_comments_number("0", "1", "%").'</a></span>';//评论数统计
                $output .= '<span class="view"><i class="yi yi-eye"></i>'.yi_get_post_views(get_the_ID()).'</span>';// 浏览量
                $output .= '</span>';
                $output .= '<div class="clear"></div>';
                $output .= '</div>';
                $output .= '</div>';
                $output .= '</div>';
                $output .= '</article>';
            endwhile;
            wp_reset_postdata();
            $output .= '</ul>';//tab end
            $output .= '</div>';
        }
        //3
        if($instance['viewe_enable']) { // 显示热门文章-依赖WP-PostViews?
            $output .= '<div id="viewe-container" class="tab-content">';//review
            $output .= '<ul id="tab-viewe-'.$this -> number.'" class="tab group">';
            //$output .= '<h4>'._e( '热门文章', 'begin' ).'</h4>';
            if($instance['viewe_thumbs']) {
                if (function_exists('get_most_viewed')) {
                    yi_get_timespan_most_posts_img('post',$instance["viewe_number"],$instance["viewe_days"], true, true);
                }else{
                    $output .= '<li><a href="https://wordpress.org/plugins/wp-postviews/" rel="external nofollow" target="_blank">需要安装WP-PostViews插件</a></li>';
                }
            }else{
                if (function_exists('get_most_viewed')) {
                    yi_get_timespan_most_posts('post',$instance["viewe_number"],$instance["viewe_days"], true, true);
                }else{
                    $output .= '<li><a href="https://wordpress.org/plugins/wp-postviews/" rel="external nofollow" target="_blank">需要安装WP-PostViews插件</a></li>';
                }
            }
            wp_reset_query();
            $output .= '</ul>';//tab end
            $output .= '</div>';
        }
        //4
        if($instance['comments_enable']) { // 显示最近留言?
            $comments = get_comments(array('number'=>$instance["comments_num"],'status'=>'approve','post_status'=>'publish'));
            $output .= '<div id="comments-container" class="tab-content">';
            $output .= '<ul>';
            //$output .= '<h4>'._e( '最近留言', 'begin' ).'</h4>';
            $show_comments = $instance["comments_num"];
            $my_email = get_bloginfo ('admin_email');
            $i = 1;
            $comments = get_comments('number=200&status=approve&type=comment');
            foreach ($comments as $my_comment) {
                if ($my_comment->comment_author_email != $my_email) {
                    $comment_content = yi_replace_enlazy(convert_smilies(strip_tags($my_comment->comment_content,'<img><font><strong>')));
                    $output .= '<li>';
                    $output .= '<a href="'.get_permalink($my_comment->comment_post_ID).'#anchor-comment-'.$my_comment->comment_ID.'" title="'.get_the_title($my_comment->comment_post_ID).'" rel="external nofollow">';
                    if($instance['comments_avatars']) {
                        //$output .= get_avatar($my_comment->comment_author_email,64, '', $my_comment->comment_author);
                        $output .='<figure class="user-thumb">'.yi_get_avatar($user_id=$my_comment->user_id, $user_email=$my_comment->comment_author_email).'</figure>';
                    }
                    //$output .= '<span class="comment_author"><strong>'.$my_comment->comment_author.'</strong></span>';
                    $output .= '<p class="comment-meta"><strong>'.$my_comment->comment_author.'</strong>&nbsp;'.yi_get_time_ago($my_comment->comment_date_gmt).'：</p>';
                    $output .= '<p class="comment-content">'.htmlspecialchars_decode($comment_content).'</p>';
                    $output .= '</a>';
                    $output .= '</li>';
                }
                if ($i == $show_comments) break;
                $i++;
            }
            $output .= '</ul>';
            $output .= '</div>';
        }
        $output .= '</div>';
        $output .= '</div>';
        //$output .= ob_get_clean();
        $output .= $after_widget;
        echo $output;
    }

/*  Widget update
/* ------------------------------------ */
    public function update($new_instance,$old_instance) {
        $instance = $old_instance;
        $instance['title'] = strip_tags($new_instance['title']);
        $instance['tabs_category'] = (isset($new_instance['tabs_category']) && $new_instance['tabs_category']) ? 1 : 0;
        $instance['tabs_date'] = (isset($new_instance['tabs_date']) && $new_instance['tabs_date']) ? 1 : 0;
    // 最新文章
        $instance['recent_enable'] = (isset($new_instance['recent_enable']) && $new_instance['recent_enable']) ? 1 : 0;
        $instance['recent_thumbs'] = (isset($new_instance['recent_thumbs']) && $new_instance['recent_thumbs']) ? 1 : 0;
        $instance['recent_cat_id'] = strip_tags($new_instance['recent_cat_id']);
        $instance['recent_num'] = strip_tags($new_instance['recent_num']);
    // 热评文章
        $instance['popular_enable'] = (isset($new_instance['popular_enable']) && $new_instance['popular_enable']) ? 1 : 0;
        $instance['popular_thumbs'] = (isset($new_instance['popular_thumbs']) && $new_instance['popular_thumbs']) ? 1 : 0;
        $instance['popular_cat_id'] = strip_tags($new_instance['popular_cat_id']);
        $instance['popular_time'] = strip_tags($new_instance['popular_time']);
        $instance['popular_num'] = strip_tags($new_instance['popular_num']);
    // 最新评论
        $instance['comments_enable'] = (isset($new_instance['comments_enable']) && $new_instance['comments_enable']) ? 1 : 0;
        $instance['comments_avatars'] = (isset($new_instance['comments_avatars']) && $new_instance['comments_avatars']) ? 1 : 0;
        $instance['comments_num'] = strip_tags($new_instance['comments_num']);
    // 热门文章
        $instance['viewe_enable'] = (isset($new_instance['viewe_enable']) && $new_instance['viewe_enable']) ? 1 : 0;
        $instance['viewe_thumbs'] = (isset($new_instance['viewe_thumbs']) && $new_instance['viewe_thumbs']) ? 1 : 0;
        $instance['viewe_number'] = strip_tags($new_instance['viewe_number']);
        $instance['viewe_days'] = strip_tags($new_instance['viewe_days']);
        return $instance;
    }

/*  Widget form
/* ------------------------------------ */
    public function form($instance) {
        // Default widget settings
        $defaults = $this -> defaults();
        $instance = wp_parse_args( (array) $instance, $defaults );
?>

    <style>
    .widget .widget-inside .options-tabs .postform { width: 100%; }
    .widget .widget-inside .options-tabs p { margin: 3px 0; }
    .widget .widget-inside .options-tabs hr { margin: 20px 0 10px; }
    .widget .widget-inside .options-tabs h4 { margin-bottom: 10px; }
    </style>

    <div class="options-tabs">
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id('title') ); ?>">标题：</label>
            <input class="widefat" id="<?php echo esc_attr( $this->get_field_id('title') ); ?>" name="<?php echo esc_attr( $this->get_field_name('title') ); ?>" type="text" value="<?php echo esc_attr($instance["title"]); ?>" />
        </p>

        <h4>最新文章</h4>
        <p>
            <input type="checkbox" class="checkbox" id="<?php echo esc_attr( $this->get_field_id('recent_enable') ); ?>" name="<?php echo esc_attr( $this->get_field_name('recent_enable') ); ?>" <?php checked( (bool) $instance["recent_enable"], true ); ?>>
            <label for="<?php echo esc_attr( $this->get_field_id('recent_enable') ); ?>">显示最新文章</label>
        </p>
        <p>
            <input type="checkbox" class="checkbox" id="<?php echo esc_attr( $this->get_field_id('recent_thumbs') ); ?>" name="<?php echo esc_attr( $this->get_field_name('recent_thumbs') ); ?>" <?php checked( (bool) $instance["recent_thumbs"], true ); ?>>
            <label for="<?php echo esc_attr( $this->get_field_id('recent_thumbs') ); ?>">显示缩略图</label>
        </p>
        <p>
            <label style="width: 55%; display: inline-block;" for="<?php echo esc_attr( $this->get_field_id("recent_num") ); ?>">显示篇数</label>
            <input style="width:20%;" id="<?php echo esc_attr( $this->get_field_id("recent_num") ); ?>" name="<?php echo esc_attr( $this->get_field_name("recent_num") ); ?>" type="text" value="<?php echo absint($instance["recent_num"]); ?>" size='3' />
        </p>
        <p>
            <label style="width: 100%; display: inline-block;" for="<?php echo esc_attr( $this->get_field_id("recent_cat_id") ); ?>">选择分类：</label>
            <?php wp_dropdown_categories( array( 'name' => $this->get_field_name("recent_cat_id"), 'selected' => $instance["recent_cat_id"], 'show_option_all' => '全部分类', 'show_count' => true ) ); ?>
        </p>

        <hr>
        <h4>热评文章</h4>
        <p>
            <input type="checkbox" class="checkbox" id="<?php echo esc_attr( $this->get_field_id('popular_enable') ); ?>" name="<?php echo esc_attr( $this->get_field_name('popular_enable') ); ?>" <?php checked( (bool) $instance["popular_enable"], true ); ?>>
            <label for="<?php echo esc_attr( $this->get_field_id('popular_enable') ); ?>">显示热评文章</label>
        </p>
        <p>
            <input type="checkbox" class="checkbox" id="<?php echo esc_attr( $this->get_field_id('popular_thumbs') ); ?>" name="<?php echo esc_attr( $this->get_field_name('popular_thumbs') ); ?>" <?php checked( (bool) $instance["popular_thumbs"], true ); ?>>
            <label for="<?php echo esc_attr( $this->get_field_id('popular_thumbs') ); ?>">显示缩略图</label>
        </p>
        <p>
            <label style="width: 55%; display: inline-block;" for="<?php echo esc_attr( $this->get_field_id("popular_num") ); ?>">显示篇数</label>
            <input style="width:20%;" id="<?php echo esc_attr( $this->get_field_id("popular_num") ); ?>" name="<?php echo esc_attr( $this->get_field_name("popular_num") ); ?>" type="text" value="<?php echo absint($instance["popular_num"]); ?>" size='3' />
        </p>
        <p>
            <label style="width: 100%; display: inline-block;" for="<?php echo esc_attr( $this->get_field_id("popular_cat_id") ); ?>">选择分类：</label>
            <?php wp_dropdown_categories( array( 'name' => $this->get_field_name("popular_cat_id"), 'selected' => $instance["popular_cat_id"], 'show_option_all' => '全部分类', 'show_count' => true ) ); ?>
        </p>
        <p style="padding-top: 0.3em;">
            <label style="width: 100%; display: inline-block;" for="<?php echo esc_attr( $this->get_field_id("popular_time") ); ?>">选择时间段：</label>
            <select style="width: 100%;" id="<?php echo esc_attr( $this->get_field_id("popular_time") ); ?>" name="<?php echo esc_attr( $this->get_field_name("popular_time") ); ?>">
                <option value="0"<?php selected( $instance["popular_time"], "0" ); ?>>全部</option>
                <option value="1 year ago"<?php selected( $instance["popular_time"], "1 year ago" ); ?>>一年内</option>
                <option value="1 month ago"<?php selected( $instance["popular_time"], "1 month ago" ); ?>>一月内</option>
                <option value="1 week ago"<?php selected( $instance["popular_time"], "1 week ago" ); ?>>一周内</option>
                <option value="1 day ago"<?php selected( $instance["popular_time"], "1 day ago" ); ?>>24小时内</option>
            </select>
        </p>

        <hr>
        <h4>热门文章</h4>
        <p>
            <input type="checkbox" class="checkbox" id="<?php echo esc_attr( $this->get_field_id('viewe_enable') ); ?>" name="<?php echo esc_attr( $this->get_field_name('viewe_enable') ); ?>" <?php checked( (bool) $instance["viewe_enable"], true ); ?>>
            <label for="<?php echo esc_attr( $this->get_field_id('viewe_enable') ); ?>">显示热门文章</label>
        </p>

        <p>
            <input type="checkbox" class="checkbox" id="<?php echo esc_attr( $this->get_field_id('viewe_thumbs') ); ?>" name="<?php echo esc_attr( $this->get_field_name('viewe_thumbs') ); ?>" <?php checked( (bool) $instance["viewe_thumbs"], true ); ?>>
            <label for="<?php echo esc_attr( $this->get_field_id('viewe_thumbs') ); ?>">显示缩略图</label>
        </p>

        <p>
            <label style="width: 55%; display: inline-block;" for="<?php echo esc_attr( $this->get_field_id("viewe_number") ); ?>">显示篇数：</label>
            <input style="width:20%;" id="<?php echo esc_attr( $this->get_field_id("viewe_number") ); ?>" name="<?php echo esc_attr( $this->get_field_name("viewe_number") ); ?>" type="text" value="<?php echo absint($instance["viewe_number"]); ?>" size='3' />
        </p>
        <p>
            <label style="width: 55%; display: inline-block;" for="<?php echo esc_attr( $this->get_field_id("viewe_days") ); ?>">时间限定（天）：</label>
            <input style="width:20%;" id="<?php echo esc_attr( $this->get_field_id("viewe_days") ); ?>" name="<?php echo esc_attr( $this->get_field_name("viewe_days") ); ?>" type="text" value="<?php echo absint($instance["viewe_days"]); ?>" size='3' />
        </p>

        <hr>
        <h4>最新留言</h4>
        <p>
            <input type="checkbox" class="checkbox" id="<?php echo esc_attr( $this->get_field_id('comments_enable') ); ?>" name="<?php echo esc_attr( $this->get_field_name('comments_enable') ); ?>" <?php checked( (bool) $instance["comments_enable"], true ); ?>>
            <label for="<?php echo esc_attr( $this->get_field_id('comments_enable') ); ?>">显示最新留言</label>
        </p>
        <p>
            <input type="checkbox" class="checkbox" id="<?php echo esc_attr( $this->get_field_id('comments_avatars') ); ?>" name="<?php echo esc_attr( $this->get_field_name('comments_avatars') ); ?>" <?php checked( (bool) $instance["comments_avatars"], true ); ?>>
            <label for="<?php echo esc_attr( $this->get_field_id('comments_avatars') ); ?>">显示头像</label>
        </p>
        <p>
            <label style="width: 55%; display: inline-block;" for="<?php echo esc_attr( $this->get_field_id("comments_num") ); ?>">显示数量：</label>
            <input style="width:20%;" id="<?php echo esc_attr( $this->get_field_id("comments_num") ); ?>" name="<?php echo esc_attr( $this->get_field_name("comments_num") ); ?>" type="text" value="<?php echo absint($instance["comments_num"]); ?>" size='3' />
        </p>
    </div>
<?php
}
}