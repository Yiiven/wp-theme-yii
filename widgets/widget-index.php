<?php
add_action('widgets_init', 'yi_unregister_widget');

// 注销WP自带的小工具
function yi_unregister_widget(){
    unregister_widget('WP_Widget_Search'); // 搜索小工具
    unregister_widget('WP_Widget_Recent_Comments'); // 近期评论小工具
    unregister_widget('WP_Widget_Tag_Cloud'); // 标签云小工具
    unregister_widget('WP_Widget_Links'); // 链接小工具
}

$widgets = array(
    'imgad',//图片广告
    'posts',//推荐阅读
    'comments',//最新评论
    'total',//网站统计
    'sticky',//置顶推荐
    'tags',//标签云
    'links',//友情链接
    'feed',//站点信息-订阅
    'text',//文本
    'hot_comment',//热评文章
    'tabs',//tabs
);

foreach ($widgets as $widget) {
    include 'widget-'.$widget.'.php';
}

add_action('widgets_init', 'yi_widget_loader');
function yi_widget_loader(){
    global $widgets;
    foreach ($widgets as $widget) {
        register_widget('widget_yi_'.$widget);
    }
}