<?php 
class widget_yi_hot_comment extends WP_Widget {
    public function __construct() {
        $widget_ops = array(
            'classname' => 'widget_yi_hot_comment',
            'description' => __('调用评论最多的文章', 'yii'),
            'customize_selective_refresh' => true,
        );
        parent::__construct('widget_yi_hot_comment', 'YI-热评文章', $widget_ops);
    }

    public function defaults() {
        return array(
            'show_thumbs'   => 1,
        );
    }

    function widget( $args, $instance ) {
        extract( $args );
        $defaults = $this -> defaults();
        $instance = wp_parse_args((array)$instance, $defaults);
        $title = apply_filters('widget_name', $instance['title']);
        $number = strip_tags($instance['number']) ? absint($instance['number']) : 5;
        $days = strip_tags($instance['days']) ? absint($instance['days']) : 90;

        $style='';
        $br = '<br/>';
        if(!$instance['show_thumbs']) {
            $style = ' class="nopic"';
            $br = "";
        }
        $hot_comment = $before_widget;
        $hot_comment .= $before_title.$title.$after_title;
        $hot_comment .= '<ul'.$style.'>';
        // if ($instance['show_thumbs']){
        // }else{
        //     $hot_comment .= yi_hot_comment_viewed($number, $days);
        // }
        $review = new WP_Query(array(
            'post_type' => array('post'),
            'showposts' => $number,
            'ignore_sticky_posts' => true,
            'orderby' => 'comment_count',
            'order' => 'desc',
            'date_query' => array(
                array(
                    'after' => '3 month ago',
                ),
            ),
        ));
        while ($review->have_posts()):
            $review->the_post();
            $hot_comment .= '<article id="post-'.get_the_ID().'" class="post">';
            $hot_comment .= '<div class="entry">';
            $hot_comment .= '<div class="article-inner">';
            if ($instance['show_thumbs']){
                if(has_post_thumbnail()){
                    $hot_comment .= '<figure class="thumb">';
                    if(has_post_thumbnail()){
                        $hot_comment .= '<a class="entry-img" href="'.get_the_permalink().'">';
                        $hot_comment .= get_the_post_thumbnail();//默认尺寸的特色图
                        $hot_comment .= '</a>';
                    }
                    $hot_comment .= '</figure>';
                }
            }
            $hot_comment .= '<header class="entry-header">';
            $hot_comment .= '<span class="entry-title"><a href="'.get_the_permalink().'" title="'.get_the_title().'"'.yi_target_blank().'>'.get_the_title().'</a></span>';
            $hot_comment .= '</header>';
            $hot_comment .= '<div class="entry-content">';
            $hot_comment .= '<span class="'.(has_post_thumbnail() ? "entry-meta" : "entry-meta-no").'">';
            $hot_comment .= '<span class="date"><i class="yi yi-date"></i>'.get_the_time('Y-m-d').'</span>'.(has_post_thumbnail() ? $br : "");
            $hot_comment .= '<span class="comment"><a href="'.get_the_permalink().'#comment"><i class="yi yi-comment"></i>'.get_comments_number("0", "1", "%").'</a></span>';//评论数统计
            $hot_comment .= '<span class="view"><i class="yi yi-eye"></i>'.yi_get_post_views(get_the_ID()).'</span>';// 浏览量
            $hot_comment .= '</span>';
            $hot_comment .= '<div class="clear"></div>';
            $hot_comment .= '</div>';
            $hot_comment .= '</div>';
            $hot_comment .= '</div>';
            $hot_comment .= '</article>';
        endwhile;
        wp_reset_query();
        $hot_comment .= '</ul>';
        $hot_comment .= $after_widget;
        echo $hot_comment;
    }

    function update($new_instance, $old_instance){
        if (!isset($new_instance['submit'])){
            return false;
        }
        $instance = $old_instance;
        $instance = array();
        $instance['show_thumbs'] = $new_instance['show_thumbs']?1:0;
        $instance['title'] = strip_tags( $new_instance['title'] );
        $instance['number'] = strip_tags($new_instance['number']);
        $instance['days'] = strip_tags($new_instance['days']);
        return $instance;
    }

    function form($instance){
        $defaults = $this -> defaults();
        $instance = wp_parse_args( (array) $instance, $defaults );
        if ( isset( $instance[ 'title' ] ) ) {
            $title = $instance[ 'title' ];
        }
        else {
            $title = '热评文章';
        }
        global $wpdb;
        $instance = wp_parse_args((array) $instance, array('number' => '5'));
        $instance = wp_parse_args((array) $instance, array('days' => '90'));
        $number = strip_tags($instance['number']);
        $days = strip_tags($instance['days']);
    ?>
    <p>
        <label for="<?php echo $this->get_field_id( 'title' ); ?>">标题：</label>
        <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo $title; ?>" />
    </p>
    <p>
        <label for="<?php echo $this->get_field_id('number'); ?>">显示数量：</label>
        <input id="<?php echo $this->get_field_id( 'number' ); ?>" name="<?php echo $this->get_field_name( 'number' ); ?>" type="text" value="<?php echo $number; ?>" size="3" />
    </p>
    <p>
        <label for="<?php echo $this->get_field_id('days'); ?>">时间限定（天）：</label>
        <input id="<?php echo $this->get_field_id( 'days' ); ?>" name="<?php echo $this->get_field_name( 'days' ); ?>" type="text" value="<?php echo $days; ?>" size="3" />
    </p>
    <p>
        <input type="checkbox" class="checkbox" id="<?php echo esc_attr( $this->get_field_id('show_thumbs') ); ?>" name="<?php echo esc_attr( $this->get_field_name('show_thumbs') ); ?>" <?php checked( (bool) $instance["show_thumbs"], true ); ?>>
        <label for="<?php echo esc_attr( $this->get_field_id('show_thumbs') ); ?>">显示缩略图</label>
    </p>
    <input type="hidden" id="<?php echo $this->get_field_id('submit'); ?>" name="<?php echo $this->get_field_name('submit'); ?>" value="1" />
    <?php }
}