<?php

class widget_yi_comments extends WP_Widget {
    function __construct(){
        $widget_ops = array(
            'classname' => 'widget_yi_comments',
            'description' => __('最新评论(头像+名称+评论)', 'yii'),
            'customize_selective_refresh' => true,
        );
        parent::__construct('widget_yi_comments', 'YI-最新评论', $widget_ops);
    }

    function widget( $args, $instance ) {
        extract( $args );

        $title   = apply_filters('widget_name', $instance['title']);
        $limit   = isset($instance['limit']) ? $instance['limit'] : 8;
        $outer   = isset($instance['outer']) ? $instance['outer'] : '1';
        $outpost = isset($instance['outpost']) ? $instance['outpost'] : '';

        $comments = "";
        $comments .= $before_widget;
        $comments .= $before_title.$title.$after_title; 
        $comments .= '<ul>';
        $comments .= mod_newcomments( $limit,$outpost,$outer );
        $comments .= '</ul>';
        $comments .= $after_widget;
        echo $comments;
    }

    function form($instance) {
        $defaults = array( 
            'title' => '最新评论', 
            'limit' => 8, 
            'outer' => '1',
            'outpost' => '',
        );
        $instance = wp_parse_args( (array) $instance, $defaults );
    ?>
    <p>
        <label for="<?php echo $this->get_field_id('title'); ?>">标题：</label>
        <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $instance['title']; ?>" />
    </p>
    <p>
        <label for="<?php echo $this->get_field_id('limit'); ?>">显示数目：</label>
        <input class="widefat" id="<?php echo $this->get_field_id('limit'); ?>" name="<?php echo $this->get_field_name('limit'); ?>" type="number" value="<?php echo $instance['limit']; ?>" />
    </p>
    <p>
        <label for="<?php echo $this->get_field_id('outer'); ?>">排除某用户ID：</label>
        <input class="widefat" id="<?php echo $this->get_field_id('outer'); ?>" name="<?php echo $this->get_field_name('outer'); ?>" type="number" value="<?php echo $instance['outer']; ?>" />
    </p>
    <p>
        <label for="<?php echo $this->get_field_id('outpost'); ?>">排除某文章ID：</label>
        <input class="widefat" id="<?php echo $this->get_field_id('outpost'); ?>" name="<?php echo $this->get_field_name('outpost'); ?>" type="number" value="<?php echo $instance['outpost']; ?>" />
    </p>
    <?php
    }
}

/**
 * [mod_newcomments 从数据库中取出最新评论]
 * @param  [int] $limit   [显示的评论数]
 * @param  [int] $outpost [要排除的文章ID]
 * @param  [int] $outer   [要排除的用户ID]
 */
function mod_newcomments( $limit,$outpost,$outer ){
    global $wpdb;
    
    if( !$outer || $outer==0 ){
        $outer = 11111111111;
    }

    $sql = "SELECT DISTINCT ID, post_title, post_password, user_id, comment_ID, comment_post_ID, comment_author, comment_date_gmt, comment_approved,comment_author_email, comment_type,comment_author_url, SUBSTRING(comment_content,1,1000) AS com_excerpt FROM $wpdb->comments LEFT OUTER JOIN $wpdb->posts ON ($wpdb->comments.comment_post_ID = $wpdb->posts.ID) WHERE comment_post_ID!='".$outpost."' AND user_id!='".$outer."' AND comment_approved = '1' AND comment_type = '' AND post_password = '' ORDER BY comment_date_gmt DESC LIMIT $limit";
    $comments = $wpdb->get_results($sql);
    $output = '';
    foreach ( $comments as $comment ) {
        $com_excerpt = yi_replace_enlazy(convert_smilies(strip_tags($comment->com_excerpt,'<img><font><strong>')));
        $output .= '<li>';
            $output .= '<a href="'.get_comment_link($comment->comment_ID).'" title="来自《'.$comment->post_title.'》的评论">';
                $output .= '<figure class="user-thumb">'.yi_get_avatar($user_id=$comment->user_id, $user_email=$comment->comment_author_email).'</figure>';
                $output .= '<p class="comment-meta"><strong>'.$comment->comment_author.'</strong>&nbsp;'.yi_get_time_ago( $comment->comment_date_gmt ).'：</p>';
                $output .= '<p class="comment-content">'.htmlspecialchars_decode($com_excerpt).'</p>';
            $output .= '</a>';
        $output .= '</li>';
    }
    
    return $output;
};