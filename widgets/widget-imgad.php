<?php

class widget_yi_imgad extends WP_Widget {
    function __construct(){
        $widget_ops = array(
            'classname' => 'widget_yi_imgonly',
            'description' => __('显示一个广告(包括富媒体)', 'yii'),
            'customize_selective_refresh' => true,
        );
        parent::__construct('widget_yi_imgad', 'YI-图片广告', $widget_ops);
    }

    function widget( $args, $instance ) {
        extract( $args );

        $title = apply_filters('widget_name', $instance['title']);// 标题
        $code = isset($instance['code']) ? $instance['code'] : '';// 代码

        $imgad = "";
        $imgad .= $before_widget;
        $imgad .= '<div class="item">'.$code.'</div>';
        $imgad .= $after_widget;
        echo $imgad;
    }

    function form($instance) {
        // 定义默认的广告标题与代码
        $defaults = array( 
            'title' => '广告',
            'code' => '<a href="https://yiven.vip" target="_blank"><img src="'.get_template_directory_uri().'/images/ad/ad_rental.jpg"></a>'
        );
        $instance = wp_parse_args( (array) $instance, $defaults );//合成数组,用instance覆盖defaults
    ?>
    <p>
        <label for="<?php echo $this->get_field_id('title');?>">广告名称：</label>
        <input id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $instance['title']; ?>" class="widefat" />
    </p>
    <p>
        <label for="<?php echo $this->get_field_id('code');?>">广告代码：</label>
        <textarea id="<?php echo $this->get_field_id('code'); ?>" name="<?php echo $this->get_field_name('code'); ?>" class="widefat" rows="12" style="font-family:Courier New;"><?php echo $instance['code']; ?></textarea>
    </p>
    <?php
    }
}