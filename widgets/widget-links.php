<?php

class widget_yi_Links extends WP_Widget {
    public function __construct() {
        $widget_ops = array(
            'description'                 => __('您的链接列表', 'yii'),
            'customize_selective_refresh' => true,
            'classname' => 'widget_yi_links'
        );
        parent::__construct('widget_yi_Links', __('YI-友情链接', 'yii'), $widget_ops);
    }

    // 输出显示在页面上
    public function widget($args, $instance){
        $show_description = isset( $instance['description'] ) ? $instance['description'] : false;
        $show_name        = isset( $instance['name'] ) ? $instance['name'] : false;
        $show_rating      = isset( $instance['rating'] ) ? $instance['rating'] : false;
        $show_images      = isset( $instance['images'] ) ? $instance['images'] : true;
        $category         = isset( $instance['category'] ) ? $instance['category'] : false;
        $orderby          = isset( $instance['orderby'] ) ? $instance['orderby'] : 'name';
        $order            = isset( $instance['order'] ) ? $instance['order'] : 'ASC';
        $limit            = isset( $instance['limit'] ) ? $instance['limit'] : -1;

        $before_widget = preg_replace('/id="[^"]*"/', 'id="%id"', $args['before_widget']);

        $widget_links_args = array(
            'title_before'     => $args['before_title'],
            'title_after'      => $args['after_title'],
            'category_before'  => $before_widget,
            'category_after'   => $args['after_widget'],
            'show_images'      => $show_images,
            'show_description' => $show_description,
            'show_name'        => $show_name,
            'show_rating'      => $show_rating,
            'category'         => $category,
            'class'            => 'linkcat widget',
            'orderby'          => $orderby,
            'order'            => $order,
            'limit'            => $limit,
            'echo'             => 0,
        );
        $widget_links = wp_list_bookmarks(apply_filters('widget_yi_links_args', $widget_links_args, $instance));
        $widget_links = preg_replace('/(\?url=)(http|https):\/\//', '$1', $widget_links);
        echo $widget_links;
    }

    // 进行更新保存
    public function update($new_instance, $old_instance){
        $new_instance = (array)$new_instance;
        $instance     = array(
            'images'      => 0,
            'name'        => 0,
            'description' => 0,
            'rating'      => 0,
        );
        foreach ($instance as $field => $val){
            if (isset( $new_instance[$field])){
                $instance[ $field ] = 1;
            }
        }
        $instance['orderby'] = 'name';
        if (in_array( $new_instance['orderby'], array('name', 'rating', 'id', 'rand'))){
            $instance['orderby'] = $new_instance['orderby'];
        }
        $instance['order'] = 'desc';
        if (in_array( $new_instance['order'], array('asc', 'desc'))){
            $instance['order'] = $new_instance['order'];
        }
        $instance['category'] = intval($new_instance['category']);
        $instance['limit']    = !empty($new_instance['limit']) ? intval($new_instance['limit']) : -1;
        return $instance;
    }

    // 给小工具(widget) 添加表单内容
    public function form($instance){
        $instance  = wp_parse_args(
            (array) $instance,
            array(
                'images'      => true,
                'name'        => true,
                'description' => false,
                'rating'      => false,
                'category'    => false,
                'orderby'     => 'name',
                'order'       => 'desc',
                'limit'       => -1,
            )
        );
        $link_cats = get_terms('link_category');
        if (!$limit = intval($instance['limit'])){
            $limit = -1;
        }
    ?>
    <p>
        <label for="<?php echo $this->get_field_id('category'); ?>"><?php _e('请选择链接分类：', 'yii'); ?></label>
        <select class="widefat" id="<?php echo $this->get_field_id('category'); ?>" name="<?php echo $this->get_field_name('category'); ?>">
            <option value=""><?php _ex('全部链接', 'yii'); ?></option>
            <?php
            foreach ($link_cats as $link_cat){
                echo '<option value="'.intval($link_cat->term_id).'"'.selected($instance['category'], $link_cat->term_id, false).'>'.$link_cat->name."</option>";
            }
            ?>
        </select>
    </p>
    <p>
        <label for="<?php echo $this->get_field_id('orderby'); ?>"><?php _e( '排序依据：','yii' ); ?></label>
        <select name="<?php echo $this->get_field_name('orderby'); ?>" id="<?php echo $this->get_field_id('orderby'); ?>" class="widefat">
            <option value="name"<?php selected($instance['orderby'], 'name'); ?>><?php _e('链接标题', 'yii'); ?></option>
            <option value="rating"<?php selected($instance['orderby'], 'rating'); ?>><?php _e('链接评级', 'yii'); ?></option>
            <option value="id"<?php selected($instance['orderby'], 'id'); ?>><?php _e('链接ID', 'yii'); ?></option>
            <option value="rand"<?php selected($instance['orderby'], 'rand'); ?>><?php _ex('随机', 'yii'); ?></option>
        </select>
    </p>
    <p>
        <label for="<?php echo $this->get_field_id('order'); ?>"><?php _e( '排序方式：' ); ?></label>
        <select name="<?php echo $this->get_field_name('order'); ?>" id="<?php echo $this->get_field_id('order'); ?>" class="widefat">
            <option value="asc"<?php selected($instance['order'], 'asc'); ?>><?php _e('升序', 'yii'); ?></option>
            <option value="desc"<?php selected($instance['order'], 'desc'); ?>><?php _e('降序', 'yii'); ?></option>
        </select>
    </p>
    <p>
        <input class="checkbox" type="checkbox"<?php checked($instance['images'], true); ?> id="<?php echo $this->get_field_id('images'); ?>" name="<?php echo $this->get_field_name('images'); ?>" />
        <label for="<?php echo $this->get_field_id('images'); ?>"><?php _e('显示链接图像', 'yii'); ?></label>
    </p>
    <p>
        <input class="checkbox" type="checkbox"<?php checked( $instance['name'], true ); ?> id="<?php echo $this->get_field_id('name'); ?>" name="<?php echo $this->get_field_name('name'); ?>" />
        <label for="<?php echo $this->get_field_id('name'); ?>"><?php _e('显示链接名称', 'yii'); ?></label>
    </p>
    <p>
        <input class="checkbox" type="checkbox"<?php checked( $instance['description'], true ); ?> id="<?php echo $this->get_field_id('description'); ?>" name="<?php echo $this->get_field_name('description'); ?>" />
        <label for="<?php echo $this->get_field_id('description'); ?>"><?php _e('显示链接描述', 'yii'); ?></label>
    </p>
    <p>
        <input class="checkbox" type="checkbox"<?php checked( $instance['rating'], true ); ?> id="<?php echo $this->get_field_id('rating'); ?>" name="<?php echo $this->get_field_name( 'rating' ); ?>" />
        <label for="<?php echo $this->get_field_id('rating'); ?>"><?php _e('显示链接评级', 'yii'); ?></label>
    </p>
    <p>
        <label for="<?php echo $this->get_field_id('limit'); ?>"><?php _e('显示链接数：', 'yii'); ?></label>
        <input id="<?php echo $this->get_field_id('limit'); ?>" name="<?php echo $this->get_field_name('limit'); ?>" type="text" value="<?php echo $limit == -1 ? '' : intval($limit); ?>" size="3" />
    </p>
    <?php
    }
}
