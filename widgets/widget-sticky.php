<?php

class widget_yi_sticky extends WP_Widget {
    function __construct(){
        $widget_ops = array(
            'classname' => 'widget_yi_posts',
            'description' => __('图文展示(置顶推荐)', 'yii'),
            'customize_selective_refresh' => true,
        );
        parent::__construct('widget_yi_sticky', 'YI-置顶推荐', $widget_ops);
    }

    function widget( $args, $instance ) {
        extract( $args );

        $title = apply_filters('widget_name', $instance['title']);
        $limit = isset($instance['limit']) ? $instance['limit'] : 6;
        $img   = isset($instance['img']) ? $instance['img'] : '';
        $comn   = isset($instance['comn']) ? $instance['comn'] : '';

        $style = '';
        if( !$img ) $style = ' class="nopic"';

        $sticky = "";
        $sticky .= $before_widget;
        $sticky .= $before_title.$title.$after_title; 
        $sticky .= '<ul'.$style.'>';
        $sticky .= yi_sticky_posts_list( $limit,$img,$comn );
        $sticky .= '</ul>';
        $sticky .= $after_widget;
        echo $sticky;
    }

    function form( $instance ) {
        $defaults = array( 
            'title' => '置顶推荐', 
            'limit' => 6,  
            'img' => '',
            'comn' => ''
        );
        $instance = wp_parse_args( (array) $instance, $defaults );
    ?>
    <p>
        <label for="<?php echo $this->get_field_id('title'); ?>">标题：</label>
        <input style="width:100%;" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $instance['title']; ?>" />
    </p>
    <p>
        <label for="<?php echo $this->get_field_id('limit'); ?>">显示数目：</label>
        <input style="width:100%;" id="<?php echo $this->get_field_id('limit'); ?>" name="<?php echo $this->get_field_name('limit'); ?>" type="number" value="<?php echo $instance['limit']; ?>" size="24" />
    </p>
    <p>
        <input style="vertical-align:-3px;margin-right:4px;" class="checkbox" type="checkbox" <?php checked( $instance['img'], 'on' ); ?> id="<?php echo $this->get_field_id('img'); ?>" name="<?php echo $this->get_field_name('img'); ?>">
        <label for="<?php echo $this->get_field_id('img'); ?>">显示图片</label>
    </p>
    <p>
        <input style="vertical-align:-3px;margin-right:4px;" class="checkbox" type="checkbox" <?php checked( $instance['comn'], 'on' ); ?> id="<?php echo $this->get_field_id('comn'); ?>" name="<?php echo $this->get_field_name('comn'); ?>">
        <label for="<?php echo $this->get_field_id('comn'); ?>">显示评论数</label>
    </p>
    <?php
    }
}


function yi_sticky_posts_list($limit,$img,$comn) {
    $sticky = get_option('sticky_posts'); rsort( $sticky );
    $args = array(
        'post__in' => $sticky,
        'showposts'        => $limit,
        'ignore_sticky_posts' => 1
    );
    query_posts($args);
    while (have_posts()) : the_post(); 
?>
<li><a<?php echo yi_target_blank() ?> href="<?php the_permalink(); ?>"><?php if( $img ){echo '<span class="thumbnail">'; echo yi_get_post_thumbnail(); echo '</span>'; }else{$img = '';} ?><span class="text"><?php the_title(); ?><?php echo yi_get_subtitle() ?></span><span class="muted"><?php the_time('Y-m-d');?></span><?php if( $comn ){ ?><span class="muted"><?php echo '评论(', comments_number('0', '1', '%'), ')'; ?></span><?php } ?></a></li>
<?php
    
    endwhile; wp_reset_query();
}