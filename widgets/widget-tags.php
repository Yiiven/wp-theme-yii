<?php

class widget_yi_tags extends WP_Widget {
    function __construct(){
        $widget_ops = array(
            'classname' => 'widget_yi_tags',
            'description' => __('显示一个文本特别推荐', 'yii'),
            'customize_selective_refresh' => true,
        );
        parent::__construct('widget_yi_tags', 'YI-标签云', $widget_ops);
    }

    public function defaults() {
		return array(
			'show_3d' => 0,
		);
	}

    function widget($args, $instance){
        extract($args);
        $defaults = $this -> defaults();
        $instance = wp_parse_args((array)$instance, $defaults);

        $title  = apply_filters('widget_name', $instance['title']);
        $number  = isset($instance['number']) ? $instance['number'] : 30;
        $counter = isset($instance['counter']) ? $instance['counter'] : 0;
        $offset = isset($instance['offset']) ? $instance['offset'] : 0;

        $html_tags = "";
        $html_tags .= $before_widget;
        $html_tags .= $before_title.$title.$after_title;
        if ($instance['show_3d']){
            $html_tags .= '<div id="widget-tags-cloud" class="tags-items">';
        }else{
            $html_tags .= '<div id="tagscloud" class="tags-items">';
        }
        /*$tags_list = get_tags('orderby=count&order=DESC&number='.$number.'&offset='.$offset);
        if ($tags_list){ 
            $i = 1;
            foreach($tags_list as $key => $val){
                $html_tags .= '<a class="tag-cloud-link tag-link-'.$val->term_id.' tag-link-position-'.$i.'" href="'.get_tag_link($val).'">';
                $html_tags .= $val->name;
                if($counter){
                    $html_tags .= '('.$val->count.')';
                }
                $html_tags .= '</a>';
                $i++;
            } 
        }else{
            $html_tags .= '暂无标签！';
        }*/
        $tags_list = wp_tag_cloud(array(
            'smallest' => '14',// 字号-最小的标签（使用次数最少）
            'largest' => '14',// 字号-最大的标签（使用次数最多）
            'unit' => 'px',// 字号单位
            'number' => $number,
            'orderby' => 'count',// 排序字段
            'order' => 'RAND',// 排序规则
            'echo'=> null,
            'show_count' => ($counter ? 1 : 0),// 是否显示标签下文章计数
        ));
        $html_tags .= $tags_list;
        $html_tags .= '</div>';
        $html_tags .= $after_widget;
        echo $html_tags;
        if ($instance['show_3d']){
            wp_enqueue_script('tags', get_template_directory_uri().'/js/tags.js', array(), version, false);
        }
    }

    function form($instance) {
        $defaults = array( 
            'title' => '热门标签', // 标题
            'count' => 30, // 数量
            'counter' => 0, // 标签计数(当前标签下的文章数量)
            'offset' => 0, // 剔除前N个
            'show_3d' => 0, // 是否启用3d效果
        );
        $instance = wp_parse_args((array)$instance, $defaults);
    ?>
    <p>
        <label for="<?php echo $this->get_field_id('title'); ?>">名称：</label>
        <input id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $instance['title']; ?>" class="widefat" />
    </p>
    <p>
        <label for="<?php echo $this->get_field_id('number'); ?>">显示数量：</label>
        <input id="<?php echo $this->get_field_id('number'); ?>" name="<?php echo $this->get_field_name('number'); ?>" type="number" value="<?php echo $instance['number']; ?>" class="widefat" />
    </p>
    <p>
        <label for="<?php echo $this->get_field_id('offset'); ?>">去除前几个：</label>
        <input id="<?php echo $this->get_field_id('offset'); ?>" name="<?php echo $this->get_field_name('offset'); ?>" type="number" value="<?php echo $instance['offset']; ?>" class="widefat" />
    </p>
    <p>
        <input class="checkbox" type="checkbox" <?php checked( $instance['counter'], 'on' ); ?> id="<?php echo $this->get_field_id('counter'); ?>" name="<?php echo $this->get_field_name('counter'); ?>">
        <label for="<?php echo $this->get_field_id('counter'); ?>">显示标签计数</label>
    </p>
    <p>
		<input type="checkbox" class="checkbox" id="<?php echo esc_attr($this->get_field_id('show_3d')); ?>" name="<?php echo esc_attr($this->get_field_name('show_3d')); ?>" <?php checked((bool)$instance["show_3d"], true); ?>>
		<label for="<?php echo esc_attr($this->get_field_id('show_3d')); ?>">启用3D特效</label>
	</p>
    <?php
    }
}