<?php 
// 关注我们
class widget_yi_feed extends WP_Widget {
    public function __construct() {
        $widget_ops = array(
            'classname' => 'widget_yi_feed',
            'description' => __('RSS、微信、微博', 'yii'),
            'customize_selective_refresh' => true,
        );
        parent::__construct('widget_yi_feed', 'YI-关注我们', $widget_ops);
    }

    function widget($args, $instance) {
        global $wpdb;
        extract($args);
        
        $count_posts = wp_count_posts();
        $total_comments = get_comment_count();
        $users = $wpdb->get_var("SELECT COUNT(ID) FROM $wpdb->users");
        $title = apply_filters('widget_name', $instance['title']);
        $yan = isset($instance['yan']) ? $instance['yan'] : 0;
        
        $feed = '';
        $feed .= $before_widget;
        if (!empty($title))
        $feed .= $before_title . $title . $after_title;
        $feed .= '<div id="feed_widget">';
        $feed .= '<div class="feed-about">';
        $feed .= '<div class="author-back" style="background-image:url('.get_template_directory_uri().'/images/feed-about-bg.jpg)"></div>';
        $feed .= '<div class="about-main">';
        $feed .= '<div class="about-img"><img src="'.(_yi('logo_image') ? _yi('logo_image') : (get_bloginfo('template_url').'/images/logo.png')).'" alt="name"></div>';
        $feed .= '<div class="about-name">'.get_bloginfo('name').'</div>';
        $feed .= '<div class="about-desc">'.get_bloginfo('description').'</div>';

        if($yan){
            //一言API
            //c         可选，[a-动画|b-漫画|c-游戏|d-小说|e-原创|f-网络|g-其他|未知-随机]
            //encode    可选，[text-文本|json-无unicode转码的json|js-指定选择器(默认.hitokoto)的同步函数|未知-unicode转码后的json]
            //charset   可选，[utf-8|gbk]
            $yan_result_text = yi_http_request('https://v1.hitokoto.cn/?c=e&encode=text&charset=uft-8', 'GET');
            $feed .= '<div class="about-yan">'.$yan_result_text.'</div>';
        }
        $feed .= '</div><ul>';
        $feed .= '<li class="weixin"><span class="weixin-box"><span class="weixin-qr"><img src="'.preg_replace('/(http|https):/' ,'' ,$instance['weixin']).'" alt="weixin"/></span><a title="微信"><i class="yi yi-weixin"></i></a></span></li>';
        //https://wpa.qq.com/msgrd?V=3&uin=88888&Site=QQ&Menu=yes
        $feed .= '<li class="qq"><a title="腾讯QQ" href="tencent://message/?uin='.$instance['qq'].'&Site=qq&Menu=yes" target="_blank" rel="external nofollow"><i class="yi yi-qq"></i></a></li>';
        $feed .= '<li class="weibo"><a title="新浪微博" href="'.esc_url('https://weibo.com/'.$instance['weibo']).'" target="_blank" rel="external nofollow"><i class="yi yi-stsina"></i></a></li>';
        $feed .= '<li class="feed"><a title="订阅" href="'.esc_url(home_url('/')).'feed/" target="_blank" rel="external nofollow"><i class="yi yi-rss"></i></a></li>';
        $feed .= '</ul><div class="about-inf">';
        $feed .= '<span class="about about-cn"><p>文章</p><p>'.$count_posts->publish.'</p></span>';
        $feed .= '<span class="about about-pn"><p>留言</p><p>'.$total_comments['approved'].'</p></span>';
        $feed .= '<span class="about about-cn"><p>用户</p><p><span class="animateNum" data-animatetype="num" data-animatetarget="'.$users.'" style="">'.$users.'</span></p></span>';
        $feed .= '</div></div></div>';
        $feed .= $after_widget;
        echo $feed;
    }

    function update($new_instance, $old_instance) {
        if (!isset($new_instance['submit'])) {
            return false;
        }
        $instance = $old_instance;
        // $instance = array();
        $instance['title'] = strip_tags($new_instance['title']);
        $instance['weixin'] = $new_instance['weixin'];
        $instance['weibo'] = $new_instance['weibo'];
        $instance['qq'] = $new_instance['qq'];
        $instance['yan'] = $new_instance['yan'];
        return $instance;
    }

    function form($instance) {
        $defaults = array( 
            'title' => '关注我们', // 标题
            'weibo' => '输入微博个性域名', // 微博个性域名
            'qq' => '输入QQ号', // QQ号
            'weixin' => get_template_directory_uri().'/images/Qrcode_mp_weixin.jpg"', // 微信二维码地址
            'yan' => true, // 是否启用一言
        );
        $instance = wp_parse_args((array)$instance, $defaults);
    ?>
    <p>
        <label for="<?php echo $this->get_field_id('title'); ?>">标题：</label>
        <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $instance['title']; ?>" />
    </p>
    <p>
        <label for="<?php echo $this->get_field_id('weixin'); ?>">微信二维码：</label>
        <input class="widefat" id="<?php echo $this->get_field_id('weixin'); ?>" name="<?php echo $this->get_field_name('weixin'); ?>" type="text" value="<?php echo $instance['weixin']; ?>" />
    </p>
    <p>
        <label for="<?php echo $this->get_field_id('weibo'); ?>">新浪微博个性域名：</label>
        <input class="widefat" id="<?php echo $this->get_field_id('weibo'); ?>" name="<?php echo $this->get_field_name('weibo'); ?>" type="text" value="<?php echo $instance['weibo']; ?>" />
    </p>
    </p>
        <label for="<?php echo $this->get_field_id('qq'); ?>">腾讯QQ：</label>
        <input class="widefat" id="<?php echo $this->get_field_id('qq'); ?>" name="<?php echo $this->get_field_name('qq'); ?>" type="text" value="<?php echo $instance['qq']; ?>" />
    </p>
    <p>
        <input class="checkbox" id="<?php echo $this->get_field_id('yan'); ?>" name="<?php echo $this->get_field_name('yan'); ?>" type="checkbox" <?php checked($instance['yan'], 'on'); ?> />
        <label for="<?php echo $this->get_field_id('yan'); ?>">启用一言(随机一句话)</label>
    </p>
    <input type="hidden" id="<?php echo $this->get_field_id('submit'); ?>" name="<?php echo $this->get_field_name('submit'); ?>" value="1" />
    <?php 
    }
}