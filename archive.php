<?php 
/**
 * Template Name: archive
 * The template for displaying archive.
 * @package yii
 * 归档页，在访问分类、标签等归档页面时，将使用此页替代index进行展示
 */
 ?>
<?php get_header(); ?>
    <div class="container archive-container">
        <?php if(have_posts()) : //检查博客是否有日志 ?>
        <main class="main">
            <div class="block-title">
                <div class="title textEllipsis" data-tipso="文章归档">文章归档</div>
                <div class="line left-line"></div>
                <div class="line right-line"></div>
            </div>
            <div class="block-content">
            <?php while(have_posts()) : the_post(); //执行 the_post() 去调取日志 ?>
                <?php get_template_part("template/article") ?>
            <?php endwhile; ?>
            </div>
        </main>
        <div class="page-navigation">
            <?php yi_paged(); ?>
        </div>
        <?php else : //博客没有日志的时候执行 ?>
        <div class="main">
            <div class="post">
                <h2><?php _e('博主很懒，什么也没留下...'); ?></h2>
            </div>
        </div>
        <?php endif; ?>
    </div>
<?php get_footer(); ?>