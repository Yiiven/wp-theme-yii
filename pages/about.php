<?php
/**
 * Template Name: 关于
 * 提示：网站简介可以书写在此处
 */
get_header(); 
?>
<div class="container page-container">
    <div id="blinks-main" class="main page-main">
        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
        <article class="blinks page">
            <div class="entry">
                <div class="article-inner">
                    <header class="entry-header">
                        <h2>关于</h2>
                    </header><!-- /header -->
                    <div class="entry-summary">
                        <p class="mt20"><h3>寄语</h3></p>
                        <p>欢迎访问本站，感谢您的支持与厚爱！博客主要记录工作和生活中的点点滴滴，但更多的还是WEB开发技术的学习和分享。同时不断改善和提升用户体验，让大家在阅读的同时有更佳的体验！</p>
                        <p>如果您有建议或者意见对我说，请在下方留言区给我留言。</p>
                        <p class="mt20"><h3>网站历程</h3></p>
                        <div class="time-axis">
                            <ul class="time-vertical">
                                <li><span class="time">2019-09-24</span><span class="description">历时67天，新主题yii终于可以上线应用了</span></li>
                                <li><span class="time">2019-09-20</span><span class="description">PHP版本升级到7.2.x(这次时间充足，把中途出现的问题全都排掉了),开启全站HTTPS</span></li>
                                <li><span class="time">2019-06-19</span><span class="description">准备制作属于自己的WordPress主题</span></li>
                                <li><span class="time">2019-06-06</span><span class="description">主站启用HTTPS</span></li>
                                <li><span class="time">2019-06-04</span><span class="description">PHP版本从5.6.x升级至7.2.x，因为实在是太困了，升级完之后发现网站不能访问，将PHP版本降低为7.1.x</span></li>
                                <li><span class="time">2019-12-21</span><span class="description">接入网络安全联网备案</span></li>
                                <li><span class="time">2018-12-15</span><span class="description">正式启用站点，历史文章搬迁到WordPress</span></li>
                                <li><span class="time">2018-11-10</span><span class="description">提交工信部备案申请</span></li>
                                <li><span class="time">2018-11-05</span><span class="description">正式入驻华为云</span></li>
                                <li><span class="time">2018-10-26</span><span class="description">完成域名yiven.vip的注册</span></li>
                            </ul>
                        </div>
                        <!--<div class="time-axis">
                            <ul class="time-horizontal">
                                <li><span class="time">2019</span><span class="description">desc</span></li>
                                <li><span class="time">2019</span><span class="description">desc</span></li>
                                <li><span class="time">2019</span><span class="description">desc</span></li>
                            </ul>
                        </div>-->
                    </div>
                    <div class="post-continue-container">
                        <?php get_template_part("template/shang"); ?>
                    </div>
                </div>
            </div>
            <?php if(comments_open()): ?>
            <div id="comments" class="comments-area">
                <?php comments_template('/comments-page.php');//评论模板 ?>
            </div>
            <?php endif; ?>
        </article>
        <?php endwhile; else: ?>
        <?php endif; ?>
    </div>
</div>
<?php get_footer(); ?>