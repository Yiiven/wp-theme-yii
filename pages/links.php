<?php
/**
 * Template Name: 自助申请友链
 * 提示：友情链接，需在后台审核
 */
if( isset($_POST['link_form']) && $_POST['link_form'] == 'send'){
    global $wpdb;
    // 表单变量初始化
    $link_name = isset( $_POST['link_name'] ) ? trim(htmlspecialchars($_POST['link_name'], ENT_QUOTES)) : '';
    $link_url =  isset( $_POST['link_url'] ) ? trim(htmlspecialchars($_POST['link_url'], ENT_QUOTES)) : '';
    $link_description =  isset( $_POST['link_qq'] ) ? trim(htmlspecialchars($_POST['link_qq'], ENT_QUOTES)) : ''; // 联系方式
    $link_target =  "_blank";
    $link_visible = "N"; // 表示链接默认不可见
    // 表单项数据验证
    if ( empty($link_name) || mb_strlen($link_name) > 20 ){
        wp_die('链接名称必须填写，长度不得超过30字');
    }
    if ( empty($link_url) || strlen($link_url) > 60 || !preg_match("/^(https?:\/\/)?(((www\.)?[a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]+)?\.([a-zA-Z]+))|(([0-1]?[0-9]?[0-9]|2[0-5][0-5])\.([0-1]?[0-9]?[0-9]|2[0-5][0-5])\.([0-1]?[0-9]?[0-9]|2[0-5][0-5])\.([0-1]?[0-9]?[0-9]|2[0-5][0-5]))(\:\d{0,4})?)(\/[\w- .\/?%&=]*)?$/i", $link_url)) { //验证url
        wp_die('请填写正确的链接地址');
    }
    $sql_link = $wpdb->insert(
        $wpdb->links, 
        array(
            'link_name' => '【待审核】--- '.$link_name,
            'link_url' => $link_url,
            'link_target' => $link_target,
            'link_description' => $link_description,
            'link_visible' => $link_visible
        )
    );
    $result = $wpdb->get_results($sql_link);
    wp_die('亲，友情链接提交成功，【等待站长审核中】！<a href="/links.html">点此返回</a>', '提交成功');
}
get_header(); 
?>
<div class="container page-container">
    <div id="blinks-main" class="main page-main">
        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
        <article class="blinks page">
            <div class="entry">
                <div class="article-inner">
                    <header class="entry-header">
                        <h2>友情链接自助申请</h2>
                    </header><!-- /header -->
                    <div class="entry-summary">
                        <p class="mt20">欢迎与本站交换友情链接，要求：内容健康，内容相关更佳。</p>
                        <p class="mt20"><h3>友链自助申请须知</h3></p>
                        <p>&#x2714; 申请前请先加上本站链接；</p>
                        <p>&#x2714; 网站域名必须是一级域名；</p>
                        <p>&#x2714; 稳定更新，尽可能做到每月文章1+；</p>
                        <p>&#x2714; 禁止一切产品营销、广告联盟类型的网站，优先通过同类原创、内容相近的网站；</p>
                        <p>&#x2714; 网站内容一定要健康积极向上，凡内容污秽不堪的、反党反社会的、宣扬暴力的、广告挂马的都将不会通过申请。</p>
                        <p class="mt20"><h3>其他</h3></p>
                        <p>博主会不定期访问友链，如果遇到网站长时间打不开，内容不符合条件等情况的话，将会撤销该友链！</p>
                        <p>如果申请后，长时间未通过审核，有可能是博主太忙未看到，可以通过右侧联系方式告知我，谢谢~</p>
                        <p id="comments" class="mt20"><h3>本站链接信息</h3></p>
                        <p>名称：<?php bloginfo('name'); ?></p>
                        <p>网址：<?php bloginfo('url') ?></p>
                    </div>
                </div>
                <div class="article-form">
                    <!--表单开始-->
                    <form method="post" class="mt20" action="<?php echo $_SERVER["REQUEST_URI"]; ?>">
                        <div class="form-group">
                            <label for="link_name"><span class="required">*</span> 链接名称:</label>
                            <input type="text" size="40" value="" class="form-control" id="link_name" placeholder="请输入链接名称" name="link_name" />
                        </div>
                        <div class="form-group">
                            <label for="link_url"><span class="required">*</span> 链接地址:</label>
                            <input type="text" size="40" value="" class="form-control" id="link_url" placeholder="请输入链接地址" name="link_url" />
                        </div>
                        <div class="form-group">
                            <label for="link_qq">联系QQ:</label>
                            <input type="text" size="40" value="" class="form-control" id="link_qq" placeholder="请输入联系QQ" name="link_qq" />
                        </div>
                        <div class="form-group">（提示：带有<span class="required">*</span>，表示必填项~）</div>
                        <div class="form-group">
                            <input type="hidden" value="send" name="link_form" />
                            <input type="submit" class="btn btn-primary" value="提交申请">
                            <input type="reset" class="btn btn-default" value="重填">
                        </div>
                    </form>
                  <!--表单结束-->
                </div>
            </div>
        </article>
        <?php endwhile; else: ?>
        <?php endif; ?>
    </div>
</div>
<?php get_footer(); ?>