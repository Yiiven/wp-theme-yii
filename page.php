<?php 
/**
 * The template for displaying page.
 * @package yii
 * 单页(静态页面)
 */
 ?>
<?php get_header(); ?>
    <div class="container sample-container">
        <?php if(have_posts()) : //检查博客是否有日志 ?>
            <div class="main page-main">
            <?php while(have_posts()) : the_post(); //执行 the_post() 去调取日志 ?>
                <div id="post-<?php the_ID(); //调取日志ID ?>" class="post page">
                    <div class="entry">
                        <div class="article-inner">
                            <header class="entry-header">
                                <h2><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h2>
                            </header><!-- /header -->
                            <div class="entry-summary">
                                <?php the_content();//全文 ?>
                                <?php wp_link_pages('页码：', '', 'number');//当在页面中使用<!--nextpage-->进行分页时，下方显示页码 ?>
                                <?php edit_post_link('编辑', '', ''); ?>
                            </div>
                            <div class="clear"></div>
                            <div class="post-continue-container">
                                <?php get_template_part("template/shang"); ?>
                            </div>
                        </div>
                    </div>
                    <div class="clear"></div>
                    <?php if(comments_open()): ?>
                    <div id="comments" class="comments-area">
                        <?php comments_template();//评论模板 ?>
                    </div>
                    <?php endif; ?>
                </div>
            <?php endwhile; ?>
            </div>
            <?php else : //博客没有日志的时候执行 ?>
            <div class="main">
                <div class="post">
                    <h2><?php _e('哎呀，你找的页面似乎已经不在地球上啦...'); ?></h2>
                </div>
            </div>
        <?php endif; ?>
    </div>
<?php get_footer(); ?>